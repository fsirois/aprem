#!/bin/sh

# First compile all the .cpp files in .o object files
STARTTIME=$(date +%s)
echo compiling
$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -c src/ini_file_reader/iniFileReader.cpp src/circuit_interface/*.cpp src/circuit_class/*.cpp src/circuit_elements/component_types/*.cpp src/circuit_elements/*.cpp -lleveldb)
ENDTIME=$(date +%s)
echo took $(($ENDTIME - $STARTTIME)) seconds to compile

# Second link all the object files into a single shared library
STARTTIME=$(date +%s)
echo linking
$(g++ -shared -o libCircuit.so *.o)
ENDTIME=$(date +%s)
echo took $(($ENDTIME - $STARTTIME)) seconds to link

