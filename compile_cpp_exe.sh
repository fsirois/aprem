#!/bin/sh

#---------------------------------
# Basic
#---------------------------------
$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP/basic_circuits -c examples/CPP/basic_circuits/basic.cpp)
g++ -o basic *.o
rm basic.o

$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP/basic_circuits -c examples/CPP/basic_circuits/multiple_tests.cpp)
g++ -o multiple_tests *.o
rm multiple_tests.o

$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP/basic_circuits -c examples/CPP/basic_circuits/IEEE_14_bus.cpp)
g++ -o IEEE_14_bus *.o
rm IEEE_14_bus.o

$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP/basic_circuits -c examples/CPP/basic_circuits/perf.cpp)
g++ -o perf *.o
rm perf.o

#---------------------------------
# Control and subcircuit
#---------------------------------
$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP/control_subcircuit -c examples/CPP/control_subcircuit/basic_subcircuit.cpp)
g++ -o basic_subcircuit *.o
rm basic_subcircuit.o

$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP/control_subcircuit -c examples/CPP/control_subcircuit/nested_subcircuit.cpp)
g++ -o nested_subcircuit *.o
rm nested_subcircuit.o

$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP -Iexamples/CPP/control_subcircuit -c examples/CPP/control_subcircuit/test_control.cpp)
g++ -o test_control *.o
rm test_control.o

$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP -Iexamples/CPP/control_subcircuit -c examples/CPP/control_subcircuit/test_control_and_subcircuit.cpp)
g++ -o test_control_and_subcircuit *.o
rm test_control_and_subcircuit.o

#---------------------------------
# Delete
#---------------------------------
$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP/delete -c examples/CPP/delete/test_basic_delete.cpp)
g++ -o test_basic_delete *.o
rm test_basic_delete.o

$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP/delete -c examples/CPP/delete/test_delete.cpp)
g++ -o test_delete *.o
rm test_delete.o

$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP -Iexamples/CPP/delete -c examples/CPP/delete/test_delete_control.cpp)
g++ -o test_delete_control *.o
rm test_delete_control.o

#---------------------------------
# Component and node OFF
#---------------------------------
$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP/comp_node_off -c examples/CPP/comp_node_off/test_comp_off.cpp)
g++ -o test_comp_off *.o
rm test_comp_off.o

$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/circuit_interface/includes -Iexamples/CPP/comp_node_off -c examples/CPP/comp_node_off/test_node_off.cpp)
g++ -o test_node_off *.o
rm test_node_off.o

# end
rm libCircuit.so
rm *.o





