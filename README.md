# APREM 3.1.0

APREM is a computer software package developed for computing load flows in power systems of small and medium size. APREM is a French acronym meaning _Parametric Analysis of Electrical Networks in Matlab_. The software is fully usable inside MatLab, but is in fact all written in C++, with a MatLab interface. It is easy to use and the interface is simple.

# Code Example
	% Create empty circuit
    myCircuit = Circuit();
	
	% Add a voltage source called 'myVsrc', between node 'n1' and '0'
	% where 0 is the ground. Voltage is 120, and phase is 0.
    myCircuit.add('Vsrc', 'myVsrc', {'n1', '0'}, {120, 0});
	
	% Add 2 impedances named 'myImp1' and 'myImp2', in series.
	% The impedance is complex for both: 20 + 20i.
	myCircuit.add('Imp', 'myImp1', {'n1','n2'}, 20 + 20i);
    myCircuit.add('Imp', 'myImp2', {'n2','0'}, 20 + 20i);
	
	% Solve the circuit
    myCircuit.solve();
	
	% Get current going through 'myImp' and voltage at node 'n2'
    Current1 = myCircuit.get('myImp', 'I');
	Voltage1 = myCircuit.get('n2', 'V');
	
	% Change impedance 2 value
	myCircuit.set('myImp2', 'Z', 30 + 10i);
	
	% Solve again
	myCircuit.solve();
	
	% Get current going through 'myImp2' again
    Current2 = myCircuit.get('myImp', 'I');

# Motivation

The philosophy behind the APREM software is to offer a total liberty to the user on the parametrization of simulated power systems while keeping the software environment extremely simple and accessible. APREM is particularly useful for studies requiring 
to repeat numerous simulations in which one or many parameters vary each time, either in nested loops or according to any other logic that can easily be programmed, for instance Monte Carlo simulations. The varied parameters can be non-topological (e.g. variation of attributes of existing devices, namely the voltage of a source, the active or reactive power of a load, a setting of a control element, etc.), or even topological (dynamic addition of components, removal of nodes or components for simulating an outage, etc.), which is rarely possible with traditional packages.

The choice of MatLab as the basic environment allows taking advantage of its simplicity and of the richness of the built-in functions in order to quickly set-up case studies (via ordinary Matlab scripts, i.e. M files) as well as for efficiently analyzing the results (all existing Matlab tools can be used).

# Installation

APREM is fairly easy to install if you have a C++ compiler linked with your MatLab version. Just download the package, run the **compile_all.m** file that will run if the C++ compiler is properly set-up. For further detail, see documentation.

Once APREM is compiled, just add the **src\@Circuit** folder to your working directory in MatLab for the Circuit class to be available. Only 2 files are required for APREM to work. Circuit.m and c\_circuit.ext (where ext depends on the OS). Circuit.m is the MatLab interface, and everything else is in the compiled file c_circuit.ext.

# Examples

To run examples and tests, go under the the **examples** folders and run any of the examples in any of the subfolders. The basic examples are under **examples\basic_circuits**.

# Known bugs
- The addition of a large number of components can sometimes be very slow. This problem is related to the Matlab script file parser. A solution to this problem is current being elaborated. [FIXED]
- A transformer that is connect to entirely floating networks (both on primary and secondary sides) will generate an error. This situation is however very unlikely in practice.
- When trying to delete subcircuits, sometimes APREM crashes.




