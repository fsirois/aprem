::---------------------------------
:: Basic
::---------------------------------
g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -Iexamples\CPP\basic_circuits -c examples\CPP\basic_circuits\basic.cpp
g++ -o basic.exe *.o
del basic.o

g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -Iexamples\CPP\basic_circuits -c examples\CPP\basic_circuits\multiple_tests.cpp
g++ -o multiple_tests.exe *.o
del multiple_tests.o

g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -Iexamples\CPP\basic_circuits -c examples\CPP\basic_circuits\IEEE_14_bus.cpp
g++ -o IEEE_14_bus.exe *.o
del IEEE_14_bus.o

g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -Iexamples\CPP\basic_circuits -c examples\CPP\basic_circuits\perf.cpp
g++ -o perf.exe *.o
del perf.o

::---------------------------------
:: Control and subcircuit
::---------------------------------
g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -Iexamples\CPP\control_subcircuit -c examples\CPP\control_subcircuit\basic_subcircuit.cpp
g++ -o basic_subcircuit.exe *.o
del basic_subcircuit.o

g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -Iexamples\CPP\control_subcircuit -c examples\CPP\control_subcircuit\nested_subcircuit.cpp
g++ -o nested_subcircuit.exe *.o
del nested_subcircuit.o

g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -Iexamples\CPP -Iexamples\CPP\control_subcircuit -c examples\CPP\control_subcircuit\test_control.cpp
g++ -o test_control.exe *.o
del test_control.o

g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -Iexamples\CPP -Iexamples\CPP\control_subcircuit -c examples\CPP\control_subcircuit\test_control_and_subcircuit.cpp
g++ -o test_control_and_subcircuit.exe *.o
del test_control_and_subcircuit.o

::---------------------------------
:: Delete
::---------------------------------
g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -Iexamples\CPP\delete -c examples\CPP\delete\test_basic_delete.cpp
g++ -o test_basic_delete.exe *.o
del test_basic_delete.o

g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -Iexamples\CPP\delete -c examples\CPP\delete\test_delete.cpp
g++ -o test_delete.exe *.o
del test_delete.o

g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -Iexamples\CPP -Iexamples\CPP\delete -c examples\CPP\delete\test_delete_control.cpp
g++ -o test_delete_control.exe *.o
del test_delete_control.o

::---------------------------------
:: Component and node OFF
::---------------------------------
g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes  -Iexamples\CPP\comp_node_off -c examples\CPP\comp_node_off\test_comp_off.cpp
g++ -o test_comp_off.exe *.o
del test_comp_off.o

g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes  -Iexamples\CPP\comp_node_off -c examples\CPP\comp_node_off\test_node_off.cpp
g++ -o test_node_off.exe *.o
del test_node_off.o


:: end
del libCircuit.so
del *.o





