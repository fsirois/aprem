
:: Compile first
g++ -O3 -IC:\Eigen\Eigen -IC:\Python34\include -Isrc\includes -Isrc\Circuit\includes -c src\Circuit\*.cpp src\circuit_class\*.cpp src\circuit_elements\component_types\*.cpp src\circuit_elements\*.cpp src\ini_file_reader\iniFileReader.cpp 

:: Link second
g++ -shared -o libCircuit.so *.o -LC:\Python34\libs -lpython34

:: Delete all object files generated
del *.o

:: Move the library to the Circuit location
del src\Circuit\libCircuit.so
move libCircuit.so src\Circuit

