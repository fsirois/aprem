
:: Compile first
g++ -O3 -IC:\Eigen\Eigen -Isrc\includes -Isrc\circuit_interface\includes -c src\circuit_interface\*.cpp src\circuit_class\*.cpp src\circuit_elements\component_types\*.cpp src\circuit_elements\*.cpp src\ini_file_reader\iniFileReader.cpp 

:: Link second
g++ -shared *.o -o libCircuit.so

