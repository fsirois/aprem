
from myControl import myControl
from Circuit import Circuit

# Basic example on how to use a control
circuit = Circuit()

circuit.add('Vsrc', 'E_A', ['A','0'], [240, 0])
circuit.add('Imp', 'R_b', ['B','0'], 20 + 0j)
circuit.add('Imp', 'R_a', ['A','B'], 20 + 0j)

# Add a control
inputs = ['R_a.I']
outputs = ['R_b.Z']
functionData = [2]
functionName = 'myControl'
controlName = 'control1'
r = circuit.add('Control', controlName, functionName, [functionData, inputs, outputs])
print(r)

# Examples on how to get information on controls
circuit.print('Control');
circuit.getNames('Control')

circuit.solve()

print(circuit.get('R_a','I'))
print(circuit.get('R_a','Z')) # Z did not changed, it was not controlled
print(circuit.get('R_b','Z')) # Notice Z changed, because it was controlled

circuit.set('R_b', 'Z', 20 + 0j)
circuit.dele('control1')

circuit.solve()
print(circuit.get('R_a','I'))
print(circuit.get('R_a','Z')) # Z did not changed, it was not controlled
print(circuit.get('R_b','Z')) # Z did not change, control was deleted