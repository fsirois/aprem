
from Circuit import Circuit

# Examples on how to delete circuit elements
serie = Circuit()
serie.add('Imp', 'Z1', ['N1','N2'], 0.5 + 0.3j)
serie.add("Imp", "Z2", ["N2","N3"], 0.3 + 0.5j)
serie.set("IONodes", ["N1","N3"])

parallel = Circuit()
parallel.add('Subcircuit', 'Serie1', ['Node2', 'Node1'], serie)
parallel.add('Subcircuit', 'Serie2', ['Node2', 'Node1'], serie)
parallel.set('IONodes', ['Node2','Node1'])
del serie

c = Circuit();
c.add('Subcircuit', 'Par1', ['SupNode3','SupNode1'], parallel)
c.add('Subcircuit', 'Par2', ['SupNode1','SupNode2'], parallel)
c.add('Imp', 'Z3', ['SupNode3','SupNode2'], 0.3 + 0.5j)
c.set('IONodes', ['SupNode3','SupNode2'])
del parallel

master = Circuit()
master.add('Subcircuit', 'Sub1', ['0', 'NodeBeta'], c)
master.add('Subcircuit', 'Sub2', ['NodeBeta', 'NodeDelta'], c)
master.add('Subcircuit', 'Sub3', ['NodeBeta', 'NodeDelta'], c)
master.add('Vsrc', 'Vsrc', ['0', 'NodeDelta'], [120, 180])
del c

# BEFORE DELETE -----------------------------------------------------------
master.print('Comp')
master.print('Subcircuit')

# Solve before deletion
master.solve()
master.getNode('NodeBeta', 'V')
master.getNode('NodeAlpha', 'V')
master.get('Vsrc', 'I')

# DELETION  ---------------------------------------------------------------
# Option 1 - Delete components one by one to delete Sub2
master.dele('Sub2_Par1_Serie1_Z1')
master.dele('Sub2_Par1_Serie1_Z2')
master.dele(['Sub2_Par1_Serie2_Z1', 'Sub2_Par1_Serie2_Z2'])
master.dele(['Sub2_Par2_Serie1_Z1', 'Sub2_Par2_Serie1_Z2',
            'Sub2_Par2_Serie2_Z1', 'Sub2_Par2_Serie2_Z2'])
master.dele('Sub2_Z3')

# Option 2 - Delete a whole subcircuit
master.dele('Sub2')

# AFTER DELETE
master.print('Comp')
master.print('Subcircuit')

# Solve after deletion
master.solve();
print(master.getNode('NodeBeta', 'V'))
print(master.getNode('NodeAlpha', 'V'))
print(master.get('Vsrc', 'I'))


