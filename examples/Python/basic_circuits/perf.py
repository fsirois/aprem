# Simple example of a parametric analysis
# Initialization
from Circuit import Circuit
import time

c = Circuit()  # Our circuit object will be called "c"
c.setG3PHASE(0) # Means that each component is a single-phase device

# Creation of circuit (only 4 components here)
c.add("Vsrc", "E_A", ["A", "0"], [240, 0]);  # Voltage source

n_item = 3333 # it will be 3 imes more than that

print('Test for ' + str(3*n_item + 1) + ' items') 

name_list = [None] * 3 * n_item
para_list = ['Z'] * 3 * n_item
impedance = (10.0 + 10.0j)/n_item
val_list = [impedance] * 3 * n_item

start = time.time()
for i in range(n_item):
	name_list[3*i] = "Ra" + str(3*i)
	name_list[3*i+1] = "Rb" + str(3*i+1)
	name_list[3*i+2] = "Rc" + str(3*i+2)
	
	c.add("Imp", name_list[3*i], ["A", "B"], 0.75*impedance) # Impedance
	c.add("Imp", name_list[3*i+1], ["B", "C"], 0.5*impedance)
	c.add("Imp", name_list[3*i+2], ["C", "0"], 2*impedance)
print('Creating : ' + str(time.time() - start))

start = time.time()
c.setComp(name_list, para_list, val_list)   # Update the value of the resistance
print('Set  : ' + str(time.time() - start))

start = time.time()
c.solve()
print('First solve    : ' + str(time.time() - start))

start = time.time()
c.solve()
print('Other Solves    : ' + str(time.time() - start))

start = time.time()
val_list = c.getComp(name_list, para_list) 
print('Get  : ' + str(time.time() - start));


   
   
   
   
   
