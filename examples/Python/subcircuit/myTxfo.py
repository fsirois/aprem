from Circuit import Circuit

def myTxfo ( ratio , R1 , X1 , Rfe , Xphi , R2 , X2 ):
# TXFOREEL Creates an object Circuit corresponding to a model of
# real transformer with interface nodes.
#
# Parameters : ratio : Transformer ratio
#              R1    : Resistance of primary wire
#              X1    : Leak inductance of the primary
#              Rfe   : Iron leaks
#              Xphi  : Magnetization inductance
#              R2    : Resistance of secondary wire
#              X2    : Leak inductance of the secondary
#
# Return value : Circuit object corresponding to a real transformer

# Create circuit
	txfo = Circuit()

# Add components
# Impedance of the primary
	txfo.add('Imp', 'Z1_s', ['P_pos', 'A'], R1 + X1 * 1j)
 
# Iron and magnetization leaks
	txfo.add('Imp', 'Z1_p', ['A', 'P_neg'], Rfe * Xphi * 1j / (Rfe + Xphi * 1j))
 
# Ideal transformer
	txfo.add('Txfo', 'Txfo_ideal', ['A', 'P_neg', 'B', 'S_neg'], [ratio , 0])
 
# Impedance of the secondary
	txfo.add('Imp', 'Z2_s', ['S_pos', 'B'], R2 + X2*1j)
 
# Interface nodes are designated
	txfo.set('IONodes', ['P_pos', 'P_neg', 'S_pos', 'S_neg'])

	return txfo






