/* Basic example in C */

#include <stdio.h>
#include <vector>

#include "c_interface.h"

int main()
{
	/* Create new circuit using the "new" function
	 * possible to create a dummy struct to avoid handling object on the C side
	 * reinterpret_casts on the interface side would do the trick */
	circuit * c = Circuit_new(); 

	/* In C, necessity to handle char ** and malloc */
	const char ** node = (const char **) malloc(4 * sizeof(char*)); /* max 4 nodes */
	double * values = (double *) malloc(2 * sizeof(double)); /* max 2 values in this example */
	
	/* Then, necessity to pass the circuit pointer to each function call */
	node[0] = "A";
	node[1] = "0";
	values[0] = 240.0;
	values[1] = 0.0;
	Circuit_add_component(c, "Vsrc", "E_A", node, 2, values, 2);
	
	node[0] = "A";
	node[1] = "0";
	node[2] = "a";
	node[3] = "0";
	values[0] = 2.0;
	values[1] = 0.0;
	Circuit_add_component(c, "Txfo", "txfo_A", node, 4, values, 2);
	
	node[0] = "a";
	node[1] = "c";
	values[0] = 120.0;
	Circuit_add_component(c, "Imp", "R_a", node, 2, values, 2);
	
	node[0] = "c";
	node[1] = "0";
	values[0] = 120.0;
	Circuit_add_component(c, "Imp", "R_b", node, 2, values, 2);
	
	Circuit_print(c, "Comp");
	
	/* Do not forget to free memory unused
	 * this is the downside of needing mallocs */
	free(node);
	free(values);

	Circuit_solve(c);
	
	double out_real, out_imag;
	Circuit_get_nodeV(c, "a", &out_real, &out_imag);
	printf("\n %f %f", out_real, out_imag);
	
	/* size of output needed hard to know in advance !!
	 * unless the interface mallocs memory and it is freed here... */
	double * out = (double *) malloc(20 * sizeof(double)) ; 
	
	/* node number */
	int n_out = Circuit_get_node(c, "a", "no", out);
	printf("\n");
	for (int i=0; i<n_out; i++)
	{
		printf(" %f", out[i]);
	}
	
	/* do not forget to free memory */
	free(out);
	
	/* manually delete the circuit
	 * this is the other downside of using a C interface */
	Circuit_delete(c);
	
	return 0;
}