% Basic test on setting a component to on and off
c = Circuit();

c.add('Isrc', 'Isrc1', {'A','0'}, {1, 12});
c.add('Imp', 'Imp1', {'A','0'}, 0.5 + 0.5i);
c.add('Imp', 'Imp2', {'A', '0'}, 0.5 + 0.5i);

% Example on how to get the names of components in the circuit
% c.getNames('Node')
% c.getNames('Comp')

% Solve the circuit and get Voltage of Imp1
c.solve();
c.get('Imp1', 'V')

% Set Imp2 off, solve and get again Imp1, notice V changed
c.set('Imp2', 'on', 0);
c.solve();
c.get('Imp1', 'V')

% Set Imp2 on again, V is back to it's formed value
c.set('Imp2', 'on', 1);
c.solve();
c.get('Imp1', 'V')

