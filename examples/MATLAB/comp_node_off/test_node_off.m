% Basic test on setting a node off
c = Circuit();

c.add('Isrc', 'I1', {'A','0'}, {1, 12});
c.add('Imp', 'Imp1', {'A','B'}, 0.5 + 0.5i);
c.add('Imp', 'Imp2', {'B', '0'}, 0.5 + 0.5i);
c.add('Imp', 'Imp3', {'A','C'}, 0.5 + 0.5i);
c.add('Imp', 'Imp4', {'C', '0'}, 0.5 + 0.5i);

% Step 1 : Solve for current circuit
c.solve();
c.get('Imp1', 'I') 
% c.getNames('Node') % Notice the 3 nodes

% Step 2 : Deativate node
c.setNode('C', 'on', 0);
c.solve();
c.get('Imp1', 'I') % Notice the current value changed.
% c.getNames('Node') % Notice the 2 "fragment" nodes created. When a node is
                     % is set off, "fragment" nodes are created and the
                     % components linked to the original nodes are linked
                     % to those fragments. The components are now unlinked.

% Step 3 : Reactivate node
% When setting node 'on' again. The components are Linked back to the
% original node, thus connecting themselves together.
c.setNode('C', 'on', 1); 
c.solve();
c.get('Imp1', 'I') % Notice the current value is back to original value