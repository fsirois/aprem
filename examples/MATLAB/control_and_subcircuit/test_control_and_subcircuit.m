% Test
controlled_circuit = Circuit();

controlled_circuit.add('Imp', 'R_a', {'A','B'}, 20 + 3i)
controlled_circuit.add('Imp', 'R_b', {'C','B'}, 20 + 3i)
controlled_circuit.add('Imp', 'R_c', {'A','C'}, 20 + 3i)

inputs = {'R_c.I'};
outputs = {'R_b.Z'};
functionData = {1};
controlled_circuit.add('Control', 'control1', 'myControl', {functionData, inputs, outputs})
controlled_circuit.set('IONodes', {'A','B'});
controlled_circuit.print('Control');

two_controlled = Circuit();
two_controlled.add('Subcircuit', 'sub1', {'n1', 'n2'}, controlled_circuit);
two_controlled.add('Subcircuit', 'sub2', {'n1', 'n2'}, controlled_circuit);
two_controlled.set('IONodes', {'n1','n2'});

two_controlled.print('Comp');
two_controlled.print('Nodes');
two_controlled.print('Control');
two_controlled.print('Subcircuit');

main_circuit = Circuit();
main_circuit.add('Subcircuit', 'main1', {'p1', '0'}, two_controlled);
main_circuit.add('Subcircuit', 'main2', {'p2', '0'}, two_controlled);
main_circuit.add('Imp', 'R1', {'p1','p2'}, 20i);
main_circuit.add('Vsrc', 'V1', {'p1','0'}, {220, 0});

main_circuit.print('Comp');
main_circuit.print('Nodes');
main_circuit.print('Subcircuit');
main_circuit.print('Control');

main_circuit.solve();

main_circuit.get('main1_sub1_R_a','I')
main_circuit.get('main1_sub1_R_c','I')
main_circuit.get('main1_sub1_R_a','Z')
main_circuit.get('main1_sub1_R_b','Z')

clear;


