
% Examples on how to delete circuit elements
circuit = Circuit()

circuit.add('Vsrc', 'Source', {'A', '0'}, {240, 0});  
circuit.add('Imp', 'Z1', {'A', 'B'}, 1 + 1j);
circuit.add('Imp', 'Z2', {'B', '0'}, 1 + 1j);
circuit.add('Imp', 'Z3', {'A', '0'}, 2 + 2j);

circuit.solve();
circuit.get('Source', 'I')

circuit.del('Z3')

circuit.solve()
circuit.get('Source', 'I')


