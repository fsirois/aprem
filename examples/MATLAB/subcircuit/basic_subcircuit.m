% Basic example on how to use subcircuits
c = Circuit();

c.add('Vsrc', 'Vsrc', {'0', 'V1'}, {14400, 0});
c.add('Imp', 'ZVsrc', {'V1', 'V2'}, 3 + 4*1i);

% myTxfo creates a circuit on its own, with interface nodes defined. See
% file myTxfo.m to see how the interface nodes are marked.
monTxfo = myTxfo(120, 0.3, 0.1, 900, 1000, 0.2, 0.2);

% add the circuit to c
c.add('Subcircuit', 'Txfo', {'0', 'V2', '0', 'VBus'}, monTxfo);

% Add another component to the circuit
c.add('PQ', 'PQ', {'VBus', '0'}, {1e3, 1e2, 14400/120});

% Get information on the subcircuits
% c.print('Subcircuit');
% c.getNames('Subcircuit')

% Solve normally
c.solve();

c.get('ZVsrc','I')
c.get('PQ','S')