% An example of nested subcircuits

% Level 1 circuits
serie = Circuit();
serie.add('Imp', 'Z1', {'N1','N2'}, 0.5 + 0.3i);
serie.add('Imp', 'Z2', {'N2','N3'}, 0.3 + 0.5i);
serie.set('IONodes', {'N1','N3'});

% Level 2 circuits
parallel = Circuit();
parallel.add('Subcircuit', 'Serie1', {'Node2', 'Node1'}, serie);
parallel.add('Subcircuit', 'Serie2', {'Node2', 'Node1'}, serie);
parallel.set('IONodes', {'Node2','Node1'});
clear serie; % Clear serie to show 'parallel' works on it's own.

% Level 3 circuit
c = Circuit();
c.add('Subcircuit', 'Par1', {'SupNode3','SupNode1'}, parallel);
c.add('Subcircuit', 'Par2', {'SupNode1','SupNode2'}, parallel);
c.add('Imp', 'Z3', {'SupNode3','SupNode2'}, 0.3 + 0.5i);
c.set('IONodes', {'SupNode3','SupNode2'});
clear parallel; % Clear parallel to show 'c' works on it's own.

% Level 4 circuit
master = Circuit();
master.add('Subcircuit', 'Sub1', {'0', 'NodeBeta'}, c);
master.add('Subcircuit', 'Sub2', {'NodeBeta', 'NodeDelta'}, c);
master.add('Subcircuit', 'Sub3', {'NodeBeta', 'NodeDelta'}, c);
master.add('Vsrc', 'Vsrc', {'0', 'NodeDelta'}, {120, 180});
clear c; % Clear c to show 'master' works on it's own

% Print the result
% Notice the auto-generated names for components and subcircuits
% Also notice that each subcircuits only knows one level below it.
master.print('Comp');
master.print('Subcircuit');

% Solve normally and get data
master.solve();
master.getNode('NodeBeta', 'V')
master.getNode('NodeAlpha', 'V')
master.get('Vsrc', 'I')

