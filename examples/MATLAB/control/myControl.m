function [newStatus, flag] = myControl(inputs, fctData, outputs)

newStatus{1} = outputs{1};
if (inputs{1} > fctData{1})
    newStatus{1} = 2 * outputs{1};
end

if (inputs{1} < fctData{1})
    flag = 1;
else
    flag = 0;
end