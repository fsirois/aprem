% Basic example on how to use a control
circuit = Circuit();

circuit.add('Vsrc', 'E_A', {'A','0'}, {240, 0})
circuit.add('Imp', 'R_b', {'B','0'}, 20 + 0i)
circuit.add('Imp', 'R_a', {'A','B'}, 20 + 0i)

% Add a control
inputs = {'R_a.I'};
outputs = {'R_b.Z'};
functionData = {1};
functionName = 'myControl';
controlName = 'control1';
circuit.add('Control', controlName, functionName, {functionData, inputs, outputs})

% Examples on how to get information on controls
% circuit.print('Control');
% circuit.getNames('Control')

circuit.solve();

circuit.get('R_a','I')
circuit.get('R_a','Z') % Z did not changed, it was not controlled
circuit.get('R_b','Z') % Notice Z changed, because it was controlled
clear all;