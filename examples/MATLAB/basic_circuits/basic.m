% Simple example of a parametric analysis
% Initialization
c = Circuit();  % Our circuit object will be called "c"
c.setG3PHASE(0); % Means that each component is a single-phase device



% Creation of circuit (only 4 components here)
c.add('Vsrc', 'E_A', {'A', '0'}, {240, 0});           % Voltage source
c.add('Txfo','txfo_A', {'A', '0', 'a', '0'}, {2, 0}); % Transformer
c.add('Imp', 'R_a', {'a', 'c'}, 120);                 % Impedance
c.add('Imp', 'R_b', {'c','0'}, 120);
% c.add('Imp', 'R_a', {'A','0'}, 120);
% We prepare the case 
R=[120 60 40 30 25 20];    % Values of resistance to be used as the load
S=zeros(size(R));

% Let's run the loop
for i=1:length(R)
   c.set('R_a','Z',R(i));   % Update the value of the resistance
   c.get('R_a','Z');
   c.solve();                 % Execute the load flow (here no PQ load is present, so it is a linear system that is solved)
   S(i)=c.get('R_a','S'); % Get the power (S=P+jQ) in the resistor (comes out as a complex number)
end

% Print example
c.print('Nodes');
c.print('Comp');

% Let's plot P (=real(S)) as a function of R
plot(R, real(S));
title ('P vs R');
xlabel('R (\Omega)');
ylabel('P (W)');
grid on;
