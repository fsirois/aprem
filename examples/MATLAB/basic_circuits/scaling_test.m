% Simple example of a parametric analysis

c = Circuit();  % Our circuit object will be called "c"
c.setIs3Phase(0); % Means that each component is a single-phase device

% Creation of circuit (only 4 components here)
c.add('Vsrc', 'E_A', {'A', '0'}, {240, 0});  % Voltage source

n_item = 3333;
n_iter = 1;

name_list = cell(3 * n_item, 1);
para_list = cell(3 * n_item, 1);
for i = 1:n_item*3
    para_list{i} = 'Z';
end
impedance = (10.0 + 10.0j)/n_item;
val_list = zeros(3 * n_item, 1);
val_list(1:end) = impedance;

tic;
for i = 1:n_item
	name_list{3*i-2} = ['Ra',num2str(3*i-2)];
	name_list{3*i-1} = ['Rb',num2str(3*i-1)];
	name_list{3*i} = ['Rc',num2str(3*i)];
	
	c.add('Imp', name_list{3*i-2}, {'A', 'B'}, 0.75*impedance); % Impedance
	c.add('Imp', name_list{3*i-1}, {'B', 'C'}, 0.5*impedance);
	c.add('Imp', name_list{3*i}, {'C', '0'}, 2*impedance);
end
toc;

%c.print('Comp');

tic;
for j=1:length(name_list)
   c.set(name_list{j}, para_list{j}, val_list(j));
end
toc;

tic;
c.solve()
toc;
   
   
   
   