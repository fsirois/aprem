% IEEE 14 bus for APREM tests
% All values are in PU

tic;
IEEE_14bus = Circuit();
IEEE_14bus.setIs3Phase(0); % Set to 1 phase circuit (not necessary)

% 2 synchronous machines
%Bus1 = generator bus
IEEE_14bus.add('PV', 'G1', {'Bus1','0'}, {2.32, 0.97});
%Bus2 = Slack bus
IEEE_14bus.add('Vsrc', 'G2', {'Bus2','0'}, {1, 0});

% 11 charges of total power 259 MW and 81.3 Mvar
% All nominal voltage to 1 pu
IEEE_14bus.add('PQ','Ch1', {'Bus2','0'}, {0.2170, 0.1270, 1});
IEEE_14bus.add('PQ', 'Ch2', {'Bus3','0'}, {0.9420, 0.1900, 1});
IEEE_14bus.add('PQ', 'Ch3', {'Bus4','0'}, {0.4780, 0, 1});
IEEE_14bus.add('PQ', 'Ch4', {'Bus5','0'}, {0.0760, 0.0160, 1});
IEEE_14bus.add('PQ', 'Ch5', {'Bus6','0'}, {0.1120, 0.0750, 1});
IEEE_14bus.add('PQ', 'Ch6', {'Bus9','0'}, {0.2950, 0.1660, 1});
IEEE_14bus.add('PQ', 'Ch7', {'Bus10','0'}, {0.0900, 0.0580, 1});
IEEE_14bus.add('PQ', 'Ch8', {'Bus11','0'}, {0.0350, 0.0180, 1});
IEEE_14bus.add('PQ', 'Ch9', {'Bus12','0'}, {0.0610, 0.0160, 1});
IEEE_14bus.add('PQ', 'Ch10', {'Bus13','0'}, {0.1350, 0.0580, 1});
IEEE_14bus.add('PQ', 'Ch11', {'Bus14','0'}, {0.1490, 0.0500, 1});

% 2 transformers of real ratio and serial reactance
IEEE_14bus.add('Txfo', 'T1', {'Bus5','0','I1','0'}, {0.932, 0});
IEEE_14bus.add('Txfo', 'T2', {'Bus4','0','I2','0'}, {0.969, 0});
IEEE_14bus.add('Imp', 'Imp_T1', {'I1','Bus6'}, 0.25202j);
IEEE_14bus.add('Imp', 'Imp_T2', {'I2','Bus9'}, 0.55618j);

% 15 lines with pi model

% Serial
IEEE_14bus.add('Imp', 'L1', {'Bus1','S1'}, 0.01938 + 0.05917j);
IEEE_14bus.add('Imp', 'L2', {'Bus1','Bus5'}, 0.05403 + 0.22304j);
IEEE_14bus.add('Imp', 'L3', {'Bus2','Bus3'}, 0.04699 + 0.19797j);
IEEE_14bus.add('Imp', 'L4', {'Bus2','Bus4'}, 0.05811 + 0.17632j);
IEEE_14bus.add('Imp', 'L5', {'Bus2','Bus5'}, 0.05695 + 0.17388j);
IEEE_14bus.add('Imp', 'L6', {'Bus3','Bus4'}, 0.06701 + 0.17103j);
IEEE_14bus.add('Imp', 'L7', {'Bus4','Bus5'}, 0.01335 + 0.04211j);
IEEE_14bus.add('Imp', 'L8', {'Bus6','Bus11'}, 0.09498 + 0.1989j);
IEEE_14bus.add('Imp', 'L9', {'Bus6','Bus12'}, 0.12291 + 0.25581j);
IEEE_14bus.add('Imp', 'L10', {'Bus6','Bus13'}, 0.06615 + 0.13027j);
IEEE_14bus.add('Imp', 'L11', {'Bus9','Bus10'}, 0.03181 + 0.08450j);
IEEE_14bus.add('Imp', 'L12', {'Bus10','Bus11'}, 0.08205 + 0.19207j);
IEEE_14bus.add('Imp', 'L13', {'Bus12','Bus13'}, 0.22092 + 0.19988j);
IEEE_14bus.add('Imp', 'L14', {'Bus13','Bus14'}, 0.17093 + 0.34802j);
IEEE_14bus.add('Imp', 'L15', {'Bus9','Bus14'}, 0.12711 + 0.27038j);

%Shunt
IEEE_14bus.add('Imp', 'B1', {'Bus1','0'}, 1j*(2/0.0528));
IEEE_14bus.add('Imp', 'B2', {'Bus2','0'}, 1j*(2/0.0528));
IEEE_14bus.add('Imp', 'B3', {'Bus1','0'}, 1j*(2/0.0492));
IEEE_14bus.add('Imp', 'B4', {'Bus5','0'}, 1j*(2/0.0492));
IEEE_14bus.add('Imp', 'B5', {'Bus2','0'}, 1j*(2/0.0438));
IEEE_14bus.add('Imp', 'B6', {'Bus3','0'}, 1j*(2/0.0438));
IEEE_14bus.add('Imp', 'B7', {'Bus2','0'}, 1j*(2/0.0374));
IEEE_14bus.add('Imp', 'B8', {'Bus4','0'}, 1j*(2/0.0374));
IEEE_14bus.add('Imp', 'B9', {'Bus2','0'}, 1j*(2/0.034));
IEEE_14bus.add('Imp', 'B10', {'Bus5','0'}, 1j*(2/0.034));
IEEE_14bus.add('Imp', 'B11', {'Bus3','0'}, 1j*(2/0.0346));
IEEE_14bus.add('Imp', 'B12', {'Bus4','0'}, 1j*(2/0.0346));
IEEE_14bus.add('Imp', 'B13', {'Bus4','0'}, 1j*(2/0.0128));
IEEE_14bus.add('Imp', 'B14', {'Bus5','0'}, 1j*(2/0.0128));

% 1 switch closed for the test
IEEE_14bus.add('Sw', 'Sw1', {'S1','Bus2'}, 1);



%--------------------------------------------------------------------------
% IMPORTANT : Test on deleting components
%--------------------------------------------------------------------------
% To see the effect of the delete, uncomment the next line
% Node S1 is deleted automatically since not used anymore
% IEEE_14bus.del({'L1','Sw1'});

% IEEE_14bus.print('Comp');
IEEE_14bus.solve();
IEEE_14bus.print('Nodes');

% Always apply the same value, just for test (and time test)
for i = 1:10, P(i) = 0.0760; end;
for i = 1:10, V(i) = 0.97; end;
for i = 1:10, angle(i) = 25; end;
for i = 1:10, Q(i) = 0.0160; end;
total = length(P)*length(V)*length(angle)*length(Q);

for i=1:length(P)
    
    IEEE_14bus.set('Ch4','P',P(i));   % Update the value of the resistance
    for j=1:length(V)
        IEEE_14bus.set('G1','V',V(i));
        for k=1:length(angle)
            IEEE_14bus.set('G2','angle',angle(i));
            for l=1:length(Q)
                IEEE_14bus.set('Ch9','Q',Q(i));
                IEEE_14bus.solve();
                
                actual = l + length(Q)*(k-1) + length(Q)*length(angle)*(j-1) + length(Q)*length(angle)*length(V)*(i-1);
                fprintf('Actual : %d out of %d\n',actual, total);
                
                % get()
                IEEE_14bus.get('Ch8','S');
                IEEE_14bus.get('Ch8','Vb');
                IEEE_14bus.get('Ch8','I');
                IEEE_14bus.get('G1','S');
                IEEE_14bus.get('G1','Vb');
                IEEE_14bus.get('G1','I');
                IEEE_14bus.get('G2','S');
                IEEE_14bus.get('G2','Vb');
                IEEE_14bus.get('G2','I');
                IEEE_14bus.get('L4','S');
                IEEE_14bus.get('L4','Vb');
                IEEE_14bus.get('L4','I');
                IEEE_14bus.get('B2','S');
                IEEE_14bus.get('B2','Vb');
                IEEE_14bus.get('B2','I');
                IEEE_14bus.get('T1','S');
                IEEE_14bus.get('T1','Vb');
                IEEE_14bus.get('T1','I');
                IEEE_14bus.get('Sw1','S');
                IEEE_14bus.get('Sw1','Vb');
                IEEE_14bus.get('Sw1','I');

                % getNode()
                IEEE_14bus.getNode('Bus1','V');
                IEEE_14bus.getNode('Bus2','V');
                IEEE_14bus.getNode('Bus3','V');
                IEEE_14bus.getNode('Bus4','V');
                IEEE_14bus.getNode('Bus5','V');
                IEEE_14bus.getNode('Bus6','V');
                IEEE_14bus.getNode('Bus9','V');
                IEEE_14bus.getNode('Bus10','V');
                IEEE_14bus.getNode('Bus11','V');
                IEEE_14bus.getNode('Bus12','V');
                IEEE_14bus.getNode('Bus13','V');
                IEEE_14bus.getNode('Bus14','V');
                IEEE_14bus.getNode('I1','V');
                IEEE_14bus.getNode('I2','V');
                IEEE_14bus.getNode('S1','V');
            end
        end
    end
end

toc;
clear;
