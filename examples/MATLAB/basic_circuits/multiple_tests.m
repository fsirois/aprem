%--------------------------------------------------------------------------
% Multiple tests to validate the integrity of APREM
%--------------------------------------------------------------------------

% Test 1 - Perfomance + automatic name creation ---------------------------
tic
c = Circuit();

for i=1:1000
    c.add('Vsrc', 'XX',{'A','0'}, {240, 0});
end
a = c.getNames('Comp')
toc % Exactly 0.0966 s on my machine (test on speed)


% Test 2 - Name is returned -----------------------------------------------
keyboard
impname = c.add('Imp', 'XX',{'A','0'}, 0.5 + 0.5i);
disp(impname);


% Test 3 - Bad calls ------------------------------------------------------
keyboard
% c.set('BadName', 'Z', 0.5)    % error (no big crash)
% c.set(impname, 'BadAtt', 0.5) % error (no big crash)
% c.set(impname, 'Z', 0.5, 5)   % error (no big crash)
c.get('BadName', 'V')         % For the moment, empty matrix 1-by-0
c.get(impname, 'BadAtt')      % For the moment, returns 0
% c.get(impname, 'Z', 'bad')    % For the moment, error (no big crash)


% Test 4 - Memory handling tests - make sure everything is ok -------------
% This is a more advanced example, but it is useful
keyboard
c1 = Circuit();
c1.add('Imp', 'Imp1', {'A','0'}, 0.5);

c2 = c1;           % Creates a reference to c1;
c3 = Circuit(c1);  % Creates a deep copy (programmed in APREM)

c1.add('Imp', 'Imp2', {'B', '0'}, 0.5i);

clear c1; % Since c2 has a reference on c1, the circuit is not deleted,
          % just the c1 reference.
          
fprintf('\n-----\n');
c2.getNames('Comp') % Notice Imp2 is printed, since c2 was a reference to
                    % the same circuit as c1
                  
clear c2; % The c1 circuit is deleted, since the 2 references c2 and c1 are
          % now deleted. APREM's handle destructor called.
fprintf('\n-----\n');
c3.getNames('Comp') % now, only the c3 circuit exists, notice that Imp2 is
                    % not printed, since c3 is a circuit by itself because
                    % of the deep copy.
                  
clear;     % APREM's handle destructor called again, for c3 this time
% clear all; % Calls the garbage collector, that calls the destructor of c3



