
#include <stdio.h>
#include <vector>

#include "CircuitFactory.h"

using namespace std;

int main()
{
	// Examples on how to delete circuit elements
	CircuitFactory circuitFactory;
	CircuitInterface* circuit = circuitFactory.CreateCircuit();
	
	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	node[0] = "A";
	node[1] = "0";
	values[0] = 240.0;
	values[1] = 0.0;
	circuit->addComponent("Source", node, values, "Vsrc");
	
	node[0] = "A";
	node[1] = "B";
	values[0] = 1.0;
	values[1] = 1.0;
	circuit->addComponent("Z1", node, values, "Imp");
	
	node[0] = "B";
	node[1] = "0";
	circuit->addComponent("Z2", node, values, "Imp");
	
	node[0] = "A";
	node[1] = "0";
	values[0] = 2.0;
	values[1] = 2.0;
	circuit->addComponent("Z3", node, values, "Imp");

	// solve a first time
	circuit->solve();
	vector<pair<double,double> > out = circuit->get("Source","I");
	printf("\n%f %f",out[0].first,out[0].second);

	// delete
	vector<string> toDelete;
	toDelete.push_back("Z3");
	circuit->deleteElements(toDelete);

	// solve a second time and see the difference
	circuit->solve();
	out = circuit->get("Source","I");
	printf("\n%f %f",out[0].first,out[0].second);
	
	return 0;
}




