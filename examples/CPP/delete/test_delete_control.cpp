
#include <stdio.h>

#include "CircuitFactory.h"

#include "controlFunctions.cpp" // to change to controlFunctions.h

#include <vector>

using namespace std;

int main()
{
	// Basic example on how to delete a control
	CircuitFactory circuitFactory;
	
	CircuitInterface* circuit = circuitFactory.CreateCircuit();

	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	node[0] = "A";
	node[1] = "0";
	values[0] = 240.0;
	values[1] = 0.0;
	circuit->addComponent("E_A", node, values, "Vsrc");
	
	node[0] = "B";
	node[1] = "0";
	values[0] = 20.0;
	values[1] = 0.0;
	circuit->addComponent("R_b", node, values, "Imp");
	
	node[0] = "A";
	node[1] = "B";
	circuit->addComponent("R_a", node, values, "Imp");
	
	// weird crash - to investigate
	circuit->solve();
	
	// Add a control
	vector<string> inputComponents;
	inputComponents.push_back("R_a");
	
	vector<string> intputAttributes;
	intputAttributes.push_back("I");
	
	vector<string> outputComponents;
	outputComponents.push_back("R_b");
	
	vector<string> outputAttributes;
	outputAttributes.push_back("Z");
	
	vector<double> functionData;
	functionData.push_back(2.0);
	
	circuit->addControl(inputComponents,
						intputAttributes,
						outputComponents,
						outputAttributes,
						functionData,
						"myControl",
						"control1",
						&myControl);

	// Examples on how to get information on controls
	circuit->print("Control");
	vector<string> names = circuit->getNames("Control");
	for (int i=0; i<names.size(); i++)
	{
		printf("\n %s \n", names[i].c_str());
	}
	
	circuit->print("Nodes");
	circuit->print("Comp");
	circuit->print("Control");

	circuit->solve();
	
	vector<pair<double, double> > out = circuit->get("R_a","I");
	for (int i=0; i<out.size(); i++)
	{
		printf("\n %f,%f", out[i].first, out[i].second);
	}
	
	// Z did not changed, it was not controlled
	out = circuit->get("R_a","Z");
	for (int i=0; i<out.size(); i++)
	{
		printf("\n %f,%f", out[i].first, out[i].second);
	}
	
	// Notice Z changed, because it was controlled
	out = circuit->get("R_b","Z");
	for (int i=0; i<out.size(); i++)
	{
		printf("\n %f,%f", out[i].first, out[i].second);
	}
	
	// reset value
	circuit->set("R_b", "Z", 20.0, 0.0);
	
	//--------------
	// DELETE
	//---------------
	vector<string> toDelete;
	toDelete.push_back("control1");
	circuit->deleteElements(toDelete);

	circuit->solve();

	out = circuit->get("R_a","I");
	for (int i=0; i<out.size(); i++)
	{
		printf("\n %f,%f", out[i].first, out[i].second);
	}
	
	// Z did not changed, it was not controlled
	out = circuit->get("R_a","Z");
	for (int i=0; i<out.size(); i++)
	{
		printf("\n %f,%f", out[i].first, out[i].second);
	}
	
	// Notice Z changed, because it was controlled
	out = circuit->get("R_b","Z");
	for (int i=0; i<out.size(); i++)
	{
		printf("\n %f,%f", out[i].first, out[i].second);
	}

	return 0;
}