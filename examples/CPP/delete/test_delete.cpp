
#include <stdio.h>
#include <vector>

#include "CircuitFactory.h"

using namespace std;

int main()
{
	//----------------------------------
	// First create a multilevel complex circuit
	//----------------------------------
	CircuitFactory CircuitFactory;
	
	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	// Level 1 circuits
	CircuitInterface * serie = CircuitFactory.CreateCircuit();
	
	node[0] = "N1";
	node[1] = "N2";
	values[0] = 0.5;
	values[1] = 0.3;
	serie->addComponent("Z1", node, values, "Imp");

	node[0] = "N2";
	node[1] = "N3";
	values[0] =  0.3;
	values[1] = 0.5;
	serie->addComponent("Z2", node, values, "Imp");
	
	node[0] = "N1";
	serie->setIONodes(node);

	// Level 2 circuits
	CircuitInterface * parallel = CircuitFactory.CreateCircuit();
	
	node[0] = "Node1";
	node[1] = "Node2";
	parallel->addSubcircuit(serie, "Serie1", node);
	
	node[0] = "Node2";
	node[1] = "Node1";
	parallel->addSubcircuit(serie, "Serie2", node);

	parallel->setIONodes(node);

	// Level 3 circuit
	CircuitInterface * c = CircuitFactory.CreateCircuit();
	
	node[0] = "SupNode3";
	node[1] = "SupNode1";
	c->addSubcircuit(parallel, "Par1", node);
	
	node[0] = "SupNode1";
	node[1] = "SupNode2";
	c->addSubcircuit(parallel, "Par2", node);
	
	node[0] = "SupNode3";
	values[0] =  0.3;
	values[1] = 0.5;
	c->addComponent("Z3", node, values, "Imp");
	
	c->setIONodes(node);
	
	// Level 4 circuit
	CircuitInterface * master = CircuitFactory.CreateCircuit();
	
	node[0] = "0";
	node[1] = "NodeBeta";
	master->addSubcircuit(c, "Sub1", node);
	
	node[0] = "NodeBeta";
	node[1] = "NodeDelta";
	master->addSubcircuit(c, "Sub2", node);
	
	node[0] = "NodeBeta";
	node[1] = "NodeDelta";
	master->addSubcircuit(c, "Sub3", node);
	
	node[0] = "0";
	values[0] = 120.0;
	values[1] = 180.0;
	master->addComponent("Vsrc", node, values, "Vsrc");
	
	//--------------------------------
	// Add the delete function tests
	//--------------------------------
	// BEFORE DELETE -----------------------------------------------------------
	//master->print("Comp");
	master->print("Subcircuit");

	// Solve before deletion
	master->solve();
	
	pair<double,double> outPair = master->getNodeV("NodeBeta");
	printf("\n%f %f\n",outPair.first,outPair.second);
	
	outPair = master->getNodeV("NodeDelta");
	printf("\n%f %f\n",outPair.first,outPair.second);
	
	vector<pair<double,double> > out = master->get("Vsrc", "I");
	printf("\n%f %f",out[0].first,out[0].second);

	// DELETION  ---------------------------------------------------------------
	vector<string> toDelete;
		
	// Option 1 - Delete components one by one to delete Sub2
	toDelete.push_back("Sub2_Par1_Serie1_Z1");
	master->deleteElements(toDelete);
	
	toDelete[0] = "Sub2_Par1_Serie1_Z2";
	master->deleteElements(toDelete);
	
	toDelete[0] = "Sub2_Par1_Serie2_Z1";
	toDelete.push_back("Sub2_Par1_Serie2_Z2");
	master->deleteElements(toDelete);
	
	toDelete[0] = "Sub2_Par2_Serie1_Z1";
	toDelete[1] = "Sub2_Par2_Serie1_Z2";
	toDelete.push_back("Sub2_Par2_Serie2_Z1");
	toDelete.push_back("Sub2_Par2_Serie2_Z2");
	master->deleteElements(toDelete);

	toDelete.resize(1);
	toDelete[0] = "Sub2_Z3";
	master->deleteElements(toDelete);

	// Option 2 - Delete a whole subcircuit
	/*toDelete.resize(1);
	toDelete[0] = "Sub2";
	master->deleteElements(toDelete);*/

	// AFTER DELETE ----------------------------------------------------------
	//master->print("Comp");
	master->print("Subcircuit");

	// Solve after deletion
	master->solve();
	
	outPair = master->getNodeV("NodeBeta");
	printf("\n%f %f\n",outPair.first,outPair.second);
	
	outPair = master->getNodeV("NodeDelta");
	printf("\n%f %f\n",outPair.first,outPair.second);
	
	out = master->get("Vsrc", "I");
	printf("\n%f %f",out[0].first,out[0].second);
		
	return 0;
}


