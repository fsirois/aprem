
using namespace std;

#include <stdio.h> // to remove
#include <vector>
#include <utility>

bool myControl(const vector<pair<double, double> > & inputs,
			   const vector<double> & fctData,
			   const vector<pair<double, double> > & outputs,
					 vector<pair<double, double> > & newStatus)
{
	newStatus[0] = outputs[0];
	if (inputs[0].first > fctData[0])
	{
	   newStatus[0].first = 2.0 * outputs[0].first;
	}

	return (inputs[0].first < fctData[0]);		 
}

bool otherControl(const vector<pair<double, double> > & inputs,
			      const vector<double> & fctData,
			      const vector<pair<double, double> > & outputs,
					    vector<pair<double, double> > & newStatus)
{
	newStatus[0] = outputs[0];
	if (inputs[0].first > fctData[0])
	{
	   newStatus[0].first = 1.5 * outputs[0].first;
	}

	return (inputs[0].first < fctData[0]);		 
}

// and so on...
