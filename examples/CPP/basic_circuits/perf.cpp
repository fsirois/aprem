
#include <stdio.h>

#include "CircuitFactory.h"

#include <vector>
#include <time.h>
#include <utility>
#include <string>
#include <stdlib.h>


using namespace std;

int main()
{
	CircuitFactory circuitFactory;
	
	CircuitInterface* c = circuitFactory.CreateCircuit();
	
	// Creation of circuit (only 4 components here)
	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	node[0] = "A";
	node[1] = "0";
	values[0] = 240.0;
	values[1] = 0.0;
	c->addComponent("E_A", node, values, "Vsrc"); // Voltage source
	
	int n_item = 3333; // it will be 3 imes more than that
	
	printf("\nTest for %d items",3*n_item + 1);
		
	vector<string> name_list;
	name_list.resize(3 * n_item);
	
	vector<string> para_list;
	para_list.resize(3 * n_item);
	for (int i=0; i<para_list.size(); i++)
	{
		para_list[i] = "Z";
	}
	
	pair<double,double> impedance;
	impedance.first = 10.0/n_item;
	impedance.second = 10.0/n_item;
	
	vector<pair<double,double> > val_list;
	val_list.resize(3 * n_item);
	for (int i=0; i<val_list.size(); i++)
	{
		val_list[i] = impedance;
	}

	// creating the big circuit
	clock_t begin_time = clock();
	for (int i=0; i<n_item; i++)
	{
		name_list[3*i].append("Ra");
		name_list[3*i].append(circuit_ns::IntToString(3*i));
		
		name_list[3*i+1].append("Rb");
		name_list[3*i+1].append(circuit_ns::IntToString(3*i+1));
		
		name_list[3*i+2].append("Rc");
		name_list[3*i+2].append(circuit_ns::IntToString(3*i+2));
		
		vector<double> imp;
		imp.resize(2);
		imp[0] = impedance.first;
		imp[1] = impedance.second;
		
		vector<string> node;
		node.resize(2);
		node[0] = "A";
		node[1] = "B";
		c->addComponent(name_list[3*i], node, imp, "Imp"); // Impedance
		
		node[0] = "B";
		node[1] = "C";
		c->addComponent(name_list[3*i+1], node, imp, "Imp");
		
		node[0] = "C";
		node[1] = "0";
		c->addComponent(name_list[3*i+2], node, imp, "Imp");
	}
	printf("\nCreating : %.0f ms", 1000 * double(clock () -  begin_time)  /  CLOCKS_PER_SEC);

	// Test of setting values
	begin_time = clock();
	int n = 100;
	for (int i = 0; i<n; i++)
		c->setComponents(name_list, para_list, val_list);  // Update the value of the resistance
	printf("\nSet : %.3f ms", 10 * double( clock () - begin_time ) /  (CLOCKS_PER_SEC));

	// test of solve ALPHA
	begin_time = clock();
	c->solve();
	printf("\nSolve : %.0f ms", 1000 * double( clock () - begin_time)  /  CLOCKS_PER_SEC);

	// test for solve other times
	begin_time = clock();
	for (int i = 0; i<n; i++)
		c->solve();
	printf("\nOther solve : %.3f ms", 10 * double( clock () - begin_time ) /  (CLOCKS_PER_SEC));
	
	// test for getting values
	begin_time = clock();
	for (int i = 0; i<n; i++)
		val_list = c->getComponents(name_list, para_list);
	printf("\nGet : %.3f", 10 * double( clock () - begin_time ) /  (CLOCKS_PER_SEC));
	
	// test for getting values
	begin_time = clock();
	for (int i = 0; i<n; i++)
		c->getComponents(name_list, para_list, val_list);
	printf("\nGet 2 : %.3f", 10 * double( clock () - begin_time ) /  (CLOCKS_PER_SEC));
	
	return 0;
}
