//--------------------------------------------------------------------------
// Multiple tests to validate the integrity of APREM
//--------------------------------------------------------------------------

#include <stdio.h>
#include <vector>
#include <string>

#include "CircuitFactory.h"

using namespace std;

int main()
{
	// Simple example of a parametric analysis
	// Initialization
	CircuitFactory circuitFactory;
	
	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	// Test 1 - Perfomance + automatic name creation ---------------------------
	CircuitInterface* c = circuitFactory.CreateCircuit();

	node[0] = "A";
	node[1] = "0";
	values[0] = 240.0;
	values[1] = 0.0;

	for (int i=0; i<1000; i++)
	{
		c->addComponent("XX", node, values, "Vsrc");
	}

	vector<string> a = c->getNames("Comp");
	printf("\n\n");
	for (int i=0; i<a.size(); i++)
	{
		printf("  %s", a[i].c_str());
	}

	// Test 2 - Name is returned -----------------------------------------------
	values[0] = 0.5;
	values[1] = 0.5;
	string impname = c->addComponent("XX", node, values, "Imp");
	printf("  %s", impname.c_str());

	// Test 3 - Bad calls ------------------------------------------------------
	c->set("BadName", "Z", 0.5);  // no crash
	c->set(impname, "BadAtt", 0.5); // no crash*/

	vector<pair<double, double> > b = c->get("BadName", "V");
	printf("\n %d", (int) b.size());
	
	b = c->get(impname, "BadAtt");     // For the moment, returns 0
	printf("\n %d", (int) b.size());

	// Test 4 - Memory handling tests - make sure everything is ok -------------
	// This is a more advanced example, but it is useful
	CircuitInterface* c1 = circuitFactory.CreateCircuit();
	c1->addComponent("Imp1", node, values, "Imp");

	CircuitInterface* c2 = c1;        // Creates a reference to c1
	CircuitInterface* c3 = circuitFactory.CopyCircuit(c1);  // Creates a deep copy

	node[0] = "B";
	values[0] = 0.0;
	c1->addComponent("Imp2", node, values, "Imp");
			
	printf("\n-----\n");
	a = c2->getNames("Comp"); // Notice Imp2 is printed, since c2 was a reference to
							   // the same circuit as c1
	printf("\n\n");
	for (int i=0; i<a.size(); i++)
	{
		printf("  %s", a[i].c_str());
	}
	
	printf("\n-----\n");
	a = c3->getNames("Comp"); // now, only the c3 circuit exists, notice that Imp2 is
						// not printed, since c3 is a circuit by itself because
						// of the deep copy.
	printf("\n\n");
	for (int i=0; i<a.size(); i++)
	{
		printf("  %s", a[i].c_str());
	}
	
	return 0;
}

