
// Simple example of a parametric analysis
	
#include <stdio.h>
#include <vector>

#include "CircuitFactory.h"

using namespace std;

#define n 6

int main()
{
	// A factory is all we need (or several factories)
	CircuitFactory circuitFactory;
	
	// The factory creates a circuit for you
	// It will delete it when it gets out of scope, no need to worry about it
	CircuitInterface * c = circuitFactory.CreateCircuit(); 
	
	// Creation of circuit (only 4 components here)
	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	node[0] = "A";
	node[1] = "0";
	values[0] = 240.0;
	values[1] = 0.0;
	c->addComponent("E_A", node, values, "Vsrc");   // Voltage source
	
	node.resize(4);
	node[0] = "A";
	node[1] = "0";
	node[2] = "a";
	node[3] = "0";
	values[0] = 2.0;
	values[1] = 0.0;
	c->addComponent("txfo_A", node, values, "Txfo"); // Transformer
	
	node.resize(2);
	node[0] = "a";
	node[1] = "c";
	values.resize(1);
	values[0] = 120.0;
	c->addComponent("R_a", node, values, "Imp");     // Impedance
	
	node[0] = "c";
	node[1] = "0";
	values[0] = 120.0;
	c->addComponent("R_b", node, values, "Imp");
	
	// We prepare the case 
	double R[n] = {120, 60, 40, 30, 25, 20};    // Values of resistance to be used as the load
	double S[n] = {0};

	// Let's run the loop

	for (int i=0; i<n; i++)
	{
	   c->set("R_a", "Z", R[i], 0.0 /*imag*/);   // Update the value of the resistance
	   c->get("R_a", "Z");
	   c->solve();               // Execute the load flow (here no PQ load is present, so it is a linear system that is solved)
	   S[i]=c->get("R_a","S")[0].first; // Get the power (S=P+jQ) in the resistor (comes out as a complex number)
	}

	// Print example
	c->print("Nodes");
	c->print("Comp");

	printf("\nResistances");
	for (int i=0; i<n; i++)
	{
		printf(" %f",R[i]);

	}
	printf("\nPower");
	for (int i=0; i<n; i++)
	{
		printf(" %f", S[i]);

	}
	
	return 0;
}