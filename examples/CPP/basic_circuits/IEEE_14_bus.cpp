// IEEE 14 bus for APREM tests
// All values are in PU

#include <stdio.h>
#include <vector>

#include "CircuitFactory.h"

using namespace std;

int main()
{
	CircuitFactory circuitFactory;
	CircuitInterface* IEEE_14bus = circuitFactory.CreateCircuit(); 

	// synchronous machines
	//Bus1 = generator bus
	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	node[0] = "Bus1";
	node[1] = "0";
	values[0] = 2.32;
	values[1] = 0.97;
	IEEE_14bus->addComponent("G1", node, values, "PV");
	
	//Bus2 = Slack bus
	node[0] = "Bus2";
	values[0] = 1.0;
	values[1] = 0.0;
	IEEE_14bus->addComponent("G2", node, values, "Vsrc");

	// 11 charges of total power 259 MW and 81.3 Mvar
	// All nominal voltage to 1 pu
	node[0] = "Bus2";
	node[1] = "0";
	values[0] = 0.2170;
	values[1] = 0.1270;
	values.push_back(1.0);
	IEEE_14bus->addComponent("Ch1", node, values, "PQ");
	
	node[0] = "Bus3";
	node[1] = "0";
	values[0] = 0.9420;
	values[1] = 0.1900;
	IEEE_14bus->addComponent("Ch2", node, values, "PQ");
	
	node[0] = "Bus4";
	node[1] = "0";
	values[0] = 0.4780;
	values[1] = 0.0;
	IEEE_14bus->addComponent("Ch3", node, values, "PQ");
	
	node[0] = "Bus5";
	node[1] = "0";
	values[0] = 0.0760;
	values[1] = 0.0160;
	IEEE_14bus->addComponent("Ch4", node, values, "PQ");
	
	node[0] = "Bus6";
	node[1] = "0";
	values[0] = 0.1120;
	values[1] = 0.0750;
	IEEE_14bus->addComponent("Ch5", node, values, "PQ");
	
	node[0] = "Bus9";
	node[1] = "0";
	values[0] = 0.2950;
	values[1] = 0.1660;
	IEEE_14bus->addComponent("Ch6", node, values, "PQ");
	
	node[0] = "Bus10";
	node[1] = "0";
	values[0] = 0.0900;
	values[1] = 0.0580;
	IEEE_14bus->addComponent("Ch7", node, values, "PQ");
	
	node[0] = "Bus11";
	node[1] = "0";
	values[0] = 0.0350;
	values[1] = 0.0180;
	IEEE_14bus->addComponent("Ch8", node, values, "PQ");
	
	node[0] = "Bus12";
	node[1] = "0";
	values[0] = 0.0610;
	values[1] = 0.0160;
	IEEE_14bus->addComponent("Ch9", node, values, "PQ");
	
	node[0] = "Bus13";
	node[1] = "0";
	values[0] = 0.1350;
	values[1] = 0.0580;
	IEEE_14bus->addComponent("Ch10", node, values, "PQ");
	
	node[0] = "Bus14";
	node[1] = "0";
	values[0] = 0.1490;
	values[1] = 0.0500;
	IEEE_14bus->addComponent("Ch11", node, values, "PQ");

	// 2 transformers of real ratio and serial reactance
	values.resize(2);
	node.resize(4);
	node[0] = "Bus5";
	node[1] = "0";
	node[2] = "I1";
	node[3] = "0";
	values[0] = 0.932;
	values[1] = 0.0;
	IEEE_14bus->addComponent("T1", node, values, "Txfo");
	
	node[0] = "Bus4";
	node[1] = "0";
	node[2] = "I2";
	node[3] = "0";
	values[0] = 0.969;
	values[1] = 0.0;
	IEEE_14bus->addComponent("T2", node, values, "Txfo");

	node.resize(2);
	node[0] = "I1";
	node[1] = "Bus6";
	values[0] = 0.0;
	values[1] = 0.25202;
	IEEE_14bus->addComponent("Imp_T1", node, values, "Imp");
	
	node[0] = "I2";
	node[1] = "Bus9";
	values[0] = 0.0;
	values[1] = 0.55618;
	IEEE_14bus->addComponent("Imp_T2", node, values, "Imp");

	// 15 lines with pi model

	// Serial
	node[0] = "Bus1";
	node[1] = "S1";
	values[0] = 0.01938;
	values[1] = 0.05917;
	IEEE_14bus->addComponent("L1", node, values, "Imp");
	
	node[0] = "Bus1";
	node[1] = "Bus5";
	values[0] = 0.05403;
	values[1] = 0.22304;
	IEEE_14bus->addComponent("L2", node, values, "Imp");
	
	node[0] = "Bus2";
	node[1] = "Bus3";
	values[0] = 0.04699;
	values[1] = 0.19797;
	IEEE_14bus->addComponent("L3", node, values, "Imp");
	
	node[0] = "Bus2";
	node[1] = "Bus4";
	values[0] = 0.05811;
	values[1] = 0.17632;
	IEEE_14bus->addComponent("L4", node, values, "Imp");
	
	node[0] = "Bus2";
	node[1] = "Bus5";
	values[0] = 0.05695;
	values[1] = 0.17388;
	IEEE_14bus->addComponent( "L5", node, values, "Imp");
	
	node[0] = "Bus3";
	node[1] = "Bus4";
	values[0] = 0.06701;
	values[1] = 0.17103;
	IEEE_14bus->addComponent( "L6", node, values, "Imp");
	
	node[0] = "Bus4";
	node[1] = "Bus5";
	values[0] = 0.01335;
	values[1] = 0.04211;
	IEEE_14bus->addComponent( "L7", node, values, "Imp");
	
	node[0] = "Bus6";
	node[1] = "Bus11";
	values[0] = 0.09498;
	values[1] = 0.1989;
	IEEE_14bus->addComponent( "L8", node, values, "Imp");
	
	node[0] = "Bus6";
	node[1] = "Bus12";
	values[0] = 0.12291;
	values[1] = 0.25581;
	IEEE_14bus->addComponent( "L9", node, values, "Imp");
	
	node[0] = "Bus6";
	node[1] = "Bus13";
	values[0] = 0.06615;
	values[1] = 0.13027;
	IEEE_14bus->addComponent( "L10", node, values, "Imp");
	
	node[0] = "Bus9";
	node[1] = "Bus10";
	values[0] = 0.03181;
	values[1] = 0.08450;
	IEEE_14bus->addComponent( "L11",  node, values, "Imp");
	
	node[0] = "Bus10";
	node[1] = "Bus11";
	values[0] = 0.08205;
	values[1] = 0.19207;
	IEEE_14bus->addComponent( "L12", node, values, "Imp");
	
	node[0] = "Bus12";
	node[1] = "Bus13";
	values[0] = 0.22092;
	values[1] = 0.19988;
	IEEE_14bus->addComponent( "L13", node, values, "Imp");
	
	node[0] = "Bus13";
	node[1] = "Bus14";
	values[0] = 0.17093;
	values[1] = 0.34802;
	IEEE_14bus->addComponent( "L14", node, values, "Imp");
	
	node[0] = "Bus9";
	node[1] = "Bus14";
	values[0] = 0.12711;
	values[1] = 0.27038;
	IEEE_14bus->addComponent( "L15", node, values, "Imp");

	// Shunt
	node[0] = "Bus1";
	node[1] = "0";
	values[0] = 0.0;
	values[1] = 2/0.0528;
	IEEE_14bus->addComponent( "B1", node, values, "Imp");
	
	node[0] = "Bus2";
	values[1] = 2/0.0528;
	IEEE_14bus->addComponent( "B2", node, values, "Imp");
	
	node[0] = "Bus1";
	values[1] = 2/0.0492;
	IEEE_14bus->addComponent( "B3", node, values, "Imp");
	
	node[0] = "Bus5";
	values[1] = 2/0.0492;
	IEEE_14bus->addComponent( "B4", node, values, "Imp");
	
	node[0] = "Bus2";
	values[1] = 2/0.0438;
	IEEE_14bus->addComponent( "B5", node, values, "Imp");
	
	node[0] = "Bus3";
	values[1] = 2/0.0438;
	IEEE_14bus->addComponent( "B6", node, values, "Imp");
	
	node[0] = "Bus2";
	values[1] = 2/0.0374;
	IEEE_14bus->addComponent( "B7", node, values, "Imp");
	
	node[0] = "Bus4";
	values[1] = 2/0.0374;
	IEEE_14bus->addComponent( "B8", node, values, "Imp");
	
	node[0] = "Bus2";
	values[1] = 2/0.034;
	IEEE_14bus->addComponent( "B9", node, values, "Imp");
	
	node[0] = "Bus5";
	values[1] = 2/0.034;
	IEEE_14bus->addComponent( "B10", node, values, "Imp");
	
	node[0] = "Bus3";
	values[1] = 2/0.0346;
	IEEE_14bus->addComponent( "B11", node, values, "Imp");
	
	node[0] = "Bus4";
	values[1] = 2/0.0346;
	IEEE_14bus->addComponent( "B12", node, values, "Imp");
	
	node[0] = "Bus4";
	values[1] = 2/0.0128;
	IEEE_14bus->addComponent( "B13", node, values, "Imp");
	
	node[0] = "Bus5";
	values[1] = 2/0.0128;
	IEEE_14bus->addComponent( "B14", node, values, "Imp");

	// 1 switch closed for the test
	node[0] = "S1";
	node[1] = "Bus2";
	values[0] = 1;
	values[1] = 0;
	IEEE_14bus->addComponent("Sw1", node, values, "Sw");



	//--------------------------------------------------------------------------
	// IMPORTANT : Test on deleting components
	//--------------------------------------------------------------------------
	// To see the effect of the delete, uncomment the next line
	// Node S1 is deleted automatically since not used anymore
	// IEEE_14bus.del({"L1","Sw1"});

	//IEEE_14bus.print("Comp")
	IEEE_14bus->solve();
	IEEE_14bus->print("Nodes");

	// Always apply the same value, just for test (and time test)
	double Pr = 0.0760;
	double Vr = 0.97;
	double angle = 25.0;
	double Qr = 0.0160;

	IEEE_14bus->set("Ch4","P", Pr, 0.0);   // Update the value of the resistance
	IEEE_14bus->set("G1","V", Vr, 0.0);	
	IEEE_14bus->set("G2","angle", angle);	
	IEEE_14bus->set("Ch9", "Q", Qr);
	
	IEEE_14bus->solve();
					
	// get()
	vector<pair<double, double> > out;
	out = IEEE_14bus->get("Ch8","S");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("Ch8","Vb");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("Ch8","I");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("G1","S");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("G1","Vb");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("G1","I");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("G2","S");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("G2","Vb");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("G2","I");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("L4","S");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("L4","Vb");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("L4","I");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("B2","S");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("B2","Vb");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("B2","I");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("T1","S");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("T1","Vb");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("T1","I");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("Sw1","S");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("Sw1","Vb");
	printf("\n %f %f", out[0].first, out[0].second);
	out = IEEE_14bus->get("Sw1","I");
	printf("\n %f %f", out[0].first, out[0].second);

	// getNode()
	pair<double,double> outNode;
	outNode = IEEE_14bus->getNodeV("Bus1");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("Bus2");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("Bus3");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("Bus4");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("Bus5");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("Bus6");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("Bus9");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("Bus10");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("Bus11");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("Bus12");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("Bus13");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("Bus14");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("I1");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("I2");
	printf("\n %f %f", outNode.first, outNode.second);
	outNode = IEEE_14bus->getNodeV("S1");
	printf("\n %f %f", outNode.first, outNode.second);

	return 0;

}
