
// Basic test on setting a node off

#include <stdio.h>
#include <vector>

#include "CircuitFactory.h"

using namespace std;

int main()
{
	CircuitFactory circuitFactory;
	
	CircuitInterface* c = circuitFactory.CreateCircuit();
	
	// Basic test on setting a component to on and off
	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	node[0] = "A";
	node[1] = "0";
	values[0] = 1.0;
	values[1] = 12.0;
	c->addComponent("I1", node, values, "Isrc");
	
	node[0] = "A";
	node[1] = "B";
	values[0] = 0.5;
	values[1] = 0.5;
	c->addComponent("Imp1", node, values, "Imp");
	
	node[0] = "B";
	node[1] = "0";
	c->addComponent("Imp2", node, values, "Imp");
	
	node[0] = "A";
	node[1] = "C";
	c->addComponent("Imp3", node, values, "Imp");
	
	node[0] = "C";
	node[1] = "0";
	c->addComponent("Imp4", node, values, "Imp");

	// Example on how to get the names of components in the circuit
	// c->getNames("Node");
	// c->getNames("Comp");

	// Step 1 : Solve for current circuit
	c->solve();
	
	vector<pair<double,double> > out = c->get("Imp1", "I");
	for (int i=0; i< out.size(); i++)
		printf("\n %f %f", out[i].first, out[i].second);

	vector<string> names = c->getNames("Node"); // Notice the 3 nodes
	for (int i=0; i< names.size(); i++)
		printf("\n %s", names[i].c_str());
	
	// Step 2 : Deativate node
	c->setNode("C", "on", 0);
	c->solve();
	
	// Notice the current value changed.
	out = c->get("Imp1", "I");
	for (int i=0; i< out.size(); i++)
		printf("\n %f %f", out[i].first, out[i].second);

	// Notice the 2 "fragment" nodes created. When a node is
	// is set off, "fragment" nodes are created and the
	// components linked to the original nodes are linked
	// to those fragments. The components are now unlinked.
	names = c->getNames("Node");
	for (int i=0; i< names.size(); i++)
		printf("\n %s", names[i].c_str());

	// Step 3 : Reactivate node
	// When setting node 'on' again. The components are Linked back to the
	// original node, thus connecting themselves together.
	c->setNode("C", "on", 1);
	c->solve();
	
	out = c->get("Imp1", "I");
	for (int i=0; i< out.size(); i++)
		printf("\n %f %f", out[i].first, out[i].second);

	// Notice the current value is back to original value
	names = c->getNames("Node");
	for (int i=0; i< names.size(); i++)
		printf("\n %s", names[i].c_str());
	
	return 0;
	
}
	
	
	
	