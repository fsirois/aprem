
#include <stdio.h>
#include <vector>

#include "CircuitFactory.h"

using namespace std;

int main()
{
	CircuitFactory circuitFactory;
	
	CircuitInterface* c = circuitFactory.CreateCircuit();
	
	// Basic test on setting a component to on and off
	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	node[0] = "A";
	node[1] = "0";
	values[0] = 1.0;
	values[1] = 12.0;
	c->addComponent("Isrc1", node, values, "Isrc");
	
	values[0] = 0.5;
	values[1] = 0.5;
	c->addComponent("Imp1", node, values, "Imp");
	c->addComponent("Imp2", node, values, "Imp");

	// Example on how to get the names of components in the circuit
	// c->getNames("Node");
	// c->getNames("Comp");

	// Solve the circuit and get Voltage of Imp1
	c->solve();
	
	vector<pair<double,double> > out = c->getV("Imp1");
	for (int i=0; i< out.size(); i++)
		printf("\n %f %f", out[i].first, out[i].second);

	// Set Imp2 off, solve and get again Imp1, notice V changed
	c->set("Imp2", "on", 0); // second input not used in this case - to fix
	c->solve();
	out = c->getV("Imp1");
	for (int i=0; i< out.size(); i++)
		printf("\n %f %f", out[i].first, out[i].second);

	// Set Imp2 on again, V is back to it's formed value
	c->set("Imp2", "on", 1);
	c->solve();
	out = c->getV("Imp1");
	for (int i=0; i< out.size(); i++)
		printf("\n %f %f", out[i].first, out[i].second);
	
	return 0;
}
