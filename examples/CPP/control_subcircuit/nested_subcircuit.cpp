
// An example of nested subcircuits

#include <stdio.h>

#include "CircuitFactory.h"
#include <vector>
#include <string>

using namespace std;

int main()
{
	CircuitFactory CircuitFactory;
	
	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	// Level 1 circuits
	CircuitInterface * serie = CircuitFactory.CreateCircuit();
	
	node[0] = "N1";
	node[1] = "N2";
	values[0] = 0.5;
	values[1] = 0.3;
	serie->addComponent("Z1", node, values, "Imp");

	node[0] = "N2";
	node[1] = "N3";
	values[0] =  0.3;
	values[1] = 0.5;
	serie->addComponent("ZVsrc", node, values, "Imp");
	
	node[0] = "N1";
	serie->setIONodes(node);

	// Level 2 circuits
	CircuitInterface * parallel = CircuitFactory.CreateCircuit();
	
	node[0] = "Node1";
	node[1] = "Node2";
	parallel->addSubcircuit(serie, "Serie1", node);
	
	node[0] = "Node2";
	node[1] = "Node1";
	parallel->addSubcircuit(serie, "Serie2", node);

	parallel->setIONodes(node);

	// Level 3 circuit
	CircuitInterface * c = CircuitFactory.CreateCircuit();
	
	node[0] = "SupNode3";
	node[1] = "SupNode1";
	c->addSubcircuit(parallel, "Par1", node);
	
	node[0] = "SupNode1";
	node[1] = "SupNode2";
	c->addSubcircuit(parallel, "Par2", node);
	
	node[0] = "SupNode3";
	values[0] =  0.3;
	values[1] = 0.5;
	c->addComponent("Z3", node, values, "Imp");
	
	c->setIONodes(node);
	
	// Level 4 circuit
	CircuitInterface * master = CircuitFactory.CreateCircuit();
	
	node[0] = "0";
	node[1] = "NodeBeta";
	master->addSubcircuit(c, "Sub1", node);
	
	node[0] = "NodeBeta";
	node[1] = "NodeDelta";
	master->addSubcircuit(c, "Sub2", node);
	
	node[0] = "NodeBeta";
	node[1] = "NodeDelta";
	master->addSubcircuit(c, "Sub3", node);
	
	node[0] = "0";
	values[0] = 120.0;
	values[1] = 180.0;
	master->addComponent("Vsrc", node, values, "Vsrc");
	
	// Print the result
	// Notice the auto-generated names for components and subcircuits
	// Also notice that each subcircuits only knows one level below it.
	master->print("Comp");
	master->print("Subcircuit");

	// Solve normally and get data
	master->solve();
	
	pair<double,double> outPair = master->getNodeV("NodeBeta");
	printf("\n%f %f\n",outPair.first,outPair.second);
	
	vector<double> outNode = master->getNode("NodeDelta", "no");
	printf("\n %d", (int) outNode[0]);

	vector<pair<double,double> > out = master->get("Vsrc", "I");
	printf("\n%f %f",out[0].first,out[0].second);
	
	
	// some display on V
	printf("\n\nShow some way to get Voltage");
	out = master->getV("Vsrc");
	printf("\nVoltage at the 2 nodes");
	for (int i = 0; i<out.size(); i++)
	{
		printf("\n %f %f",out[i].first,out[i].second);
	}
	
	printf("\n\nVoltage going through one way or another.");
	out = master->get("Vsrc", "Va");
	printf("\n %f %f",out[0].first,out[0].second);
	
	out = master->get("Vsrc", "Vb");
	printf("\n %f %f",out[0].first,out[0].second);
	
	return 0;
}


