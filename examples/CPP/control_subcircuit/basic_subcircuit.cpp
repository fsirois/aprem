
#include <stdio.h>

#include "CircuitFactory.h"
#include <vector>
#include <string>

static CircuitFactory g_CircuitFactory;

#include "myTxfo.cpp" 

using namespace std;

int main()
{

	// Basic example on how to use subcircuits
	CircuitInterface * circuit = g_CircuitFactory.CreateCircuit();

	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	node[0] = "0";
	node[1] = "V1";
	values[0] = 14400.0;
	values[1] = 0.0;
	circuit->addComponent("Vsrc", node, values, "Vsrc");

	node[0] = "V1";
	node[1] = "V2";
	values[0] = 14400.0;
	values[1] = 0.0;
	circuit->addComponent("ZVsrc", node, values, "Imp");

	// myTxfo creates a circuit on its own, with interface nodes defined. See
	// file myTxfo.cpp to see how the interface nodes are marked.
	CircuitInterface * monTxfo = myTxfo(120, 0.3, 0.1, 900, 1000, 0.2, 0.2);

	// add the circuit to circuit
	node.resize(4);
	node[0] = "0";
	node[1] = "V2";
	node[2] = "0";
	node[3] = "VBus";
	circuit->addSubcircuit(monTxfo, "Txfo_1", node);

	// Add another component to the circuit
	node.resize(2);
	node[0] = "VBus";
	node[1] = "0";
	values.resize(3);
	values[0] = 1000.0;
	values[1] = 100.0;
	values[2] = 14400.0/120.0;
	circuit->addComponent("PQ", node, values, "PQ");

	// Get information on the subcircuits
	circuit->print("Subcircuit");
	circuit->print("Node");
	circuit->print("Comp");
	vector<string> names = circuit->getNames("Subcircuit");
	for (int i=0; i<names.size(); i++)
	{
		printf(" %s", names[i].c_str());
	}
	
	// Solve normally
	circuit->solve();

	vector<pair<double,double> > out = circuit->get("ZVsrc","I");
	printf("\n%f %f",out[0].first,out[0].second);
	
	out = circuit->get("PQ","S");
	printf("\n%f %f",out[0].first,out[0].second);
	
	return 0;
}


