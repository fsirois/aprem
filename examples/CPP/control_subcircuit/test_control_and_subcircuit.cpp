
// Test with controls on subcircuits

#include <stdio.h>

#include "CircuitFactory.h"
#include <vector>
#include <string>

#include "controlFunctions.cpp" // to change to a .h

using namespace std;

int main()
{
	CircuitFactory CircuitFactory;
	
	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	CircuitInterface * controlled_circuit = CircuitFactory.CreateCircuit();

	// Create a basic controlled circuit
	node[0] = "A";
	node[1] = "B";
	values[0] = 20.0;
	values[1] = 3.0;
	controlled_circuit->addComponent("R_a", node, values, "Imp");

	node[0] = "C";
	node[1] = "B";
	controlled_circuit->addComponent("R_b", node, values, "Imp");
	
	node[0] = "A";
	node[1] = "C";
	controlled_circuit->addComponent("R_c", node, values, "Imp");
	
	node[0] = "A";
	node[1] = "B";
	controlled_circuit->setIONodes(node);

	// Add a control
	vector<string> inputComponents;
	inputComponents.push_back("R_c");
	
	vector<string> intputAttributes;
	intputAttributes.push_back("I");
	
	vector<string> outputComponents;
	outputComponents.push_back("R_b");
	
	vector<string> outputAttributes;
	outputAttributes.push_back("Z");
	
	vector<double> functionData;
	functionData.push_back(1.0);
	
	controlled_circuit->addControl(inputComponents,
						intputAttributes,
						outputComponents,
						outputAttributes,
						functionData,
						"myControl",
						"control1",
						&myControl);

	controlled_circuit->print("Control");
	
		
	// Add them as subcircuits to an higher level circuit
	CircuitInterface * two_controlled = CircuitFactory.CreateCircuit();
	
	node[0] = "n1";
	node[1] = "n2";
	two_controlled->addSubcircuit(controlled_circuit, "sub1", node);
	two_controlled->addSubcircuit(controlled_circuit, "sub2", node);

	two_controlled->setIONodes(node);

	two_controlled->print("Comp");
	two_controlled->print("Nodes");
	two_controlled->print("Control");
	two_controlled->print("Subcircuit");
	
	// Put the higher level circuit in a yet higher level circuit
	CircuitInterface * main_circuit = CircuitFactory.CreateCircuit();
	
	node[0] = "p1";
	node[1] = "0";
	main_circuit->addSubcircuit(two_controlled, "main1", node);
	
	node[0] = "p2";
	node[1] = "0";
	main_circuit->addSubcircuit(two_controlled, "main2", node);

	node[0] = "p1";
	node[1] = "p2";
	values[0] = 0.0;
	values[1] = 20.0;
	main_circuit->addComponent("R1", node, values, "Imp");
	
	node[0] = "p1";
	node[1] = "0";
	values[0] = 220.0;
	values[1] = 0.0;
	main_circuit->addComponent("V1", node, values, "Vsrc");

	main_circuit->print("Comp");
	main_circuit->print("Nodes");
	main_circuit->print("Control");
	main_circuit->print("Subcircuit");

	// Solve normally and get data
	main_circuit->solve();
	
	vector<pair<double,double> > out = main_circuit->get("main1_sub1_R_a", "I");
	printf("\n%f %f",out[0].first,out[0].second);
	
	out = main_circuit->get("main1_sub1_R_c", "I");
	printf("\n%f %f",out[0].first,out[0].second);
	
	out = main_circuit->get("main1_sub1_R_a", "I");
	printf("\n%f %f",out[0].first,out[0].second);
	
	out = main_circuit->get("main1_sub1_R_b", "I");
	printf("\n%f %f",out[0].first,out[0].second);
	
	return 0;
}





