
using namespace std;

// TXFOREEL Creates an object Circuit corresponding to a model of
// real transformer with interface nodes.
//
// Parameters : ratio : Transformer ratio
//              R1    : Resistance of primary wire
//              X1    : Leak inductance of the primary
//              Rfe   : Iron leaks
//              Xphi  : Magnetization inductance
//              R2    : Resistance of secondary wire
//              X2    : Leak inductance of the secondary
//
// Return value : Circuit object corresponding to a real transformer

CircuitInterface * myTxfo(double ratio , double R1 , double X1 , double Rfe , double Xphi , double R2 , double X2)
{
	// Create circuit
	CircuitInterface * txfo = g_CircuitFactory.CreateCircuit();

	vector<string> node;
	node.resize(2);
	vector<double> values;
	values.resize(2);
	
	node[0] = "P_pos";
	node[1] = "A";
	values[0] = R1;
	values[1] = X1;
	
	// Add components
	// Impedance of the primary
	txfo->addComponent("Z1_s", node, values, "Imp");
 
 	node[0] = "A";
	node[1] = "P_neg";
	values[0] = Rfe * Xphi * Xphi / (Rfe * Rfe + Xphi * Xphi);
	values[1] = Rfe * Xphi * Rfe / (Rfe * Rfe + Xphi * Xphi);
	
	// Iron and magnetization leaks
	txfo->addComponent("Z1_p", node, values, "Imp");
 
	node.resize(4);
	node[2] = "B";
	node[3] = "S_neg";
 	values[0] = ratio;
	values[1] = 0.0;
	
	// Ideal transformer
	txfo->addComponent("Txfo_ideal", node, values, "Txfo");
 
 	node.resize(2);
	node[0] = "S_pos";
	node[1] = "B";
 	values[0] = R2;
	values[1] = X2;
	
	// Impedance of the secondary
	txfo->addComponent("Z2_s", node, values, "Imp");
	
	
	// Interface nodes are designated
	node.resize(4);
	node[0] = "P_pos";
	node[1] = "P_neg";
	node[2] = "S_pos";
	node[3] = "S_neg";
	txfo->setIONodes(node);

	return txfo;
}








