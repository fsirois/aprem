% Circuit Class
%
% This class allows to simulate electrical networks using a load flow
% algorithm. It is able to hold all the components of the circuit,
% including controls and subcircuits, and also the nodes linking those
% elements.
%
% This class is an interface for the C++ class contained in APREM. The
% methods included in this interface are all the functions a user can
% use and are complete. Complex circuits may be simulated using this
% simple interface.
%
% The APREM program allows to add:
%   - Impedances
%   - Current Sources
%   - Voltage Sources
%   - Power Sources
%   - Power Charges
%   - Switches
%   - Controls (user-defined controls, such as breakers)
%   - Subcircuits (by inserting circuits inside other circuits)
%
% Functionalities include:
%   - Setting values for components
%   - Getting values from components
%   - Deleting items (components, controls, subcircuits)
%   - Setting nodes or components On or Off
%
% This class was creating with the idea to ease parametric analysis of
% circuits. Thus, with th set/get functionalities and the possibility to
% set a node or a component Off and set it On again allows easy
% parametrization of circuit analysis.
%
% Example:
%   c = Circuit();
%   c.add('Imp', 'r1', {'n1','0'}, 20+20i);
%   c.add('Vsrc', 'V1', {'n1', '0'}, {120, 0});
%   c.solve();
%   I1 = c.get('r1', 'I');
%   c.set('r1', 'Z', 30 + 10i);
%   c.solve();
%   I2 = c.get('r1', 'I');

classdef Circuit < handle
    
    % MATLAB properties
    properties (GetAccess = public, SetAccess = private)
        is3Phase = 0;  % Indicates if the circuit has 1 or 3 phases.
        maxIterControl = 100; % Number of iterations max when solving.
    end
    
    % Handle on C++ object
    properties (SetAccess = public, GetAccess = public, Hidden = false)
        objectHandle; % Handle on the C++ class. Never use directly.
    end
    
    % All methods
    methods (Access = public)
        
        % Constructor -----------------------------------------------------
        function obj = Circuit(varargin)
            % Creates a circuit object and returns it. When calling the
            % constructor with no parameter, it will create an empty
            % circuit. When giving a circuit as an input, it will create
            % a deep copy of the circuit.
            %
            % Example : 
            %   circuit_1 = Circuit();
            %   ...
            %   circuit_2 = Circuit(circuit_1);
            %
            % Do not use : circuit_1 = circuit_2
            
            % Default constructor
            if nargin == 0
                obj.objectHandle = obj.c_circuit('new');

            % Copy constructor
            elseif nargin == 1
                if ~isa(varargin{1}, 'Circuit')
                    error('Argument must either be nothing or a Circuit object.');
                end
                
                obj.objectHandle = obj.c_circuit('new', varargin{1});
                obj.is3Phase = varargin{1}.is3Phase;
                
            else
                error('Too many arguments.');
            end
            
            % Adds the path for the INI file
            if nargin <= 1         
                %iniFilePath = fileparts(mfilename('fullpath'))
                %obj.c_circuit('set', 'iniVariables', iniFilePath);
            end
        end
        
        % Destructor ------------------------------------------------------
        function delete(obj)
            if (obj.objectHandle ~= 0)
                obj.c_circuit('delete');
            end
        end
        

        %% Add/Delete items in the circuit
        
        % add -------------------------------------------------------------
        function Name = add(obj, type, name, nodes, param)
            % ADD  Adds an item to the circuit.
            %
            % Name = Circuit.add(type, name, nodes, param);
            %
            % INPUTS  
            %   type -> Type of item to add to the circuit. (string)
            %           Options are: Imp Isrc Vsrc Sw Txfo PQ PV
            %                        Control and Subcircuit
            %   nodes -> List of the nodes of the element. In the case
            %            of a control, it is the name of the MATLAB
            %            function acting as a control. (cell of strings)
            %   param -> List of parameters to pass to the circuit.
            %            For detail of the parameters, see full DOC. (cell)
            %
            % OUTPUT
            %   Name of the item added to the circuit. (string)
            %
            % EXAMPLES 
            %   c.add('Vsrc', 'Vsrc', {'nodeAlpha', '0'}, {120, 0});
            %
            %   c1 = Circuit();
            %   ...
            %   c2.add('Subcircuit', 'sub_a', {'n1','n2','n3','n4'}, c2);
            %
            % See also DEL
			if ~iscell(param)
				param = {param};
			end
            Name = obj.c_circuit('add', type, name, nodes, param); 
        end
        
        % load ------------------------------------------------------------
        function draw_info = load(obj, file_name)
            file_path = which(file_name);
            [comp_info, draw_info.param_string, draw_info.draw_strings] = obj.c_circuit('load',file_path);
            
            for index =1:length(comp_info)
               draw_info.component(index).name = comp_info{1,index};
               draw_info.component(index).draw_info = comp_info{2,index};
               draw_info.component(index).nodes = comp_info{3,index};
            end
        end
        
        
        % del -------------------------------------------------------------
        function del(obj, list_ID)
        	% DEL  Removes an item from the circuit.
            %
            % Circuit.del(list_ID);
            %
            % INPUTS
            %   list_ID -> Names of items to delete. (cell of strings).
            %
            % EXAMPLE
            %   c.del({'r1', 'sub1', 'Vsrc});
            %
            % See also ADD
            if ~isempty(list_ID)
                if ischar(list_ID)
                    list_ID = {list_ID};
                end
                obj.c_circuit('del',list_ID);
            end
        end
        
        %% Get functions
        
        % get -------------------------------------------------------------
        function attVal = get(obj, varargin)
            % GET  Gets the value of an attribute of a circuit element.
            %
            % Name = Circuit.get(comp1, att1, comp2, att2, ...);
            %
            % INPUTS  
            %   comp -> Name of the item we are interested in.
            %   att -> Name of the attribute we want to know. It may be
            %          for example, 'V', 'I' or 'S'. To see each attribute
            %          available for each type of component, see full
            %          documentation.
            %
            % Any number of pair <Comp, Att> can be provided, as long
            % as the number of inputs is even.
            %
            % OUTPUT
            %   Value or list of values of the attributes for given items.
            %
            % EXAMPLES 
            %   out = c.get('V1', 'V');
            %   out = c.get('R1', 'I', 'PV1', 'S');
            %   out = c.get({'Comp1', 'nodes', 'Comp1', 'on'});
            %
            % See also GETNODE GETNAMES
            
            % If the list of names and attributes is given as a cell get
            % separate items.
            if (length(varargin) == 1 ) && iscell(varargin{1})
                varargin = varargin{1};
            end
            
            % Number of names and number of attributes must be equal.
            nbComp = (length(varargin)) / 2;
            if nbComp ~= round(nbComp)
                error('The number of component and the number of attributes mismatch.');
            end
            
            % Get attribute value for each component
            attVal{nbComp} = 0;
            for k = 1:nbComp
                
                % Get component name and attribute name
                compName = varargin{2*k-1};
                attName = varargin{2*k};
                
                % If the circuit is a 3 phased one, correct currents
                attVal{k} = obj.c_circuit('get', compName, attName);
                if (obj.is3Phase == 1 && ...
                    (strcmp(attName,'I') || strcmp(attName,'Is') || ...
                     strcmp(attName,'Iabs') || strcmp(attName, 'Inom')))
                    attVal{k} = attVal{k} / sqrt(3);
                end
            end
            
            % If length of output is one, do not return as a cell
            if length(attVal) == 1
                attVal = attVal{1};
            end
        end

        
        % getNode ---------------------------------------------------------
        function attVal = getNode(obj, nodeName, attName)
            % GETNODE  Gets the value of an attribute of a node.
            %
            % Name = Circuit.getNode(nodeName, attribute);
            %
            % INPUTS  
            %   nodeName -> Name of the node we are interested in.
            %   att -> Name of the attribute we want to know. It may be
            %          'on' or 'V'.
            %
            % OUTPUT
            %   Value of the node attribute.
            %
            % EXAMPLES 
            %   out = c.getNode('Node1', 'V');
            %
            % See also GET GETNAMES
            
            attVal = obj.c_circuit('get_node', nodeName, attName);
        end
        
        % getNames --------------------------------------------------------
        function names = getNames(obj, list_name)
            % GETNAMES  Gets the names of a type of items in the circuit.
            %
            % Name = Circuit.getNames(option);
            %
            % INPUTS  
            %   option -> Type of items to get the names (string). It may
            %             be 'Imp', 'Isrc', 'Vsrc', 'Control', 'Node',
            %             'Subcircuit', 'PQ', 'PV' or 'Txfo'.
            %
            % OUTPUT
            %   List of the names.
            %
            % EXAMPLES 
            %   names = c.getNames('Imp');
            %
            % See also GET GETNAMES
            
        	names = obj.c_circuit('get_names', list_name);
        end
        

        %% Set methods
        
        % set -------------------------------------------------------------
        function set(obj, compName, varargin)
            % SET  Sets the values attributes of a circuit elements.
            %
            % Circuit.set(compName, att1, val1, att2, val2, ...);
            %
            % INPUTS  
            %   compName -> Name of the item we want to modify.
            %   att -> Name of the attribute we want to modify. It may be
            %          for example, 'V', 'Z' or 'P'. To see each attribute
            %          available for each type of component, see full
            %          documentation.
            %   val -> Value to give to the attribute.
            %
            % Any number of pair <Att,Val> can be provided, as long as
            % there is an equal number of Att and Val.
            %
            % NOTE: There is one specific case. If the compName is given
            %       as 'IONodes', just give one input, that is a cell
            %       of strings, listing the interface nodes of the circuit.
            %
            % EXAMPLES 
            %   c.set('V1', 'V', 120);
            %   c.set('R1', 'V', '120', 'on', 0);
            %   c.set('IONodes', {'n1, 'n2', 'n3', 'n4'}); % Special case
            %
            % See also SETNODE
            
            if (length(varargin) == 1)
                obj.c_circuit('set', compName, varargin{1});
            elseif ~mod(length(varargin), 2)    % Si le nombre d'attributs � modifier et le nombre de valeur fournies ne correspondent pas.
                for i = 1:2:length(varargin)
                	obj.c_circuit('set',  compName, varargin{i}, varargin{i+1});
                end
            end
        end
        
        % setNode ---------------------------------------------------------
        function setNode(obj, nodeName, attName, val)
            % SETNODE  Sets the value of an attribute of a node.
            %
            % Name = Circuit.setNode(nodeName, attName, val);
            %
            % INPUTS  
            %   nodeName -> Name of the node we want to modify.
            %   attName -> Name of the attribute we want to know. Usually,
            %              the attribute will be 'on'.
            %   val -> Value to give to the attribute.
            %
            % EXAMPLES 
            %   out = c.setNode('Node1', 'on', 0); % 0 = OFF
            %
            % See also SET
            
            obj.c_circuit('set_node', nodeName, attName, val);
        end
        

        %% Other methods
                        
        % solve -----------------------------------------------------------
        function solve(obj)
            % SOLVE  Solves the circuit.
            % This method enumerates the circuit if needed, makes
            % validations concerning the topology, solves the circuit and
            % stores the data into the circuit.
            %
            % EXAMPLE
            %   c.solve();

            [ok, names, values] = obj.c_circuit('solve');

            assignValue = @(name, value) evalin('base', [name '=' num2str(value) ';']);
            cellfun(assignValue, names, values);
        end
        
        % print -----------------------------------------------------------
        function print(obj, param)
            % PRINT Print useful information.
            % Prints information on the screen, according to option.
            %
            % INPUT
            %   Option -> Option (string) that tells the function what to
            %             print on the screen. Possible options are :
            %             'Node', 'Comp', 'Subcircuit' and 'Control'.
            %
            % EXAMPLE
            %   c.print('Subcircuit');
            
            obj.c_circuit('print', param);
        end

        % setIs3Phase ------------------------------------------------------
        function setIs3Phase(obj, status)
            % setIs3Phase  Sets the subcircuit as 3 phase or 1 phase.
            %
            % INPUT
            %   status -> If 1, the circuit is 3 phase, balanced. The
            %             current is adapted when it is an output. If 0.
            %             the circuit has one phase (default).
            %
            % EXAMPLE
            %   c.setIs3Phase(1);

            obj.is3Phase = status;
        end

    end   % End of public methods
    
end   % End of circuit