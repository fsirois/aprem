//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
// Circuit Interface for MATLAB
// This file is an interface for the circuitClass class declared in the
// circuitClass.h file.
// Since MATLAB is unable to support C++ directly, a Handle is created to
// keep the address of the circuit object created. It is returned to
// MATLAB is the form of a integer.
// The handle deletes the circuit object when the MATLAB circuit object
// is deleted.
//      - Deleted when MATLAB circuit object is cleared
//          circuitA = Circuit();
//          ...
//          clear; % circuitA destructor called
//
//      - Deleted when MATLAB circuit object is redefined.
//          circuitA = Circuit();
//          circuitB = Circuit();
//          ...
//          circuitA = circuitB; % circuitA destructor called
//          circuitA = Circuit(circuitB); % circuitA destructor NOT called
//          
//-------------------------------------------------------------------------
// Created by : Kevin Ratelle
// Created at : 5053 Resther, Montréal
//-------------------------------------------------------------------------

#include "c_circuit.h"

using namespace std;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	// Get the command string
	char buff[64];

	if (nrhs < 1 || mxGetString(prhs[1], buff, sizeof(buff)))
		mexErrMsgTxt("First input should be a command string less than 64 characters long.");
	
	string cmd = buff;
	
	//---------------------------------------------------------------------
	// Constructors
	//---------------------------------------------------------------------
	if (cmd == "new")
	{
		// Check parameters
		if (nlhs != 1)
			mexErrMsgTxt("New: One output expected.");
		
		// Default constructor
		if (nrhs == 2)
		{
		   plhs[0] = create_handle(new circuit);
		}
		// Copy constructor
		else if (nrhs == 3)
		{
			mxArray * CircuitToCopyHandle = mxGetProperty(prhs[2], 0, "objectHandle");
			circuit * CircuitToCopyInstance = get_object<circuit>(CircuitToCopyHandle);
			if (CircuitToCopyInstance == NULL)
				mexErrMsgTxt("There was an error with the handle.");
			plhs[0] = create_handle(new circuit(CircuitToCopyInstance));
		}
		else
		{
			string ErrorMessage = "Constructor must take 0 or 1 input, and received ";
			ErrorMessage += circuit_ns::IntToString((int) nrhs);
			ErrorMessage += " inputs";
			mexErrMsgTxt(ErrorMessage.c_str());
		}
		return;
	}
	
	// Check there is a second input, which should be the class instance handle
	if (nrhs < 2)
		mexErrMsgTxt("Second input should be a class instance handle.");

	//---------------------------------------------------------------------
	// Destructor
	//---------------------------------------------------------------------
	if (cmd == "delete")
	{
		mxArray * Handle = mxGetProperty(prhs[0], 0, "objectHandle");
		destroy_object<circuit>(Handle);
		
		// Warning if other commands were ignored
		if (nlhs != 0 || nrhs != 2)
			mexWarnMsgTxt("Delete: Unexpected arguments ignored.");
		
		return;
	}
	
	// Get the class instance pointer from the second input
	mxArray * Handle = mxGetProperty(prhs[0], 0, "objectHandle");
	circuit * circuit_instance = get_object<circuit>(Handle);
	if (circuit_instance == NULL)
		mexErrMsgTxt("There was an error with the handle.");

	//---------------------------------------------------------------------
	// Solve
	//---------------------------------------------------------------------
	if (cmd == "solve")
	{
		if(nrhs != 2){
			mexWarnMsgIdAndTxt("APREM:solve:TooManyArg","Solve function");
		}
		if(!mxIsClass(prhs[0], "Circuit"))
			mexErrMsgTxt("First argument of function checkTopology must be a Circuit object.  Example: circuit.checkTopology() or checkTopology(circuit)");
		//if(nlhs > 1) 
		//	mexErrMsgTxt("Too many output arguments.");

		for (const ParametrizedComponent& parametrizedComponent : circuit_instance->m_ParametrizedComponents)
		{
			const string& compName = parametrizedComponent.getName();
			const mxArray* value = mexGetVariablePtr("base", compName.c_str());
			if (value == nullptr)
			{
				continue;
			}

			Component* component = circuit_instance->GetComponentByName(compName);
			if (component == nullptr)
			{
				continue;
			}

			const double* realPtr = mxGetPr(value);
			const double* imagPtr = mxGetPi(value);
			if (realPtr == nullptr && imagPtr == nullptr)
			{
				continue;
			}

			for (int propertyIndex = 0; propertyIndex < parametrizedComponent.m_vsProperties.size(); propertyIndex++)
			{
				const ParametrizedProperty parametrizedProperty = parametrizedComponent.m_vsProperties[propertyIndex];

				const double real = (realPtr != nullptr ? realPtr[propertyIndex] : 0.0f) * parametrizedProperty.m_fMultiplier;
				const double imag = (imagPtr != nullptr ? imagPtr[propertyIndex] : 0.0f) * parametrizedProperty.m_fMultiplier;

				circuit_instance->set(compName, parametrizedProperty.m_sProperty, real, imag);
			}

		}

		CircuitErrorCode solveOutput = circuit_instance->solve();

		mxArray* array_ptr = mxCreateDoubleMatrix(1, 1, mxREAL);

		double* start_of_pr = (double*) mxGetPr(array_ptr);
		*start_of_pr = solveOutput.IsValid() ? 0.0 : 1.0;

		plhs[0] = array_ptr;

		std::vector<std::string> varNames;
		std::vector<std::pair<double, double>> values;

		if (!solveOutput.IsValid())
		{
			mexWarnMsgTxt(solveOutput.GetErrorMessage().c_str());
		}
		else
		{
			circuit_instance->BuildOutput(varNames, values);
		}

		plhs[1] = mex_circuit_ns::string_to_mex(varNames);
		plhs[2] = mex_circuit_ns::complex_to_mex(values);

		return;
	}
	
	//---------------------------------------------------------------------
	// Load circuit from text file
	//---------------------------------------------------------------------
	if (cmd == "load")
	{
		const string filePath = mex_circuit_ns::mx_to_string(prhs[2]);

		CircuitErrorCode errorCode;
		const DrawInfo result = circuit_instance->Load(filePath, errorCode);

		if (!errorCode.IsValid())
		{
			mexErrMsgTxt(errorCode.GetErrorMessage().c_str());
			return;
		}

		const auto& items = result.ComponentDrawInfoArray;
		int n_out = static_cast<int> (items.size());

		constexpr int countPerItem = 3;
		mxArray* mx_out = mxCreateCellMatrix(countPerItem, n_out);

		for (int itemIndex = 0; itemIndex < items.size(); itemIndex++)
		{
			const int valueIndex = countPerItem * itemIndex;

			const DrawInfoItem& item = items[itemIndex];
			mxSetCell(mx_out, valueIndex, mxCreateString(item.Name.c_str()));

			mxSetCell(mx_out, valueIndex + 1, mxCreateString(item.Info.c_str()));

			mxArray* nodeCell = mxCreateCellMatrix(1, item.NodeNames.size());
			for (int nodeIndex = 0; nodeIndex < item.NodeNames.size(); nodeIndex++)
			{
				const std::string& nodeName = item.NodeNames[nodeIndex];
				mxSetCell(nodeCell, nodeIndex, mxCreateString(nodeName.c_str()));
			}

			mxSetCell(mx_out, valueIndex + 2, nodeCell);
		}

		mxArray* mx_draw_strings = mxCreateCellMatrix(1, result.DrawStrings.size());

		for (int drawStringIndex = 0; drawStringIndex < result.DrawStrings.size(); drawStringIndex++)
		{
			mxSetCell(mx_draw_strings, drawStringIndex, mxCreateString(result.DrawStrings[drawStringIndex].c_str()));
		}

		plhs[0] = mx_out;
		plhs[1] = mxCreateString(result.DrawParam.c_str());
		plhs[2] = mx_draw_strings;

		return;
	}
	
	//---------------------------------------------------------------------
	// Methods interface from MATLAB to C++
	//---------------------------------------------------------------------
	if (cmd == "add")
	{
		mex_circuit_ns::mex_add(circuit_instance, nlhs, plhs, nrhs, prhs);
		return;
	}
	
	if (cmd == "del")
	{
		mex_circuit_ns::mex_del(circuit_instance, nlhs, plhs, nrhs, prhs);
		return;
	}
   
	if (cmd == "set")
	{
		mex_circuit_ns::mex_set(circuit_instance, nlhs, plhs, nrhs, prhs);
		return;
	}
	
	if (cmd == "get")
	{
		mex_circuit_ns::mex_get(circuit_instance, nlhs, plhs, nrhs, prhs);
		return;
	}
	
	if (cmd == "set_node")
	{
		mex_circuit_ns::mex_set_node(circuit_instance, nlhs, plhs, nrhs, prhs);
		return;
	}
	
	if (cmd == "get_node")
	{
		mex_circuit_ns::mex_get_node(circuit_instance, nlhs, plhs, nrhs, prhs);
		return;
	}
	
	if (cmd == "print")
	{
		mex_circuit_ns::mex_print(circuit_instance, nlhs, plhs, nrhs, prhs);
		return;
	}
	
	if (cmd == "get_names")
	{
		mex_circuit_ns::mex_get_names(circuit_instance, nlhs, plhs, nrhs, prhs);
		return;
	}
	
	// Got here, so command not recognized
	string output = "Command not recognized : " + cmd;
	mexErrMsgTxt(output.c_str());
}


