//-------------------------------------------------------------------------
// Add elements (MATLAB/C++ interface)
//-------------------------------------------------------------------------

#include "c_circuit.h"

namespace mex_circuit_ns
{

// Generic add ------------------------------------------------------------
void mex_add(circuit * circuit_instance,
             int nlhs, mxArray *plhs[],
             int nrhs, const mxArray *prhs[])
{

std::string type_str = mex_circuit_ns::mx_to_string(prhs[2]);
std::string name = mex_circuit_ns::mx_to_string(prhs[3]);    

ElementType element_type = circuit_ns::StringToElementType(type_str);
std::string output_name;

switch (element_type)
{
    case ElementType::Subcircuit:
    {
        // Circuit to add as a subcircuit
        mxArray * SubCircuitHandle = mxGetProperty(prhs[5], 0, "objectHandle");
        circuit * SubcircuitInstance = get_object<circuit>(SubCircuitHandle);

        if (SubcircuitInstance->get_nIONodes() == 0)
           mexErrMsgTxt("IONodes expected");

        // Name of IONodes
        size_t nIONodes_in = mxGetNumberOfElements(prhs[4]);
        if (nIONodes_in != SubcircuitInstance->get_nIONodes())
            mexErrMsgTxt("Number of nodes must be equal");

        std::vector<std::string> IONodes = mex_circuit_ns::mx_to_string_vector(prhs[4]);

        bool out = circuit_instance->addSubcircuit(SubcircuitInstance, name, IONodes);

        if (!out)
            mexErrMsgTxt("Error, name already exist");

        output_name = name;
        break;
    }
    case ElementType::Control:
    {    
        std::string function_name = mx_to_string(prhs[4]);
        /* TODO : Validate existence of such a file */
        
        output_name = mex_circuit_ns::mex_add_control(name,
                                                      function_name,
                                                      circuit_instance,
                                                      prhs[5]);
        break;
    }
    default:
    {
        if (nrhs != 6)
            mexErrMsgTxt("Wrong number of inputs");

		std::vector<double> params = mex_circuit_ns::mx_to_double_vector(prhs[5]);

		std::vector<std::string> nodeList = mex_circuit_ns::mx_to_string_vector(prhs[4]);

		CircuitErrorCode errorCode;
		output_name = circuit_instance->addComponent(name, nodeList, params, type_str, errorCode);

		break;
	}
}

plhs[0] = mxCreateString(output_name.c_str());
}


// GAdd control -----------------------------------------------------------
std::string mex_add_control(std::string name,
                            std::string functionName,
                            circuit * circuit_instance,
                            const mxArray * mx_data)
{
    // 0 - functionData
    // 1 - inputs
    // 2 -  outputs
    if (!mxIsCell(mx_data))
        mexErrMsgTxt("Last input to add control should be a cell");
    
    size_t in_size = mxGetNumberOfElements(mx_data);
    if (in_size != 3)
        mexErrMsgTxt("Number of inputs must be 3");

    // Name of inputComponents
    std::vector<std::string> inputs = mex_circuit_ns::mx_to_string_vector(mxGetCell(mx_data, 1));
    std::vector<std::string> outputs = mex_circuit_ns::mx_to_string_vector(mxGetCell(mx_data, 2));

    // Name of outputComponents
    std::vector<std::pair<double,double>> functionDataPair = mex_circuit_ns::mx_to_complex_vector(mxGetCell(mx_data, 0));
    std::vector<double> functionData;
    for (int i=0; i<functionDataPair.size(); ++i)
    {
        functionData.push_back(functionDataPair[i].first);
    }
    
    std::vector<std::string> inputComponents;
    std::vector<std::string> inputAttributes;
    for (int i=0; i<inputs.size(); ++i)
    {
        size_t pos = inputs[i].find('.');
        if (pos == std::string::npos)
            mexErrMsgTxt("Each input must have the syntax : name.field");
        
        inputComponents.push_back(inputs[i].substr(0, pos));
        inputAttributes.push_back(inputs[i].substr(pos+1, inputs[i].length()));
    }
    
    std::vector<std::string> outputComponents;
    std::vector<std::string> outputAttributes;
    for (int i=0; i<outputs.size(); ++i)
    {
        size_t pos = outputs[i].find('.');
        if (pos == std::string::npos)
            mexErrMsgTxt("Each output must have the syntax : name.field");
        
        outputComponents.push_back(outputs[i].substr(0, pos));
        outputAttributes.push_back(outputs[i].substr(pos+1, outputs[i].length()));
    }
        
    std::string CtrlName = circuit_instance->addControl(inputComponents,
                                                   inputAttributes,
                                                   outputComponents,
                                                   outputAttributes,
                                                   functionData,
                                                   functionName,
                                                   name);

    if (CtrlName.size() == 0)
        mexErrMsgTxt("Error, component do not exist.");

    return CtrlName;

}
}

