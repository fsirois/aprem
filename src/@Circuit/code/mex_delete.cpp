//-------------------------------------------------------------------------
// Delete (MATLAB/C++ interface)
//-------------------------------------------------------------------------

#include "c_circuit.h"

namespace mex_circuit_ns
{
    
void mex_del(circuit * circuit_instance, int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    std::vector<std::string> names = mx_to_string_vector(prhs[2]);
	circuit_instance->deleteElements(names);
}
}

