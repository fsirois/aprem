//-------------------------------------------------------------------------
// Set/Get methods (MATLAB/C++ interface)
//-------------------------------------------------------------------------

#include "c_circuit.h"

namespace mex_circuit_ns
{

//-------------------------------------------------------------------------
// Set methods
//-------------------------------------------------------------------------
    
// Set --------------------------------------------------------------------
void mex_set(circuit * circuit_instance,
             int nlhs, mxArray *plhs[],
             int nrhs, const mxArray *prhs[])
{
    plhs[0] = mxCreateDoubleMatrix(1,1,mxREAL);
    double *out = (double *) mxGetPr(plhs[0]);
    
    std::string name = mx_to_string(prhs[2]);
    
    if (name == "IONodes")
    {
        if (nrhs != 4)
            mexErrMsgTxt("Number of inputs should be 4");

        std::vector<std::string> input_output_nodes = mx_to_string_vector(prhs[3]);
        *out = (double) circuit_instance->setIONodes(input_output_nodes);
        return;
    }

    std::string field = mx_to_string(prhs[3]);

    ElementType elem_type = circuit_instance->getElementType(name);
    if (elem_type == ElementType::Invalid)
    {
        mexErrMsgTxt("Circuit element not found.");
    }

    bool FieldIsCorrect = false;
    if (elem_type == ElementType::Component)
    {
        const double * in_r = (double *) mxGetPr(prhs[4]);
        const double * in_i = (double *) mxGetPi(prhs[4]);
        
        const double inReal = *in_r;
        const double inImaginary = in_i != nullptr ? *in_i : 0.0;
 
        FieldIsCorrect = circuit_instance->set(name, field, inReal, inImaginary);
    }
    else if (elem_type == ElementType::Control)
    {
        FieldIsCorrect = mex_set_control(circuit_instance, prhs);
    }

	if (!FieldIsCorrect)
	{
		mexErrMsgTxt("Error on the field and element.");
	}

	return;
}

bool mex_set_control(circuit * circuit_instance,
	const mxArray * prhs[])
{
    bool FieldIsCorrect = false;
    
    std::string field_to_set = mx_to_string(prhs[3]);
    std::string control_name = mx_to_string(prhs[2]);
    
    if (field_to_set == "on")
    {
        bool * on_in = (bool *) mxGetPr(prhs[4]);
        circuit_instance->setControlOn(control_name, *on_in);
    }
    else if (field_to_set == "functionName")
    {
        std::string function_name = mx_to_string(prhs[4]);
        circuit_instance->setControlFunctionName(control_name, function_name);
    }
    else if (field_to_set == "functionData")
    {
        std::vector<double> data_vector = mx_to_double_vector(prhs[4]);
        circuit_instance->setControlFunctionData(control_name, data_vector);
    }
    else
    {
        std::vector<std::string> ComponentVector;
        std::vector<std::string> AttributeVector;

        ComponentVector = mx_to_string_vector(prhs[4]);
            
        if (field_to_set == "inputs" || field_to_set == "outputs")
            AttributeVector = mx_to_string_vector(prhs[5]);
        
        circuit_instance->setControl(control_name,
                                     field_to_set,
                                     ComponentVector,
                                     AttributeVector);
    }
    
    return FieldIsCorrect;
}

// General set for nodes --------------------------------------------------
void mex_set_node(circuit * circuit_instance,
                  int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    std::string node_name = mx_to_string(prhs[2]);
    std::string field_to_set = mx_to_string(prhs[3]);

    double * input = (double *) mxGetPr(prhs[4]);
    circuit_instance->setNode(node_name, field_to_set, (int) *input);
    return;
}

//-------------------------------------------------------------------------
// Get methods
//-------------------------------------------------------------------------

// Get --------------------------------------------------------------------
void mex_get(circuit * circuit_instance,
             int nlhs, mxArray *plhs[],
             int nrhs, const mxArray *prhs[])
{
    std::string name = mx_to_string(prhs[2]);
    std::string field = mx_to_string(prhs[3]);

    if (field == "nodes")
    {
        std::vector<std::string> NodeNames = circuit_instance->getComponentNodeNames(name);
        plhs[0] = mex_circuit_ns::string_to_mex(NodeNames);
        return;
    }

    std::vector<std::pair<double,double>> Output;
    if (field == "V")
       Output = circuit_instance->getV(name);
    else
       Output = circuit_instance->get(name, field);

    plhs[0] = mxCreateDoubleMatrix(1,(int) Output.size(),mxCOMPLEX);
    double * out = (double *) mxGetPr(plhs[0]);
    double * out_imag = (double *) mxGetPi(plhs[0]);
    for (int i=0; i<Output.size(); i++)
    {
        *(out + i) = Output[i].first;
        *(out_imag + i) = Output[i].second;
    }
    return;
}   

// Get node ---------------------------------------------------------------
void mex_get_node(circuit * circuit_instance,
                  int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    std::string node_name = mx_to_string(prhs[2]);
    std::string field = mx_to_string(prhs[3]);

    double *out;
    double *out_imag;
    if (field == "V")
    {
        std::pair<double,double> Out = circuit_instance->getNodeV(node_name);

        plhs[0] = mxCreateDoubleMatrix(1,1,mxCOMPLEX);
        out = (double *) mxGetPr(plhs[0]);
        *out = Out.first;

        out_imag = (double *) mxGetPi(plhs[0]);
        *out_imag = Out.second;
     }      
     else
     {
        std::vector<double> Out =  circuit_instance->getNode(node_name, field);
        plhs[0] = mxCreateDoubleMatrix(1,(int) Out.size(), mxREAL);
        out = (double *) mxGetPr(plhs[0]);
        for (int i=0; i<Out.size(); ++i)
        {
            *(out+i) = (double) Out[i];
        }
     }

    return;
}

// Get lists of names -----------------------------------------------------
void mex_get_names(circuit * circuit_instance,
                  int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    if (nlhs != 1)
        mexErrMsgTxt("Number of outputs should be 1.");
    
    std::string option = mex_circuit_ns::mx_to_string(prhs[2]);
    std::vector<std::string> returnNames = circuit_instance->getNames(option);
    
    plhs[0] = mex_circuit_ns::string_to_mex(returnNames);
    return;
}
}