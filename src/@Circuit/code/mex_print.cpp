//-------------------------------------------------------------------------
// Print (MATLAB/C++ interface)
//-------------------------------------------------------------------------
#include "c_circuit.h"

namespace mex_circuit_ns
{
void mex_print(circuit * circuit_instance, int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{ 
	std::string option = mex_circuit_ns::mx_to_string(prhs[2]);
	circuit_instance->print(option);
}
}

