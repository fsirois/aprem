//-------------------------------------------------------------------------
// Support functions to interface with MATLAB
//-------------------------------------------------------------------------
#include "c_circuit.h"

using namespace std;

namespace mex_circuit_ns
{
    // Convert from a string to mex (output) ------------------------------
    mxArray * string_to_mex(vector<string> str_out)
    {
		int n_out = static_cast<int> (str_out.size());
		mxArray * mx_out = mxCreateCellMatrix(1,n_out);
		for (int i=0; i<n_out; ++i)
			mxSetCell(mx_out, i, mxCreateString(str_out[i].c_str()));
    
		return mx_out;
    }

	mxArray * complex_to_mex(const vector<pair<double, double>>& out)
	{
		mxArray * mx_out = mxCreateCellMatrix(1, out.size());
		for (size_t i = 0; i < out.size(); ++i)
		{
			mxArray * complexValue = mxCreateDoubleMatrix(1, 1, mxCOMPLEX);

			double * realPtr = mxGetPr(complexValue);
			*realPtr = out[i].first;
			double * imagPtr = mxGetPi(complexValue);
			*imagPtr = out[i].second;

			mxSetCell(mx_out, i, complexValue);
		}

		return mx_out;
	}

    // Convert from a mxArray to string (input) ---------------------------
    string mx_to_string(const mxArray * mx_string)
    {
        char buff[64];
        string name;
        mxGetString(mx_string, buff, sizeof(buff));
        name = buff;
        
        return name;
    }
    
    // Convert from a mxArray to string vector (input) --------------------
    vector<string> mx_to_string_vector(const mxArray * mx_string_cell)
    {
        vector<string> str_vec;
        if (!mxIsCell(mx_string_cell))
        {
            str_vec.push_back(mx_to_string(mx_string_cell));
            return str_vec;
        }
        const mxArray * pCellElement;
        size_t str_vec_size = mxGetNumberOfElements(mx_string_cell);

        for (int i = 0; i < str_vec_size; ++i)
        {
            pCellElement = mxGetCell(mx_string_cell, i);
            string current_node =  mx_to_string(pCellElement);
            str_vec.push_back(current_node);
        }
        
        return str_vec;
    }
    
    vector<double> mx_to_double_vector(const mxArray * mx_double_cell)
    {
        
        size_t numberDoubleValues = mxGetNumberOfElements(mx_double_cell);
        vector<double> double_vector;
        double * tempDouble;
        const mxArray * pCellElement;
        for (int i = 0; i < numberDoubleValues; i++)
        {
            pCellElement = mxGetCell(mx_double_cell, i);
            tempDouble = mxGetPr(pCellElement);
            double_vector.push_back(*tempDouble);
        }
        
        return double_vector;
    }
    
   // Convert from a mxArray to vector of pair of doubles (input) ---------
   vector<pair<double,double>> mx_to_complex_vector (mxArray * mx_cell)
   {
        vector<pair<double,double>> complex_vec;
        
        double * tempDouble;
        if (!mxIsCell(mx_cell))
        {
            pair<double,double> complex;
            
            tempDouble = mxGetPr(mx_cell);
            complex.first = tempDouble == NULL ? 0 : *tempDouble;
            
            tempDouble = mxGetPi(mx_cell);
            complex.second = tempDouble == NULL ? 0 : *tempDouble;
            
            complex_vec.push_back(complex);
            return complex_vec;
        }
                
        size_t numberDouble = mxGetNumberOfElements(mx_cell);
        
        pair<double,double> tempPair;
        const mxArray *pCellElement;
        for (int i = 0; i < numberDouble; ++i)
        {
            tempPair.first = 0;
            tempPair.second = 0;
            
            pCellElement = mxGetCell(mx_cell, i);
            tempDouble = mxGetPr(pCellElement);
            
            if (tempDouble!=NULL)
                tempPair.first = *tempDouble;
            
            tempDouble = mxGetPi(pCellElement);
           
            if (tempDouble!=NULL)
                tempPair.second = *tempDouble;
            
            complex_vec.push_back(tempPair);
        }
        
        return complex_vec;
   }
}




