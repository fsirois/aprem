
#include "call_control_function.h"

using namespace std;

pair<bool,vector<pair<double,double>>> call_control_function(string functionName,
                                                       vector<double> functionData,
                                                       vector<pair<double,double>> data_in,
                                                       vector<pair<double,double>> data_out)
{
    // Here, in this case, we call a MATLAB function, but in any other
    // application, one could reprogram this function to call a C/C++
    // function or even a Python, Java, etc function.
    pair<bool, vector<pair<double,double>>> newDataOut;
    
    //---------------------------------------------------------------------
    // The following part is proper to MATLAB
    //---------------------------------------------------------------------
    mxArray *output_array[2], *input_array[3];
    
    mwSize * dims;
    int dim = static_cast<int> (data_in.size());
    dims = reinterpret_cast<mwSize*> (&dim);
    input_array[0] = mxCreateCellArray(1, dims);
    for (int i=0; i<data_in.size(); i++)
    {
        mxArray * doubleValue = mxCreateDoubleMatrix(1,1, mxCOMPLEX);
        double * dbl_ptr = mxGetPr(doubleValue);
        *dbl_ptr = data_in[i].first;
        dbl_ptr = mxGetPi(doubleValue);
        *dbl_ptr = data_in[i].second;
        mxSetCell(input_array[0], i, doubleValue);
//         mxDestroyArray(doubleValue);
    }

    dim = static_cast<int> (functionData.size());
    input_array[1] = mxCreateCellArray(1, dims);
    for (int i=0; i<functionData.size(); i++)
    {
        mxArray * doubleValue = mxCreateDoubleScalar(functionData[i]);
        mxSetCell(input_array[1], i, doubleValue);
        //mxDestroyArray(doubleValue);
    }

    dim = static_cast<int> (data_out.size());
    input_array[2] = mxCreateCellArray(1, dims);
    for (int i=0; i<data_out.size(); i++)
    {
        mxArray * doubleValue = mxCreateDoubleMatrix(1,1, mxCOMPLEX);
        double * dbl_ptr = mxGetPr(doubleValue);
        *dbl_ptr = data_out[i].first;
        dbl_ptr = mxGetPi(doubleValue);
        *dbl_ptr = data_out[i].second;
        mxSetCell(input_array[2], i, doubleValue);
        //mxDestroyArray(doubleValue);
    }
    
    int out = mexCallMATLAB(2, output_array, 3, input_array, functionName.c_str());
    if (out)
    {
        newDataOut.first = false;
        return (newDataOut);
    }

    double * ptr = mxGetPr(output_array[1]);
    if (static_cast<int> (*ptr) == 0)
        newDataOut.first = false;
    else
        newDataOut.first = true;
    
    size_t n_out = mxGetNumberOfElements(output_array[0]);
    if (n_out != data_out.size())
    {
        newDataOut.first = false;
        return (newDataOut);
    }
  
    const mxArray *pCellElement;
    for (int i=0; i<data_out.size(); i++)
    {
        pCellElement = mxGetCell(output_array[0], i);
        pair<double,double> out_pair;
        out_pair.first = 0;
        out_pair.second = 0;
        double * tmp_ptr;
        tmp_ptr = mxGetPr(pCellElement);
        if (tmp_ptr != NULL)
            out_pair.first = *tmp_ptr;
        tmp_ptr = mxGetPi(pCellElement);
        if (tmp_ptr != NULL)
            out_pair.second = *tmp_ptr;
        newDataOut.second.push_back(out_pair);
    }

    mxDestroyArray(input_array[0]);
    mxDestroyArray(input_array[1]);
    mxDestroyArray(input_array[2]);

   /* mxDestroyArray(output_array[0]);
    mxDestroyArray(output_array[1]);*/
    
    return (newDataOut);
}
