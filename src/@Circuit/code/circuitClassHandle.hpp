//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
// C++ handle for MATLAB
// This file allows to create handles for different C++ objects and give
// the HANDLE to MATLAB as a UINT64.
//
// The functions to use on the C/C++ side are :
//    - create_handle :  Create an handle for MATLAB, that you can return
//                       to MATLAB directly.
//    - get_object :     Get the object from the handle. You can get the
//                       handle from the MATLAB object.
//    - destroy_object : Destroy the object contained in the handle. Never
//                       delete the object directly.
//-------------------------------------------------------------------------
// Kevin Ratelle 2015
// Adapted from a design by Tim Bailey 2004.
//-------------------------------------------------------------------------

#ifndef OBJECT_HANDLE_H
#define OBJECT_HANDLE_H

#include <mex.h>
#include <typeinfo>
#include <list>
#include <stdint.h>

enum ManageHandleMode
{
    Register,
    Delete
};

// Classes declaration
template<typename T> class ObjectHandle;
template<typename T> class Collector;

//-------------------------------------------------------------------------
// Interface functions - Only functions used in circuit.cpp explicity
//-------------------------------------------------------------------------

// Create a handle for the object
template <typename T>
mxArray *create_handle(T* t)
{
	ObjectHandle<T>* handle= new ObjectHandle<T>(t);
	return handle->to_mex_handle();
}

// Get the object from the handle
template <typename T>
T* get_object(const mxArray *mxh)
{
	ObjectHandle<T>* handle= ObjectHandle<T>::from_mex_handle(mxh);
    
    if (!ObjectHandle<T>::validate_type(handle))
        return NULL;
        
	return handle->get_object();
}

// Delete the object, from the handle (it is the owner)
template <typename T>
void destroy_object(const mxArray *mxh)
{
	ObjectHandle<T>* handle= ObjectHandle<T>::from_mex_handle(mxh);
    Collector<T>::manage_handle(handle, Delete);
}

//-------------------------------------------------------------------------
// Object handle - Declaration
//-------------------------------------------------------------------------
// A handle will be responsible for deleting the object
// It it the ONLY object allowed to delete an instance of the handled
// object (circuit). Not the circuit interface, not the collector.
template <typename T>
class ObjectHandle
{
public:
	ObjectHandle(T*& ptr) : type(&typeid(T)), t(ptr)
    { 
		signature = this; 
		Collector<T>::manage_handle(this, Register);
		ptr = 0;
	} 

	~ObjectHandle()
    { 
        if (signature != NULL)
            delete t;
		signature= NULL;
	} 

	mxArray* to_mex_handle(); 
	static ObjectHandle* from_mex_handle( const mxArray* ma );
    static bool validate_type(const ObjectHandle * const obj);
	inline T* get_object() const { return t; }
    
private:
	ObjectHandle* signature; 
	const std::type_info * const type;
	T *t;
	friend class Collector<T>;
};

//-------------------------------------------------------------------------
// Object handle - Implementation
// ------------------------------------------------------------------------
// Import a handle from MatLab as a mxArray of UINT64. Check that
// it is actually a pointer to an ObjectHandle<T>.
template <typename T>
ObjectHandle<T>* ObjectHandle<T>::from_mex_handle(const mxArray* handle)
{
	if (mxGetClassID(handle) != mxUINT64_CLASS 
		|| mxIsComplex(handle) || mxGetM(handle)!=1 || mxGetN(handle)!=1)
		mexErrMsgTxt("Parameter is not an ObjectHandle type.");

	// We *assume* we can store ObjectHandle<T> pointer in the mxUINT64 of handle
    ObjectHandle* obj = *reinterpret_cast<ObjectHandle<T>**>(mxGetPr(handle));

	return obj;
}

template <typename T>
bool ObjectHandle<T>::validate_type(const ObjectHandle* const obj)
{
    // Check memory has correct signature
	if (obj->signature != obj)
    {
		mexErrMsgTxt("Parameter does not represent an ObjectHandle object.");
        return false;
    }
    
    // Check type 
	if (*(obj->type) != typeid(T)) 
    { 
		mexPrintf("Given: <%s>, Required: <%s>.\n", obj->type->name(), typeid(T).name());
		mexErrMsgTxt("Given ObjectHandle does not represent the correct type.");
        return false;
	}

	return true;
}

// Create a numeric array as handle for an ObjectHandle.
// We ASSUME we can store object pointer in the mxUINT64 element of mxArray.
template <typename T>
mxArray* ObjectHandle<T>::to_mex_handle() 
{
	mxArray* handle  = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
    *reinterpret_cast<ObjectHandle<T>**>(mxGetPr(handle)) = this;
	return handle;
}

//-------------------------------------------------------------------------
// Collection
//-------------------------------------------------------------------------
// Collection singleton (one collector object for each type T).
// Ensures that registered handles are deleted when the dll is released.
// The Collector provides protection against resource leaks in the case
// where 'clear all' is called in MatLab. This is because MatLab will call
// the destructors of statically allocated objects but not free-store
// allocated objects.
// The collector is he only object allowed to delete handles. Nothing else.
// The handles themselves are responsible to delete the objects they
// handle (each one a different circuit), nobody else.
template <typename T>
class Collector
{
public:
	~Collector()
    {
		typename std::list<ObjectHandle<T>*>::iterator i;
		typename std::list<ObjectHandle<T>*>::iterator end= objlist.end();
		for (i= objlist.begin(); i!=end; ++i)
        {
			if (*i != NULL && (*i)->signature == *i) // check for valid signature
				delete *i;
		}
	}

	static void manage_handle (ObjectHandle<T>* handle, ManageHandleMode Mode)
    {
        // This singleton lists all the pointers to all the handles
        // of the given type T
        static Collector singleton;
        
        // If we are in register mode, it will add the handle to the list
        if (Mode == Register)
        {
            singleton.objlist.push_back(handle);
        }
        
        // If in delete mode, delete object of given pointer if contained
        // in the list of managed objects by the collector
        else /*Delete*/
        {
            typename std::list<ObjectHandle<T>*>::iterator i;
            typename std::list<ObjectHandle<T>*>::iterator end = singleton.objlist.end();
            for (i= singleton.objlist.begin(); i!=end; ++i)
            {
                if (*i == handle) // check for valid signature
                {
                    delete *i;
                    *i = NULL;
                }
            }
        }
	}

private: 
    // Prevent construction
	Collector() {}
	Collector(const Collector&);
    
    // List of items to control the references
    std::list<ObjectHandle<T>*> objlist;
};

#endif /* Object handle */






