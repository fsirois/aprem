//-------------------------------------------------------------------------
// Circuit interface for MATLAB - Declaration
//-------------------------------------------------------------------------

#include "mex.h"
#include "circuitClassHandle.hpp" // class handle for MATLAB
#include "circuitClass.h"         // C++ circuit class

namespace mex_circuit_ns
{
    // Methods accessible from MATLAB -------------------------------------
    void mex_add(circuit * circuit_instance,
                 int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]);
    
    void mex_del(circuit * circuit_instance,
                 int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]);
    
    void mex_set(circuit * circuit_instance,
                 int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]);
    
    void mex_get(circuit * circuit_instance,
                 int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[]);
    
    void mex_set_node(circuit * circuit_instance,
                      int nlhs, mxArray *plhs[],
                      int nrhs, const mxArray *prhs[]);
    
    void mex_get_node(circuit * circuit_instance,
                      int nlhs, mxArray *plhs[],
                      int nrhs, const mxArray *prhs[]);
    
    void mex_get_names(circuit * circuit_instance,
                      int nlhs, mxArray *plhs[],
                      int nrhs, const mxArray *prhs[]);
    
    void mex_print(circuit * circuit_instance,
                   int nlhs, mxArray *plhs[],
                   int nrhs, const mxArray *prhs[]);

    // Support functions --------------------------------------------------
    std::string mex_add_control(std::string name,
                                std::string function_name,
                                circuit * circuit_instance,
                                const mxArray * input);
    
    bool mex_set_control(circuit * circuit_instance,
                         const mxArray * prhs[]);
    
    std::vector<double> mx_to_double_vector(const mxArray * mx_double_cell);
    std::string mx_to_string(const mxArray * mx_string);
    mxArray * string_to_mex(std::vector<std::string> str_out);
    std::vector<std::string> mx_to_string_vector(const mxArray * mx_string_cell);
	mxArray * complex_to_mex(const std::vector<std::pair<double, double>>& out);
    std::vector<std::pair<double,double>> mx_to_complex_vector (mxArray * mx_complex_cell);
}
