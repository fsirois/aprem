
#include "CircuitFactory.h"

CircuitFactory::~CircuitFactory()
{
	for (int i=0; i<m_CircuitVector.size(); i++)
	{
		delete m_CircuitVector[i];
	}
}
	
CircuitInterface * CircuitFactory::CreateCircuit()
{
	circuit * newCircuit = new circuit();
	
	m_CircuitVector.push_back(newCircuit);
	
	return newCircuit;
}

CircuitInterface * CircuitFactory::CopyCircuit(CircuitInterface * circuitToCopy)
{
	circuit * newCircuit = new circuit(static_cast<circuit*> (circuitToCopy));
	
	m_CircuitVector.push_back(newCircuit);
	
	return newCircuit;
}
