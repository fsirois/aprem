
#ifndef CIRCUIT_FACTORY_H
#define CIRCUIT_FACTORY_H

#include <vector>
#include "CircuitInterface.h"
#include "circuitClass.h"

class CircuitFactory
{
public:
	CircuitFactory(){}
	~CircuitFactory();
	
	CircuitInterface * CreateCircuit();
	CircuitInterface * CopyCircuit(CircuitInterface * circuitToCopy);

private:
	std::vector<circuit*> m_CircuitVector;
};

#endif