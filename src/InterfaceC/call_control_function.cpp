
#include "call_control_function.h"

using namespace std;

pair<bool,vector<pair<double,double> > > call_control_function(string functionName,
															   vector<double> functionData,
															   vector<pair<double,double> > data_in,
															   vector<pair<double,double> > data_out)
{
	// should be done outside : the return should be done by reference
    pair<bool, vector<pair<double,double> > > newDataOut;
	newDataOut.second.resize(data_in.size());
	
	newDataOut.first = FunctionHandler::CallFunction(functionName,
													 data_in,
													 functionData,
													 data_out,
													 newDataOut.second);
	return newDataOut;
}



