
#ifndef FUNCTIONHANDLER_H
#define FUNCTIONHANDLER_H

#include <vector>
#include <utility>
#include <map>
#include <string>

typedef bool (*controlFunction)(const std::vector<std::pair<double,double> > & data_in,
				   const std::vector<double> & control_data,
				   const std::vector<std::pair<double, double> > & data_out,
					 std::vector<std::pair<double, double> > & output);

class FunctionHandler
{
public:	
	static void RegisterFunction(std::string functionName, controlFunction fct);
	static void UnregisterFunction(std::string functionName);
	
	static bool CallFunction(const std::string & functionName,
							 const std::vector<std::pair<double,double> > & data_in,
							 const std::vector<double> & control_data,
							 const std::vector<std::pair<double, double> > & data_out,
								   std::vector<std::pair<double, double> > & output);
	
private:
	static std::map<std::string, controlFunction> m_controlFunctionMap;
};

#endif
