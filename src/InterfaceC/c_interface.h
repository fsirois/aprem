#include <iostream>

#include "circuitClass.h"

extern "C"
{
	circuit * Circuit_new();
	circuit * Circuit_new_copy(circuit * CircuitToCopy);
	int Circuit_delete(circuit * circuit_instance);

	bool Circuit_solve(circuit * const circuit_instance);
	
	const char * Circuit_add_component(circuit * const circuit_instance,
									   const char * const type,
									   const char * const c_name,
									   const char * const * const node_list,
									   int n_node,
									   const double * const value,
									   int n_value);

	/*
		...
		const char * Circuit_add_control();
		const char * Circuit_add_subcircuit();
	*/

	void Circuit_get_nodeV(const circuit * const circuit_instance,
						   const char * const node_name_c,
						   double * const out_real,
						   double * const out_imag);

	int Circuit_get_node(const circuit * const circuit_instance,
						 const char * const node_name_c,
						 const char * const field_c,
						 double * const out);
						 
	void Circuit_print(const circuit * const circuit_instance,
					   const char * const param);
}
