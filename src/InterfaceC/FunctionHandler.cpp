
#include "FunctionHandler.h"

#include <stdio.h>

using namespace std;

// definition of the static member variable
map<std::string, controlFunction> FunctionHandler::m_controlFunctionMap;
		
void FunctionHandler::RegisterFunction(std::string functionName, controlFunction fct)
{
	m_controlFunctionMap[functionName] = fct;
}

void FunctionHandler::UnregisterFunction(std::string functionName)
{
	// unimplemented
}
	
bool FunctionHandler::CallFunction(const string & functionName,
										  const vector<pair<double,double> > & dataIn,
										  const vector<double> & functionData,
										  const vector<pair<double, double> > & dataOut,
												vector<pair<double, double> > & output)
{
	controlFunction funct = m_controlFunctionMap[functionName];
	
	bool outBool = (*funct)(dataIn, functionData, dataOut, output);
	
	return outBool;	   
}