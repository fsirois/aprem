
#include "c_interface.h"

using namespace std;

circuit * Circuit_new()
{
	return new circuit();
}


circuit * Circuit_new_copy(circuit * CircuitToCopy)
{
	return new circuit(CircuitToCopy);
}

int Circuit_delete(circuit * circuit_instance)
{
	delete circuit_instance;
	return 1;
}

bool Circuit_solve(circuit * const circuit_instance)
{
	return circuit_instance->solve().first;
}

const char * Circuit_add_component(circuit * const circuit_instance,
								   const char * const c_type,
								   const char * const c_name,
								   const char * const * const node_list,
								   int n_node,
								   const double * const value,
								   int n_value)
{
	vector<double> params;
	params.resize(n_value);
	for (int i=0; i<n_value; i++)
		params[i] = value[i];

	vector<string> nodeList;
	nodeList.resize(n_node);
	for (int i=0; i<n_node; i++)
		nodeList[i] = node_list[i];
	
	string output_name = circuit_instance->addComponent(c_name, nodeList, params, c_type);

	return output_name.c_str();
}


void Circuit_get_nodeV(const circuit * const circuit_instance,
					   const char * const node_name_c,
					   double * const out_real,
					   double * const out_imag)
{
        pair<double,double> Out = circuit_instance->getNodeV(node_name_c);
		
		*out_real = Out.first;
		*out_imag = Out.second;
}

int Circuit_get_node(const circuit * const circuit_instance,
					 const char * const node_name_c,
					 const char * const field_c,
					 double * const out)
{
	vector<double> OutVector =  circuit_instance->getNode(node_name_c, field_c);
	
	int n_item = OutVector.size();
	for (int i=0; i<n_item; i++)
		out[i] = OutVector[i];
	
	return n_item;
}

void Circuit_print(const circuit * const circuit_instance,
				   const char * const c_param)
{
	circuit_instance->print(c_param);	   
}
					   
/* ... */









