
#ifndef CALL_CONTROL_FUNCTION
#define CALL_CONTROL_FUNCTION

#include "Python.h"
#include "circuitClass.h"
#include "PythonInterpreter.h"

std::pair<bool,std::vector<std::pair<double,double> > >
call_control_function(std::string functionName,
                      std::vector<double> functionData,
                      std::vector<std::pair<double,double> > data_in,
                      std::vector<std::pair<double,double> > data_out);

#endif
