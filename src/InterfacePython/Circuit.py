﻿# -*- coding: utf-8 -*-

from ctypes import *

class Circuit:

    #--------------------------------------------------------------------------
    # Constructor of the class superconductor
    #--------------------------------------------------------------------------
	def __init__(self, *args):
	    
		self.g3PHASE = 0 # Indicates if the circuit has 1 or 3 phases.
		self.maxIterControl = 100 # Number of iterations max when solving.
		
		self.circuitLib = cdll.LoadLibrary('libCircuit.so')
	
		nargin = len(args);
	
		# Default constructor
		if nargin == 0:
			self.circuitLib.Circuit_new.restype = c_int
			self.circuitHandle = self.circuitLib.Circuit_new()
			
		# Copy constructor
		elif nargin == 1:
			
			circuitToCopy = args[0]
			circuitHandleToCopy = circuitToCopy.circuitHandle
			
			#if ~isa(varargin{1}, 'Circuit')
				#error('Argument must either be nothing or a Circuit object.');
			#end
			
			self.circuitLib.Circuit_new_copy.argtypes = [c_int]
			self.circuitLib.Circuit_new_copy.restype = c_int
			self.circuitHandle = self.circuitLib.Circuit_new_copy(circuitHandleToCopy)

			self.g3PHASE = circuitToCopy.g3PHASE
			
		else:
			print ("Error too many arguments.")
	
	# Destructor ------------------------------------------------------
	def __del__(self):
		''' do something'''
		if self.circuitHandle != None:
			self.circuitLib.Circuit_delete.argtypes = [c_int]
			self.circuitLib.Circuit_delete(self.circuitHandle)
			
		self.circuitHandle = None
		
    # add -------------------------------------------------------------
	def add(self, type, name, node_list, lst):
		'''
		ADD  Adds an item to the circuit.
		
		Name = Circuit.add(type, name, nodes, param);
		
		 INPUTS  
		   type -> Type of item to add to the circuit. (string)
				   Options are: Imp Isrc Vsrc Sw Txfo PQ PV
								Control and Subcircuit
		   nodes -> List of the nodes of the element. In the case
					of a control, it is the name of the MATLAB
					function acting as a control. (cell of strings)
		   param -> List of parameters to pass to the circuit.
					For detail of the parameters, see full DOC. (cell)
		
		 OUTPUT
		   Name of the item added to the circuit. (string)
		
		 EXAMPLES 
		   c.add('Vsrc', 'Vsrc', {'nodeAlpha', '0'}, {120, 0});
		
		   c1 = Circuit();
		   ...
		   c2.add('Subcircuit', 'sub_a', {'n1','n2','n3','n4'}, c2);
		
		 See also DEL
		'''
		
		self.circuitLib.Circuit_add.argtypes = [c_int, c_char_p, c_char_p, py_object, py_object]
		self.circuitLib.Circuit_add.restype = c_char_p

		type = type.encode()
		name = name.encode()
		
		# If adding a subcircuit
		if isinstance(lst, Circuit):
			lst = lst.circuitHandle

		# If adding anything else
		else:
			if isinstance(lst, complex):
				tmplst = lst
				lst = [tmplst.real, tmplst.imag]
				
			if not isinstance(lst, list):
				lst = [lst]
			
		out = self.circuitLib.Circuit_add(self.circuitHandle, type, name, node_list, lst)
		out = out.decode()
			
		return out
		
	def print(self, option):
		self.circuitLib.Circuit_print.argtypes = [c_int, c_char_p]
		
		option = option.encode()
		self.circuitLib.Circuit_print(self.circuitHandle, option)
		
	def getNames(self, list_name):
		''' GETNAMES  Gets the names of a type of items in the circuit
            %
            % Name = Circuit.getNames(option);
            %
            % INPUTS  
            %   option -> Type of items to get the names (string). It may
            %             be 'Imp', 'Isrc', 'Vsrc', 'Control', 'Node',
            %             'Subcircuit', 'PQ', 'PV' or 'Txfo'.
            %
            % OUTPUT
            %   List of the names.
            %
            % EXAMPLES 
            %   names = c.getNames('Imp');
            %
            % See also GET GETNAMES
		'''
		self.circuitLib.Circuit_get_names.restype = py_object
		
		list_name = list_name.encode()
		
		names = self.circuitLib.Circuit_get_names(self.circuitHandle, list_name);
		
		return names
		
	def dele(self, items_to_delete):
		self.circuitLib.Circuit_del.argtypes = [c_int, py_object]
	
		if not isinstance(items_to_delete, list):
			items_to_delete = [items_to_delete]
			
		self.circuitLib.Circuit_del(self.circuitHandle, items_to_delete)
		
		return 1
	
	def setComp(self, compList, paramList, valueList):
		''' '''
		self.circuitLib.Circuit_set_comp.argtypes = [c_int, py_object, py_object, py_object]
		self.circuitLib.Circuit_set_comp.restype = c_int
		
		out = self.circuitLib.Circuit_set_comp(self.circuitHandle, compList, paramList, valueList)
		
		return out
		
	def getComp(self, compList, paramList):
		self.circuitLib.Circuit_get_comp.argtypes = [c_int, py_object, py_object]
		self.circuitLib.Circuit_get_comp.restype = py_object
		
		out = self.circuitLib.Circuit_get_comp(self.circuitHandle, compList, paramList)
		
	def set(self, compName, *args):
		'''SET  Sets the values attributes of a circuit elements.
            %
            % Circuit.set(compName, att1, val1, att2, val2, ...);
            %
            % INPUTS  
            %   compName -> Name of the item we want to modify.
            %   att -> Name of the attribute we want to modify. It may be
            %          for example, 'V', 'Z' or 'P'. To see each attribute
            %          available for each type of component, see full
            %          documentation.
            %   val -> Value to give to the attribute.
            %
            % Any number of pair <Att,Val> can be provided, as long as
            % there is an equal number of Att and Val.
            %
            % NOTE: There is one specific case. If the compName is given
            %       as 'IONodes', just give one input, that is a cell
            %       of strings, listing the interface nodes of the circuit.
            %
            % EXAMPLES 
            %   c.set('V1', 'V', 120);
            %   c.set('R1', 'V', '120', 'on', 0);
            %   c.set('IONodes', {'n1, 'n2', 'n3', 'n4'}); % Special case
            %
            % See also SETNODE
		'''
		attVal = None

		if len(args) == 1:
			
			parameterName = compName.encode()
			
			val = args[0]
			
			self.circuitLib.Circuit_set.argtypes = [c_int, c_char_p, py_object]
			self.circuitLib.Circuit_set.restype = c_int
			out = self.circuitLib.Circuit_set(self.circuitHandle, parameterName, val)
			
		else:
			if (len(args) % 2) != 0: # Si le nombre d'attributs à modifier et le nombre de valeur fournies ne correspondent pas.
				return attVal
		
			nbComp = int(len(args) / 2)

			self.circuitLib.Circuit_set_elem.argtypes = [c_int, c_char_p, c_char_p, py_object]
			self.circuitLib.Circuit_set_elem.restype = c_int
			
			compName = compName.encode()
			for i in range(nbComp):								
				attName = args[2*i].encode()
				value = args[2*i+1]
				out = self.circuitLib.Circuit_set_elem(self.circuitHandle, compName, attName, value)

		return attVal
	
	def get(self, *args):
		'''
		GET  Gets the value of an attribute of a circuit element.
            
             Name = Circuit.get(comp1, att1, comp2, att2, ...);
            
             INPUTS  
               comp -> Name of the item we are interested in.
               att -> Name of the attribute we want to know. It may be
                      for example, 'V', 'I' or 'S'. To see each attribute
                      available for each type of component, see full
                      documentation.
            
             Any number of pair <Comp, Att> can be provided, as long
             as the number of inputs is even.
            
             OUTPUT
               Value or list of values of the attributes for given items.
            
             EXAMPLES 
               out = c.get('V1', 'V');
               out = c.get('R1', 'I', 'PV1', 'S');
               out = c.get({'Comp1', 'nodes', 'Comp1', 'on'});
            
             See also GETNODE GETNAMES
		'''
		
		self.circuitLib.Circuit_get.argtypes = [c_int, c_char_p, c_char_p]
		self.circuitLib.Circuit_get.restype = py_object
			
		# Number of names and number of attributes must be equal.
		attVal = None
		if (len(args) % 2) != 0:
			return attVal
			
		nbComp = int(len(args) / 2)
		
		# Get attribute value for each component
		attVal = [None] * nbComp
		for k in range(nbComp):
                
			# Get component name and attribute name
			compName = args[2*k].encode()
			attName = args[2*k+1].encode()
             
			# If the circuit is a 3 phased one, correct currents
			t = self.circuitLib.Circuit_get(self.circuitHandle, compName, attName)
			
			attVal[k] = t
			
			if (self.g3PHASE == 1 and (strcmp(attName,'I') or strcmp(attName,'Is') or strcmp(attName,'Iabs') or strcmp(attName, 'Inom'))):
				attVal[k] = attVal[k] / sqrt(3)
            
		# If length of output is one, do not return as a cell
		if len(attVal) == 1:
			attVal = attVal[0];
			
		return attVal
	
	def getNode(self, nodeName, attName):
		'''
		GETNODE  Gets the value of an attribute of a node.

        Name = Circuit.getNode(nodeName, attribute);
            
            INPUTS  
               nodeName -> Name of the node we are interested in.
               att -> Name of the attribute we want to know. It may be
                      'on' or 'V'.
            
             OUTPUT
               Value of the node attribute.
            
             EXAMPLES 
               out = c.getNode('Node1', 'V');
            
             See also GET GETNAMES
        '''
		nodeName = nodeName.encode()
		attName = attName.encode()
		
		self.circuitLib.Circuit_get_node.argtypes = [c_int, c_char_p, c_char_p]
		self.circuitLib.Circuit_get_node.restype = py_object
		t = self.circuitLib.Circuit_get_node(self.circuitHandle, nodeName, attName)
		
		attVal = t
		return attVal
        #attVal = obj.c_circuit('get_node', nodeName, attName);
	
	def setNode(self, nodeName, attName, val):
		'''
            % SETNODE  Sets the value of an attribute of a node.
            %
            % Name = Circuit.setNode(nodeName, attName, val);
            %
            % INPUTS  
            %   nodeName -> Name of the node we want to modify.
            %   attName -> Name of the attribute we want to know. Usually,
            %              the attribute will be 'on'.
            %   val -> Value to give to the attribute.
            %
            % EXAMPLES 
            %   out = c.setNode('Node1', 'on', 0); % 0 = OFF
            %
            % See also SET
        '''
		
		nodeName = nodeName.encode()
		attName = attName.encode()
			
		self.circuitLib.Circuit_set_node.argtypes = [c_int, c_char_p, c_char_p, c_int]
		self.circuitLib.Circuit_set_node.restype = c_int
		out = self.circuitLib.Circuit_set_node(self.circuitHandle, nodeName, attName, val)
		#obj.c_circuit('set_node', nodeName, attName, val);
		return out
	
	def solve(self):
		''' '''
		self.circuitLib.Circuit_solve.argtypes = [c_int]
		self.circuitLib.Circuit_solve.restype = c_int
		out = self.circuitLib.Circuit_solve(self.circuitHandle)
		return out
	
	
	# setG3PHASE ------------------------------------------------------
	def setG3PHASE(self, status):
		''' SETG3PHASE  Sets the subcircuit as 3 phase or 1 phase.
            %
            % INPUT
            %   status -> If 1, the circuit is 3 phase, balanced. The
            %             current is adapted when it is an output. If 0.
            %             the circuit has one phase (default).
            %
            % EXAMPLE
            %   c.setG3PHASE(1);
		'''
		
		self.g3PHASE = status
		return
	
	
