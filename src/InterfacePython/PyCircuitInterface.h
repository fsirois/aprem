#include <iostream>

#include "PythonSupport.h"

extern "C"
{
    circuit * Circuit_new();
	circuit * Circuit_new_copy(circuit * CircuitToCopy);
	
	int Circuit_delete(circuit * circuit_instance);
								
	const char * Circuit_add(circuit * circuit_instance,
							 char * type,
							 char * c_name,
							 PyObject * node_list,
							 PyObject * value);
							 
	int Circuit_print(circuit * circuit_instance,
					  char * option);
					  
	PyObject * Circuit_get_names(circuit * circuit_instance,
							     char * option);
	
	
	int Circuit_del(circuit * circuit_instance,
				    PyObject * names_py);
	
    PyObject * Circuit_get(circuit * circuit_instance,
						   char * name_c,
						   char * field_c);
						   
	PyObject * Circuit_get_comp(circuit * circuit_instance,
								PyObject * CompList,
								PyObject * AttList);
						   
	PyObject * Circuit_get_node(circuit * circuit_instance,
								char * node_name_c,
								char * field_c);

	int Circuit_set_node(circuit * circuit_instance,
						 char * node_name_c,
					     char * field_to_set_c,
					     int input);
	
    int Circuit_set(circuit * circuit_instance,
                    char * param_name,
                    PyObject * value);
					
	int Circuit_set_elem(circuit * circuit_instance,
					     char * comp_name,
					     char * att_name,
					     PyObject * value);

    int Circuit_set_comp(circuit * circuit_instance,
						 PyObject * CompList,
						 PyObject * AttList,
						 PyObject * ValueList);
                
	int Circuit_solve(circuit * circuit_instance);
}

bool py_set_control(circuit * circuit_instance,
				   std::string control_name,
				   std::string field_to_set,
                   PyObject * value);
				   
std::string py_add_control(std::string name,
						   std::string functionName,
                      circuit * circuit_instance,
                      PyObject * py_data);




