
#ifndef PYTHON_SUPPORT_HEADER
#define PYTHON_SUPPORT_HEADER

#include <Python.h>
#include "circuitClass.h"

// Data from CPython to C++
std::vector<double> py_to_double_vector(PyObject * value);
std::vector<std::string> py_to_string_vector(PyObject * value);
std::vector<std::pair<double,double> > py_to_complex_vector(PyObject * value);

// Data from C++ to CPython
PyObject * string_vector_to_python(std::vector<std::string> NamesVector);
PyObject * complex_vector_to_python(const std::vector<std::pair<double,double> > & ComplexVector);

#endif /* PYTHON_SUPPORT_HEADER */
