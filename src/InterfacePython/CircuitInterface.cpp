
#include "PyCircuitInterface.h"

using namespace std;

circuit * Circuit_new()
{
	return new circuit();
}


circuit * Circuit_new_copy(circuit * CircuitToCopy)
{
	circuit * Circuit = NULL;
	if (CircuitToCopy != NULL)
		Circuit = new circuit(CircuitToCopy);
	else
		printf("There was an error with the handle.");
	
	return Circuit;
}


int Circuit_delete(circuit * circuit_instance)
{
	delete circuit_instance;
	return 1;
}

int Circuit_print(circuit * circuit_instance, char * option_c)
{ 
	string option = option_c;
	circuit_instance->print(option);
	
	return 1;
}

// Get lists of names -----------------------------------------------------
PyObject * Circuit_get_names(circuit * circuit_instance,
							 char * option_c)
{
	string option = option_c;
    vector<string> NamesVector = circuit_instance->getNames(option);
    
	PyObject * result = string_vector_to_python(NamesVector);
    return result;
}

int Circuit_del(circuit * circuit_instance,
				PyObject * names_py)
{
	vector<string> names = py_to_string_vector(names_py);
	circuit_instance->deleteElements(names);
}

int Circuit_solve(circuit * circuit_instance)
{	
	circuit_instance->solve();
	return 1;
}








