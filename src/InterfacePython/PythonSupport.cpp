
#include<PythonSupport.h>

using namespace std;

//----------------------------------------------------------
// C++ to CPython
//----------------------------------------------------------
PyObject * string_vector_to_python(vector<string> NamesVector)
{
    PyObject * result = PyList_New(0);
	for (int i=0; i<NamesVector.size(); ++i)
	{
		string curName = NamesVector[i];
		PyList_Append(result, PyUnicode_FromString(curName.c_str()));
	}
	return result;
}

PyObject * complex_vector_to_python(const vector<pair<double,double> > & ComplexVector)
{
	PyObject * result = PyList_New(0);
	for (int i=0; i<ComplexVector.size(); ++i)
	{
		double real = ComplexVector[i].first;
		double imag = ComplexVector[i].second;
		PyList_Append(result, PyComplex_FromDoubles(real, imag));
	}
	
	return result;
}

//-------------------------------------
// CPython to C++
//------------------------------------
vector<string> py_to_string_vector(PyObject * value)
{ 
	Py_ssize_t NodeListSize = PyList_Size(value);
	vector<string> string_vector;
	string_vector.resize(NodeListSize);

	for (Py_ssize_t i=0; i<NodeListSize; ++i)
	{
		PyObject * strObject = PyList_GetItem(value,i);
		string_vector[i] = PyUnicode_AsUTF8(strObject);
	}
	
	return string_vector;
}

vector<double> py_to_double_vector(PyObject * value)
{ 
	vector<double> double_vector;
	
	Py_ssize_t NodeListSize = PyList_Size(value);
	for (Py_ssize_t i=0; i<NodeListSize; ++i)
	{
		PyObject * FloatObject = PyList_GetItem(value,i);
		double tmp = PyFloat_AsDouble(FloatObject);
		double_vector.push_back(tmp);
	}
	
	return double_vector;
}

vector<pair<double,double> > py_to_complex_vector (PyObject * value)
   {
	vector<pair<double,double> > complex_vec;
	
	double tempDouble;
	if (PyComplex_Check(value))
	{
		pair<double,double> complex;
		
		complex.first = PyComplex_RealAsDouble(value);
		complex.second = PyComplex_ImagAsDouble(value);
		
		complex_vec.push_back(complex);
		return complex_vec;
	}
        
	if (PyList_Check(value))
	{			
	    pair<double,double> tempPair;
	    Py_ssize_t NodeListSize = PyList_Size(value);
	    complex_vec.resize(NodeListSize);

	    for (int i = 0; i < NodeListSize; ++i)
	    {
		tempPair.first = 0;
		tempPair.second = 0;
			
		PyObject * ComplexObject = PyList_GetItem(value,i);
		//if (!PyComplex_Check(ComplexObject))
			//continue;
			
		tempPair.first = PyComplex_RealAsDouble(ComplexObject);		
		tempPair.second = PyComplex_ImagAsDouble(ComplexObject);
			
		complex_vec[i] = tempPair;
	    }
	}
        
        return complex_vec;
   }
