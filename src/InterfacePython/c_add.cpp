//-------------------------------------------------------------------------
// Add elements (MATLAB/C++ interface)
//-------------------------------------------------------------------------

#include "PyCircuitInterface.h"

using namespace std;

// Generic add ------------------------------------------------------------
const char * Circuit_add(circuit * circuit_instance,
						 char * type,
						 char * c_name,
						 PyObject * node_list,
						 PyObject * value)
{

string type_str = type;
string name = c_name;

ElementType element_type = circuit_ns::StringToElementType(type_str);
string output_name;

switch (element_type)
{
    case ElementType::Subcircuit:
    {
        // Circuit to add as a subcircuit
		long Address = PyLong_AsLong(value);
		circuit * SubcircuitInstance = reinterpret_cast<circuit*> (Address);

        if (SubcircuitInstance->get_nIONodes() == 0)
           printf("IONodes expected");

        vector<string> IONodes = py_to_string_vector(node_list);
        if (IONodes.size() != SubcircuitInstance->get_nIONodes())
            printf("Number of nodes must be equal");

        bool out = circuit_instance->addSubcircuit(SubcircuitInstance, name, IONodes);

        if (!out)
            printf("Error, name already exist");

        output_name = name;
        break;
    }
    case ElementType::Control:
    {    
        string function_name = PyUnicode_AsUTF8(node_list);
        /* TODO : Validate existence of such a file */
        
        output_name = py_add_control(name,
                                     function_name,
                                     circuit_instance,
                                     value);
        break;
    }
    default:
    {
        vector<double> params = py_to_double_vector(value);

		vector<string> nodeList = py_to_string_vector(node_list);
		
		output_name = circuit_instance->addComponent(name, nodeList, params, type_str);
        
        break;
    }

}

return output_name.c_str();

}

// Add control -----------------------------------------------------------
string py_add_control(std::string name,
					  std::string functionName,
                      circuit * circuit_instance,
                      PyObject * py_data)
{
    // Name of inputComponents
    vector<string> inputs = py_to_string_vector(PyList_GetItem(py_data,1));
    vector<string> outputs = py_to_string_vector(PyList_GetItem(py_data,2));

    // Name of outputComponents
    vector<pair<double,double> > functionDataPair = py_to_complex_vector(PyList_GetItem(py_data, 0));
    vector<double> functionData;
    for (int i=0; i<functionDataPair.size(); ++i)
    {
        functionData.push_back(functionDataPair[i].first);
    }
    
    vector<string> inputComponents;
    vector<string> inputAttributes;
    for (int i=0; i<inputs.size(); ++i)
    {
        size_t pos = inputs[i].find('.');
        if (pos == string::npos)
            printf("Each input must have the syntax : name.field");
        
        inputComponents.push_back(inputs[i].substr(0, pos));
        inputAttributes.push_back(inputs[i].substr(pos+1, inputs[i].length()));
    }
    
    vector<string> outputComponents;
    vector<string> outputAttributes;
    for (int i=0; i<outputs.size(); ++i)
    {
        size_t pos = outputs[i].find('.');
        if (pos == string::npos)
            printf("Each output must have the syntax : name.field");
        
        outputComponents.push_back(outputs[i].substr(0, pos));
        outputAttributes.push_back(outputs[i].substr(pos+1, outputs[i].length()));
    }
        
    string CtrlName = circuit_instance->addControl(inputComponents,
                                                   inputAttributes,
                                                   outputComponents,
                                                   outputAttributes,
                                                   functionData,
                                                   functionName,
                                                   name);

    if (CtrlName.size() == 0)
        printf("Error, component do not exist.");

    return CtrlName;

}


