//-------------------------------------------------------------------------
// Set/Get methods (MATLAB/C++ interface)
//-------------------------------------------------------------------------

#include "PyCircuitInterface.h"

using namespace std;

//-------------------------------------------------------------------------
// Set methods
//-------------------------------------------------------------------------
    
// Set --------------------------------------------------------------------
int Circuit_set(circuit * circuit_instance,
                char * param_name,
                PyObject * value)
{
    string name = param_name;
    bool out = false;
    
    if (name == "IONodes")
    {
        vector<string> input_output_nodes = py_to_string_vector(value);
        
        out = circuit_instance->setIONodes(input_output_nodes);
    }
    return 1;
}

int Circuit_set_elem(circuit * circuit_instance,
					 char * comp_name,
					 char * att_name,
					 PyObject * value)
{ 
	string name = comp_name;
    string field = att_name;

    ElementType elem_type = circuit_instance->getElementType(name);
    if (elem_type == ElementType_NULL)
    {
        printf("Circuit element not found.");
    }

    bool FieldIsCorrect = false;
    if (elem_type == ElementType::Component)
    {
		double in_r = PyComplex_RealAsDouble(value);
		double in_i = PyComplex_ImagAsDouble(value);

        FieldIsCorrect = circuit_instance->set(name, field, in_r, in_i);
    }
    else if (elem_type == ElementType::Control)
    {
        FieldIsCorrect = py_set_control(circuit_instance,
									   comp_name,
									   att_name,
									   value);
    }
    if (!FieldIsCorrect)
        printf("Error on the field and element.");

	return 1;
}

int Circuit_set_comp(circuit * circuit_instance,
                     PyObject * CompList,
					 PyObject * AttList,
                     PyObject * ValueList)
{ 
	vector<string> compVector = py_to_string_vector(CompList);
	vector<string> attVector = py_to_string_vector(AttList);
    vector<pair<double,double> > valueVector = py_to_complex_vector(ValueList);

	int OK = circuit_instance->setComponents(compVector,
											 attVector,
										     valueVector);

	return OK;
}

bool py_set_control(circuit * circuit_instance,
			 	    string control_name,
			 	    string field_to_set,
                    PyObject * value)
{
    bool FieldIsCorrect = false;
    
    if (field_to_set == "on")
    {
        bool on_in = (bool) PyLong_AsLong(value);
        circuit_instance->setControlOn(control_name, on_in);
    }
    else if (field_to_set == "functionName")
    {
        string function_name = PyUnicode_AsUTF8(value);
        circuit_instance->setControlFunctionName(control_name, function_name);
    }
    else if (field_to_set == "functionData")
    {
		PyObject * PyListObject = PyList_GetItem(value,0);
        vector<double> data_vector = py_to_double_vector(PyListObject);
        circuit_instance->setControlFunctionData(control_name, data_vector);
    }
    else
    {
        vector<string> ComponentVector;
        vector<string> AttributeVector;

		PyObject * PyListObject = PyList_GetItem(value,0);
        ComponentVector = py_to_string_vector(PyListObject);
            
        if (field_to_set == "inputs" || field_to_set == "outputs")
		{
			PyObject * PyListObject = PyList_GetItem(value,1);
            AttributeVector = py_to_string_vector(PyListObject);
		}
        
        circuit_instance->setControl(control_name,
                                     field_to_set,
                                     ComponentVector,
                                     AttributeVector);
    }
    
    return FieldIsCorrect;
}

// General set for nodes --------------------------------------------------
int Circuit_set_node(circuit * circuit_instance,
					 char * node_name_c,
					 char * field_to_set_c,
					 int input)
{
    string node_name = node_name_c;
    string field_to_set = field_to_set_c;
	
    circuit_instance->setNode(node_name, field_to_set, input);
	
    return 1;
}

//-------------------------------------------------------------------------
// Get methods
//-------------------------------------------------------------------------

// Get --------------------------------------------------------------------
PyObject * Circuit_get(circuit * circuit_instance,
					   char * name_c,
					   char * field_c)
{
    string name = name_c;
    string field = field_c;

    if (field == "nodes")
    {
        vector<string> NodeNames = circuit_instance->getComponentonentNodeNames(name);
		PyObject * result = string_vector_to_python(NodeNames);
       
        return result;
    }

    vector<pair<double,double> > Output;
    if (field == "V")
       Output = circuit_instance->getV(name);
    else
       Output = circuit_instance->get(name, field);

	PyObject * result = complex_vector_to_python(Output);
    return result;
}   

// Get node ---------------------------------------------------------------
PyObject * Circuit_get_node(circuit * circuit_instance,
							char * node_name_c,
							char * field_c)
{
	PyObject * result = PyList_New(0);

    string node_name = node_name_c;
    string field = field_c;

    if (field == "V")
    {
        pair<double,double> Out = circuit_instance->getNodeV(node_name);
		
		double real = Out.first;
		double imag = Out.second;
		
		PyList_Append(result, PyComplex_FromDoubles(real, imag));

     }      
     else
     {
        vector<double> Out =  circuit_instance->getNode(node_name, field);
		
		for (int i=0; i<Out.size(); ++i)
			PyList_Append(result, PyFloat_FromDouble(Out[i]));
     }

    return result;
}

// Get comp ---------------------------------------------------------
PyObject * Circuit_get_comp(circuit * circuit_instance,
			    PyObject * CompList,
			    PyObject * AttList)
{
	const vector<string> & compVector = py_to_string_vector(CompList);
	const vector<string> & attVector = py_to_string_vector(AttList);
	
    	const vector<pair<double,double> > & valueVector = circuit_instance->getComponents(compVector,
																			   attVector);
																			   
	PyObject * out = complex_vector_to_python(valueVector);													   
						
	return out;
}
	
	
	
	
	
	
	

