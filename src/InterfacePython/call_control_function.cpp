
#include "call_control_function.h"

using namespace std;

pair<bool,vector<pair<double,double> > > call_control_function(string functionName,
															   vector<double> functionData,
															   vector<pair<double,double> > data_in,
															   vector<pair<double,double> > data_out)
{
    // Here, in this case, we call a Python function, but in any other
    // application, one could reprogram this function to call a C/C++
    // function or even a Python, Java, etc function.
    pair<bool, vector<pair<double,double> > > newDataOut;
	newDataOut.first = false;
	if (data_in.size() == 0 || functionData.size() == 0 || data_out.size() == 0)
		return newDataOut;
	
    //---------------------------------------------------------------------
    // The following part is proper to PYTHON
    //---------------------------------------------------------------------	
	
	// The python interpreter class will create a new interpreter, and will
	// go back to the main thread when deleted automatically
	PythonInterpreter PyInterp;

	bool setting_items_valid = true;
	
	// Get the data in, data out and control data from python
	Py_ssize_t dim = static_cast<Py_ssize_t> (data_in.size());
	PyObject * DataIn = PyList_New(dim);
    for (int i=0; i<data_in.size(); i++)
    {
		PyObject * ComplexObject = PyComplex_FromDoubles(data_in[i].first,
														 data_in[i].second);
		if (PyList_SetItem(DataIn, i, ComplexObject) == -1)
			setting_items_valid = false;
    }

	dim = static_cast<Py_ssize_t> (functionData.size());
	PyObject * DataFloat = PyList_New(dim);
    for (int i=0; i<functionData.size(); i++)
    {
        PyObject * FloatObject = PyFloat_FromDouble(functionData[i]);
        if (PyList_SetItem(DataFloat, i, FloatObject) == -1)
			setting_items_valid = false;
    }

	dim = static_cast<Py_ssize_t> (data_out.size());
    PyObject * DataOutPy = PyList_New(dim);
    for (int i=0; i<data_out.size(); i++)
    {
		PyObject * ComplexObject = PyComplex_FromDoubles(data_out[i].first,
														 data_out[i].second);
		if (PyList_SetItem(DataOutPy, i, ComplexObject) == -1)
			setting_items_valid = false;
    }
	
	// If an error is found, clear the memory and return nothing
	if (!setting_items_valid)
	{
		Py_DECREF(DataIn);
		Py_DECREF(DataFloat);
		Py_DECREF(DataOutPy);
		return newDataOut;
	}
	
	// Ask the python interpreter for the function
	PyObject * function = PyInterp.r_getFunction(functionName.c_str());
	if (function == NULL)
	{
		return newDataOut;
	}
	
	// Prepare the data for the call and decrease proper reference counts
	PyObject * args = PyTuple_Pack(3,DataIn, DataFloat, DataOutPy);	
	Py_DECREF(DataIn);
	Py_DECREF(DataFloat);
	Py_DECREF(DataOutPy);
	
	// Call the python function and decrease the ref count
	PyObject * result = PyObject_CallObject(function, args);
	Py_DECREF(function);
	Py_DECREF(args);
	
	if (result == NULL)
	{
		return newDataOut;
	}
	
	// Get the data from the function
	PyObject * flagPy = PyTuple_GetItem(result, 1);
	PyObject * newOutPy = PyTuple_GetItem(result, 0);
	if (flagPy == NULL || newOutPy == NULL ||
	    PyList_Size(newOutPy) != data_out.size())
	{
		Py_DECREF(result);
		return newDataOut;
	}
	
	// Manage flag from python
    long flag = PyLong_AsLong(flagPy);
    if (flag != 0)
        newDataOut.first = true;
  
    // Manage complex data from python
    for (int i=0; i<data_out.size(); i++)
    {
		PyObject * ComplexObject = PyList_GetItem(newOutPy, i);
		Py_complex ComplexNumber = PyComplex_AsCComplex(ComplexObject);
		
        pair<double,double> out_pair;
		out_pair.first = ComplexNumber.real;
		out_pair.second = ComplexNumber.imag;
		
        newDataOut.second.push_back(out_pair);
    }
	
	Py_DECREF(result);
    return (newDataOut);
}



