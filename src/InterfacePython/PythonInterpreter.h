
#ifndef PYTHON_INTERPRETER_HEADER
#define PYTHON_INTERPRETER_HEADER

#include <Python.h>

class PythonInterpreter
{
public:
    PythonInterpreter();
    ~PythonInterpreter();
	PyObject * r_getFunction(const char * name) const;
	
private:
	PyGILState_STATE mainState;
	PyThreadState* mainThreadState;
	PyThreadState* newThreadState;
};

#endif //PYTHON_INTERPRETER_HEADER
