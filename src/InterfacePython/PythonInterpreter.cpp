
#include <PythonInterpreter.h>

PythonInterpreter::PythonInterpreter()
{
	PyEval_InitThreads();
	
	mainState = PyGILState_Ensure();
	mainThreadState = PyThreadState_Get();
	newThreadState = Py_NewInterpreter();
	
	PyThreadState_Swap(newThreadState);
}

PythonInterpreter::~PythonInterpreter()
{
	Py_EndInterpreter(newThreadState);
	PyThreadState_Swap(mainThreadState);
	PyGILState_Release(mainState);
}

PyObject * PythonInterpreter::r_getFunction(const char * name) const
{
	PySys_SetPath(L".");
	
	PyObject * module = PyImport_ImportModule(name);
	if (module == NULL)
		return NULL;
	
	PyObject * function = PyObject_GetAttrString(module, name);
	Py_DECREF(module);
	
	return function;
}