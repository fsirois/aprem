
#include "CircuitParser.h"
#include "support_functions.h"
#include "DrawInfo.h"
#include "Types.h"

using namespace std;

namespace circuit_ns
{
	CircuitOutput ToOutput(const string& str)
	{
		const size_t startParenthesis = str.find('(');
		const size_t endParenthesis = str.find(')');

		const string propertyName = str.substr(0, startParenthesis);
		const string componentName = str.substr(startParenthesis + 1, endParenthesis - startParenthesis - 1);

		return CircuitOutput{ componentName, ToPropertyType(propertyName) };
	}
}

string ToString(const SimulationType type)
{
	switch (type)
	{
	case SimulationType::AC:
		return "AC";
	case SimulationType::DC:
		return "DC";
	case SimulationType::TRAN:
		return "TRAN";
	default:
		break;
	}

	return "Invalid";
}

SimulationType ToSimulationType(const string& name)
{
	if (name == "AC")
	{
		return SimulationType::AC;
	}
	else if (name == "DC")
	{
return SimulationType::DC;
	}
	else if (name == "TRAN")
	{
	return SimulationType::TRAN;
	}

	return SimulationType::Invalid;
}

double GenerateMultiplier(const int exponent, const bool negative = false)
{
	double multiplier = 1.0f;
	double oneStepMultiplier = negative ? 0.1 : 10.0;

	for (int i = 0; i < exponent; i++)
	{
		multiplier *= oneStepMultiplier;
	}

	return multiplier;
}

double CharToMultiplier(const char value)
{
	switch (value)
	{
	case 'f':
		return GenerateMultiplier(15, true);
	case 'p':
		return GenerateMultiplier(12, true);
	case 'n':
		return GenerateMultiplier(9, true);
	case 'u':
		return GenerateMultiplier(6, true);
	case 'm':
		return GenerateMultiplier(3, true);
	case 'k':
		return GenerateMultiplier(3);
	case 'M':
		return GenerateMultiplier(6);
	case 'G':
		return GenerateMultiplier(9);
	case 'T':
		return GenerateMultiplier(12);

	// APREM assumes every angle it gets from the outside is a degree, so the radian is actually the one to correct
	case 'r':
		return circuit_ns::kRadianToDegreeMultiplier;
	}

	return 1.0;
}

double ExponentToMultiplier(const string& value)
{
	if (value.size() == 0)
	{
		return 1.0f;
	}

	bool isNegative = value[0] == '-';
	string exponentString = value.substr(isNegative ? 1 : 0);

	int exponent = 0;
	try
	{
		exponent = stoi(exponentString);
	}
	catch (exception& )
	{
	}

	return GenerateMultiplier(exponent, isNegative);
}

double CircuitParser::ToComplexCartesian(string& value, double& real, double& imag)
{
	const double multiplier = ToMultiplier(value);

	// the number must contain only digits, +, - and j characters
	// for example:
	//	j2
	//	3.0
	//	4.0+j5
	//	1-j7.4
	const size_t minusCharacter = value.find('-');
	const size_t plusCharacter = value.find('+');
	const size_t signCharacter = std::min(minusCharacter, plusCharacter);
	const size_t jCharacter = value.find('j');

	// what comes after the j is the complex part AND is affected by the sign
	if (jCharacter != string::npos)
	{
		const string imaginaryString = value.substr(jCharacter + 1);
		try
		{
			imag = stof(imaginaryString);
		}
		catch (exception&)
		{
			// throw something or return value 
			printf("\n--%s", imaginaryString.c_str());
		}
	}

	if (minusCharacter != string::npos)
	{
		imag *= -1.0;
	}

	if (signCharacter != string::npos || jCharacter == string::npos)
	{
		const string realString = value.substr(0, signCharacter);
		try
		{
			real = stof(realString);
		}
		catch (exception&)
		{
			// throw something or return value 
			printf("\n--%s", realString.c_str());
		}
	}

	return multiplier;
}

double CircuitParser::ToComplexAngular(const string& value, double& real, double& imag)
{
	const size_t angleCharacter = value.find('@');

	string normString = value.substr(0, angleCharacter);
	const double multiplier = ToMultiplier(normString);

	double norm = 0.0;
	try
	{
		norm = stof(normString);
	}
	catch (exception&)
	{
		// throw something or return value 
		printf("\n--%s", normString.c_str());
	}

	string angleString = value.substr(angleCharacter + 1);
	const double angle = circuit_ns::to_radian(ToAngleDegree(angleString));

	circuit_ns::from_angle(norm, angle, real, imag);

	return multiplier;
}

double CircuitParser::ToAngleDegree(string& angleString)
{
	const char lastChar = angleString[angleString.size() - 1];
	bool bIsDegree = false;
	if (lastChar == 'd' || lastChar == 'r')
	{
		bIsDegree = lastChar == 'd';
		angleString.resize(angleString.size() - 1);
	}

	double angle = 0.0;
	try
	{
		angle = stof(angleString);
	}
	catch (exception&)
	{
		// throw something or return value 
		printf("\n--%s", angleString.c_str());
	}

	angle = bIsDegree ? angle : circuit_ns::to_degree(angle);

	return angle;
}


void CircuitParser::ToComplex(const string& valueIn, double& real, double& imag)
{
	real = 0.0;
	imag = 0.0;
	double multiplier = 0.0;

	// take care of multiplier first
	string value = valueIn;

	const size_t angleCharacter = value.find('@');
	const bool complexNumberIsAngular = angleCharacter != string::npos;
	if (complexNumberIsAngular)
	{
		multiplier = ToComplexAngular(value, real, imag);
	}
	else
	{
		multiplier = ToComplexCartesian(value, real, imag);
	}

	real *= multiplier;
	imag *= multiplier;
}

CircuitParser::CircuitParser() : FileParser()
{
	SetFirstLineIsFileName(true);
	SetMainSeparator(FileParser::k_WhiteSpace);
	SetEndLineCommentChar(';');
	SetFullLineCommentChar('*');
	SetAllowEmptyValue(true);
	SetStopAfterVariable(".DRAW");
}

double CircuitParser::ToMultiplier(string& value)
{
	const char lastChar = value[value.size() - 1];
	if (!isdigit(lastChar))
	{
		double multiplier = CharToMultiplier(lastChar);
		if (abs(multiplier - 1.0f) < 0.0001)
		{
			// give warning
		}

		value.resize(value.size() - 1);
		return multiplier;
	}

	for (int i = 0; i < value.size(); i++)
	{
		const char character = value[i];
		if (character == 'e')
		{
			// remainer of string is exponent

			int exponentIndexStart = i + 2;
			const string exponentString = value.substr(exponentIndexStart);

			value.resize(i);

			double multiplier = ExponentToMultiplier(exponentString);
			if (abs(multiplier - 1.0f) < 0.0001)
			{
				// give warning
			}
			return multiplier;
		}
	}

	return 1.0;
}

double CircuitParser::ToDouble(string value)
{
	double multiplier = ToMultiplier(value);

	double out = 0.0;
	try
	{
		out = stof(value);
	}
	catch (exception&)
	{
		// throw something or return value 
	}

	return out * multiplier;
}

PackedCircuit CircuitParser::LoadFile(const string& filePath)
{
	FileParser::LoadFile(filePath);

	return GetParsedData();
}

PackedCircuit CircuitParser::GetParsedData()
{
	PackedCircuit packedCircuit;

	for (const NameParamsPair variablePair : m_IniVariables)
	{
		const string& name = variablePair.first;
		const string& variable = variablePair.second;

		if (name == "FileName")
		{
			packedCircuit.m_sName = variable;
			continue;
		}

		// special variable
		if (name[0] == '.')
		{
			const string specialVariable = name.substr(1);

			if (specialVariable == "TAG")
			{
				packedCircuit.m_sTag = variable;
			}
			else if (specialVariable == "SAVE")
			{
				vector<string> outputs;
				circuit_ns::split(variable, outputs);

				for (int i = 0; i < outputs.size(); i++)
				{
					const circuit_ns::CircuitOutput newOutput = circuit_ns::ToOutput(outputs[i]);
					packedCircuit.m_vCircuitOutput.push_back(newOutput);
				}
			}
			else if (specialVariable == "DRAW")
			{
				packedCircuit.m_sDrawParam = variable;
			}
			else
			{
				const SimulationType simType = ToSimulationType(specialVariable);

				if (simType == SimulationType::Invalid)
				{
					// throw error of some sort
				}

				packedCircuit.m_eSimulationType = simType;
			}
		}
		else if (name.size() > 1)
		{
			ComponentType type = circuit_ns::CharToComponentType(name[0], name[1]);
			if (type != ComponentType::Invalid)
			{
				PackedComponent component;
				component.m_sName = name;
				component.m_sParams = variable;
				component.m_eType = type;
				packedCircuit.m_vComponents.push_back(component);
			}
		}

	}

	return packedCircuit;
}

string CircuitParser::GetParsedDataAsString()
{
	PackedCircuit packedCircuit = GetParsedData();

	string out;
	out.append("\n---- Circuit ----\nCircuit Name : " + packedCircuit.m_sName);
	out.append("\nSimulation Type : " + ToString(packedCircuit.m_eSimulationType));
	out.append("\nTag : " + packedCircuit.m_sTag);

	out.append("\nValues to save : ");
	for (const circuit_ns::CircuitOutput& output : packedCircuit.m_vCircuitOutput)
	{
		const string parameterTypeAsString = ToString(output.m_eType);
		out.append("\n	-" + parameterTypeAsString + " for component " + output.m_sComponentName);
	}

	for (const PackedComponent& component : packedCircuit.m_vComponents)
	{
		const string  componentTypeAsString = circuit_ns::ComponentTypeToString(component.m_eType);
		out.append("\n " + componentTypeAsString + ": " + component.m_sParams);
	}

	return out;
}
