#ifndef CIRCUIT_PARSER_H
#define CIRCUIT_PARSER_H

#include "FileParser.h"

enum class SimulationType
{
	AC = 0,
	DC = 1,
	TRAN = 2,
	Invalid = 3
};

struct PackedComponent
{
	std::string m_sName;
	std::string m_sParams;
	ComponentType m_eType;
};

struct PackedCircuit
{
	SimulationType m_eSimulationType;
	std::string m_sTag;
	std::string m_sName;
	std::string m_sDrawParam;

	std::vector<circuit_ns::CircuitOutput> m_vCircuitOutput;
	std::vector<PackedComponent> m_vComponents;
};

class CircuitParser : public FileParser
{
public:
	CircuitParser();
	
	static double ToAngleDegree(std::string& value);
	static void ToComplex(const std::string& value, double& real, double& imag);
	static double ToDouble(std::string value);
	static double ToMultiplier(std::string& value);
	
	PackedCircuit LoadFile(const std::string& filePath);
	std::string GetParsedDataAsString();

	const std::vector<std::string>& GetDrawStrings() const
	{
		return m_AdditionalContent;
	}

private:
	static double ToComplexCartesian(std::string& value, double& real, double& imag);
	static double ToComplexAngular(const std::string& value, double& real, double& imag);

	PackedCircuit GetParsedData();
};

#endif // CIRCUIT_PARSER_H