
#include "FileParser.h"

using namespace std;

const char FileParser::k_WhiteSpace[2] = { ' ', '\t' };

bool FileParser::VariableExists(const string& iniName) const
{
	for (const NameParamsPair& namePair : m_IniVariables)
	{
		if (namePair.first == iniName)
		{
			return true;
		}
	}
	
	return false;
}

bool FileParser::LoadFile(const string& filePath)
{
	if (filePath.empty())
	{
		return false;
	}

	ifstream iniFile(filePath.c_str());
	if (!iniFile.good())
	{
		return false;
	}

	string currentLine; 
	bool isFirstLine = true;
	while (getline(iniFile, currentLine))
	{
		circuit_ns::trim(currentLine);
		if (currentLine.empty())
		{
			continue;
		}

		if (m_bFirstLineIsFileName && isFirstLine)
		{
			m_IniVariables.push_back(make_pair("FileName", currentLine));
			isFirstLine = false;

			continue;
		}

		char firstChar = currentLine.at(0);
		if (firstChar == m_cFullLineCommentChar)
		{
			continue;
		}

		size_t commentPosition = currentLine.find(m_cEndLineCommentChar);
		currentLine = currentLine.substr(0, commentPosition);
		circuit_ns::trim(currentLine);
		if (currentLine.empty())
		{
			continue;
		}

		size_t n_char = currentLine.size();

		size_t signPosition = n_char;
		for (int i = 0; i < m_cMainSeparator.size(); i++)
		{
			const size_t foundSignPosition = currentLine.find(m_cMainSeparator[i]);
			if (foundSignPosition < signPosition)
			{
				signPosition = foundSignPosition;
			}
		}

		string iniName = currentLine.substr(0, signPosition);
		string iniValue = "";
		if ((signPosition + 1) < currentLine.size())
		{
			iniValue = currentLine.substr(signPosition + 1, n_char - signPosition);
		}

		circuit_ns::trim(iniName);
		if (iniName.empty())
		{
			continue;
		}

		circuit_ns::trim(iniValue);
		if (iniValue.empty() && !m_bAllowEmptyValue)
		{
			continue;
		}

		if (VariableExists(iniName))
		{
			// throw error on already existing variable
		}

		// Actually add the value to the internal map
		m_IniVariables.push_back(make_pair(iniName, iniValue));

		if (iniName == m_StopAfterVariable)
		{
			break;
		}
	}

	// once normal variables are read, some lines are read just as raw strings
	// TODO : refactor this so code is shared with upper par of function
	while (getline(iniFile, currentLine))
	{
		circuit_ns::trim(currentLine);
		if (currentLine.empty())
		{
			continue;
		}

		char firstChar = currentLine.at(0);
		if (firstChar == m_cFullLineCommentChar)
		{
			continue;
		}

		size_t commentPosition = currentLine.find(m_cEndLineCommentChar);
		currentLine = currentLine.substr(0, commentPosition);
		circuit_ns::trim(currentLine);
		if (currentLine.empty())
		{
			continue;
		}

		m_AdditionalContent.push_back(currentLine);
	}

	return true;
}

int FileParser::GetInt(const string& variable, int Default) const
{
	int OutputInt = Default;

	string valueString = GetString(variable);
	if (!valueString.empty())
	{
		try
		{
			OutputInt = stoi(valueString);
		}
		catch (exception& ) {}
	}
	return OutputInt;
}

double FileParser::GetDouble(const string& variable, double Default) const
{
	double outputDouble = Default;

	string valueString = GetString(variable);
	if (!valueString.empty())
	{
		try
		{
			outputDouble = stof(valueString);
		}
		catch (exception& ) {}
	}
	return outputDouble;
}

string FileParser::GetString(const string& variable, string Default) const
{
	string outputString = Default;

	if (!variable.empty())
	{
		for (const NameParamsPair& namePair : m_IniVariables)
		{
			if (namePair.first == variable)
			{
				outputString = namePair.second;
				break;
			}
		}

	}
	return outputString;
}

int FileParser::Get(const string& Variable, int Default) const
{
	return GetInt(Variable, Default);
}

double FileParser::Get(const string& Variable, double Default) const
{
	return GetDouble(Variable, Default);
}

string FileParser::Get(const string& Variable, const string& Default) const
{
	return GetString(Variable, Default);
}

string FileParser::GetParsedDataAsString() const
{
	string out;
	for (const NameParamsPair& variablePair : m_IniVariables)
	{
		const string& name = variablePair.first;
		const string& variable = variablePair.second;

		out.append("\n" + name + ':' + variable);
	}

	return out;
}