#ifndef FILE_PARSER_H
#define FILE_PARSER_H

#include <map>
#include <fstream>
#include <stdlib.h>

#include <vector>
#include <exception>      // std::exception

#include "support_functions.h"


class FileParser
{
private:
	FileParser(const FileParser&) = delete;
	FileParser(FileParser&&) = delete;
	FileParser& operator=(const FileParser&) = delete;
	FileParser& operator=(FileParser&&) = delete;

public:
	static const char k_WhiteSpace[2];
	FileParser() = default;


	int GetInt(const std::string& Variable, int Default = 0) const;

	double GetDouble(const std::string& Variable, double Default = 0.0) const;

	std::string GetString(const std::string & Variable, std::string Default = "") const;

	int Get(const std::string & Variable, int Default) const;

	double Get(const std::string & Variable, double Default) const;

	std::string Get(const std::string & Variable, const std::string & Default) const;

	void SetPath(const std::string & filePath) { m_FilePath = filePath; }
	std::string GetPath() const { return m_FilePath; }

	bool LoadFile(const std::string& filePath);
	std::string GetParsedDataAsString() const;

	inline void SetEndLineCommentChar(const char c)
	{
		m_cEndLineCommentChar = c;
	}

	inline void SetMainSeparator(const char c)
	{
		m_cMainSeparator.resize(1);
		m_cMainSeparator[0] = c;
	}

	inline void SetMainSeparator(const std::vector<char> c)
	{
		m_cMainSeparator = c;
	}

	inline void SetFullLineCommentChar(const char c)
	{
		m_cFullLineCommentChar = c;
	}

	template<size_t N>
	inline void SetMainSeparator(const char (&c)[N])
	{
		const std::vector<char> v{ std::begin(c) , std::end(c) };
		SetMainSeparator(v);
	}

	inline void SetAllowEmptyValue(const bool value)
	{
		m_bAllowEmptyValue = value;
	}

	inline void SetFirstLineIsFileName(const bool value)
	{
		m_bFirstLineIsFileName = value;
	}

	inline void SetStopAfterVariable(const std::string value)
	{
		m_StopAfterVariable = value;
	}

protected:
	using NameParamsPair = std::pair<std::string, std::string>;
	std::vector<NameParamsPair> m_IniVariables;
	std::vector<std::string> m_AdditionalContent;

private:
	bool VariableExists(const std::string& iniName) const;

private:
	std::string m_FilePath;

	bool m_bFirstLineIsFileName = false;
	bool m_bAllowEmptyValue = false;
	char m_cFullLineCommentChar = '#';
	char m_cEndLineCommentChar = ';';
	std::string m_StopAfterVariable = "END";
	std::vector<char> m_cMainSeparator{ '=' };
};

#endif // FILE_PARSER_H