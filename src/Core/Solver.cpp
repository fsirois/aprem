
#include "Solver.h"

Solver::Solver(const std::string & solver_type)
{
	type = solver_type;
}

Eigen::VectorXd Solver::solve(SparseMatrixXd& matrix, const Eigen::VectorXd& vector)
{
	Eigen::VectorXd solution_vector;
	bool solveOK = false;
	
	if (!type.compare("BiCGSTAB"))
	{
		BiCGSTABSolver solver;
		solver.preconditioner().setDroptol(0.00000001);
		solver.compute(matrix);
		
		if (solver.info() == Eigen::Success)
			solution_vector = solver.solve(vector); 
	}
	else if (!type.compare("QR"))
	{
		SparseQRSolver solver;
		matrix.makeCompressed();
		solver.compute(matrix);
		
		if (solver.info() == Eigen::Success)
			solution_vector = solver.solve(vector); 
	}
	else if (!type.compare("LU"))
	{
		SparseLUSolver solver;
		matrix.makeCompressed();
		solver.compute(matrix);
		
		if (solver.info() == Eigen::Success)
			solution_vector = solver.solve(vector); 
	}

	return solution_vector;
}
