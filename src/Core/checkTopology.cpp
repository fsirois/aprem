
#include "circuitClass.h"

using namespace std;

CircuitErrorCode circuit::checkTopology()
{
	CircuitErrorCode fctOut;

	if (!enumerated)
	{
		string errorMessage = "Circuit has not been enumerated.";
		errorMessage += "Please call cCircuit.enumerate() before calling cCircuit.checkTopology().";

		fctOut.SetErrorMessage(errorMessage);
	}

	if (node.size() < 1)
	{
		fctOut.SetErrorMessage("Circuit topology could not be checked since it contains less than 2 nodes.");
	}

	return fctOut;
}
