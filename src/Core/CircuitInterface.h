
#pragma once

#include <iostream>
#include <vector>
#include <map>
#include <utility>

#if COMPILE_C_VERSION
	#include "FunctionHandler.h"
#else
	typedef bool (*controlFunction)(const std::vector<std::pair<double,double> > & data_in,
					const std::vector<double> & control_data,
					const std::vector<std::pair<double, double> > & data_out,
					std::vector<std::pair<double, double> > & output);
#endif

struct DrawInfo;
class circuit;

class CircuitInterface
{
public:
	virtual ~CircuitInterface() {}

	virtual CircuitErrorCode solve() = 0;

	virtual DrawInfo Load(const std::string& fileName, CircuitErrorCode& errorCode) = 0;
	virtual CircuitErrorCode Validate() const = 0;

	//-----------------------------------------------------
	// Add methods
	//-----------------------------------------------------
	virtual std::string addComponent(const std::string& name,
									 const std::vector<std::string>& node,
									 const std::vector<double>& values,
									 const std::string& type_str, 
									 CircuitErrorCode& errorCode) = 0;

    virtual bool addSubcircuit(CircuitInterface * Circuit,
							   std::string name,
							   std::vector<std::string> IONodes) = 0;
	
	//-----------------------------------------------------
	// Set methods
	//-----------------------------------------------------	
	virtual bool setComponents(const std::vector<std::string> & comp,
							   const std::vector<std::string> & param,
							   std::vector<std::pair<double,double> > & values) = 0;

	virtual bool setNode(std::string node_name, std::string field_to_set, int value) = 0;

	virtual bool setIONodes(std::vector<std::string> new_input_output_names) = 0;

	virtual bool setControlOn(std::string control_name, bool on) = 0;

	virtual bool setControlFunctionData(std::string control_name,
										std::vector<double> function_data) = 0;

	virtual bool setControl(std::string field_to_set,
							std::string control_name,
							std::vector<std::string> ComponentNames,
							std::vector<std::string> Attributes) = 0;

	virtual bool set(std::string comp_name, std::string field,
					 double in_r, double in_i = 0.0) = 0;

	//-----------------------------------------------------
	// Get methods
	//-----------------------------------------------------
	virtual std::vector<std::pair<double,double> > getComponents(const std::vector<std::string> & comp,
																 const std::vector<std::string> & param) const = 0;

	virtual void getComponents(const std::vector<std::string> & comp,
							   const std::vector<std::string> & param,
							   std::vector<std::pair<double,double> > & out) const = 0;

	virtual std::vector<double> getNode(std::string node_name, std::string field) const = 0;

	virtual std::pair<double,double> getNodeV(std::string field) const = 0;

	virtual std::vector<std::pair<double,double> > get(const std::string & comp_name,
													   const std::string & field) const = 0;

	virtual std::vector<std::pair<double,double> > getV(const std::string & name) const = 0;

	//-----------------------------------------------------
	// Other
	//-----------------------------------------------------
	virtual bool deleteElements(std::vector<std::string> elements_to_delete) = 0;
	virtual std::vector<std::string> getNames(std::string Type) 	   const = 0;
	virtual void print(std::string option) 							   const = 0;

	// control
	virtual std::string addControl(std::vector<std::string> inputComponents,
								   std::vector<std::string> inputAttributes,
								   std::vector<std::string> outputComponents,
								   std::vector<std::string> outputAttributes,
								   std::vector<double> functionData,
								   std::string functionName,
								   std::string controlName,
								   controlFunction functionPtr = nullptr) = 0;

	using VectorOfComplex = std::vector<std::pair<double, double>>;
	virtual std::pair<bool, VectorOfComplex> callControlFunction(std::string functionName,
																 std::vector<double> functionData,
																 VectorOfComplex data_in,
																 VectorOfComplex data_out) = 0;

#if COMPILE_C_VERSION
	virtual bool setControlFunction(std::string control_name,
					controlFunction function_data) = 0;
#endif
};

