
#pragma once

class CircuitErrorCode
{
public:
	CircuitErrorCode(const std::string& errorMessage) :
		m_errorMessage{errorMessage}
	{
	}

	CircuitErrorCode() = default;
	CircuitErrorCode(const CircuitErrorCode& other) = default;
	CircuitErrorCode(CircuitErrorCode&& other) :
		m_errorMessage(std::move(other.m_errorMessage))
	{
	}

	CircuitErrorCode& operator=(const CircuitErrorCode& other) = default;
	CircuitErrorCode& operator=(CircuitErrorCode&& other)
	{
		m_errorMessage = std::move(other.m_errorMessage);
		return *this;
	}

	bool IsValid() const
	{
		return m_errorMessage.size() == 0;
	}

	const std::string& GetErrorMessage()
	{
		return m_errorMessage;
	}

	void SetErrorMessage(const std::string message)
	{
		m_errorMessage = message;
	}

private:
	std::string m_errorMessage;
};