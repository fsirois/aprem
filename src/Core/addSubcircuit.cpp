//-------------------------------------------------------------------------
// addSubcircuit implementation
//-------------------------------------------------------------------------
#include "circuitClass.h"

#include "Node.h"

using namespace std;

// TODO : Separate addSubcircuit into a few procedures
// namespace add_subcircuit_ns
// {
//     map<string,string> copy_node(circuit * Circuit);
//     void copy_Component();
//     void copy_control();
//     void copy_subcircuit();
// }

bool circuit::addSubcircuit(CircuitInterface * Circuit, string name, vector<string> IONodes)
{
	return addSubcircuit(static_cast<circuit*> (Circuit), name, IONodes);
}

bool circuit::addSubcircuit(circuit* Circuit, string name, vector<string> IONodes)
{
	// Add the subcircuit
	topologyChanged = 1;
	enumerated = 0;
	
	// Verifying the validity of the name of the subcircuit
	name = NameHandler.ValidateName(name, ElementType::Subcircuit);
	
	// Step 1 : Copy nodes ------------------------------------------------
	map<string,string> nodeNameReplace;
	vector<Node*> newNodeList;
	for (int i=0; i<Circuit->node.size(); ++i)
	{
		string oldNodeName = Circuit->node[i]->getName();
		string newNodeName = oldNodeName;

		// Changer pour flag sur le noeud lui-même
		bool isIO = false;
		int jmem;
		for (int j=0; j<IONodes.size(); ++j)
			if (Circuit->input_output_nodes[j]->getName()==newNodeName)
			{
				isIO = true;
				jmem = j;
			}

		Node * newNode = NULL;
		if (!isIO)
		{
			if (NameHandler.nodeExist(newNodeName))
				newNodeName = NameHandler.correctNodeName(newNodeName);
			newNode = new Node(Circuit->node[i], newNodeName);
			node.push_back(newNode);
			newNode->setNumber(static_cast<int> (node.size()));
		}
		else
		{
			if(!NameHandler.nodeExist(IONodes[jmem]))
			{
				newNodeName = IONodes[jmem];
				if (newNodeName == "0" || newNodeName == "GND")
				{
					newNodeList.push_back(Ground);
					nodeNameReplace[oldNodeName] = "0";
					continue;
				}

				if (NameHandler.nodeExist(newNodeName))
					newNodeName = NameHandler.correctNodeName(newNodeName);
				newNode = new Node(Circuit->node[i], newNodeName);
				node.push_back(newNode);
				newNode->setNumber(static_cast<int> (node.size()));
			}
			else
			{
				newNode = NameHandler.getNode(IONodes[jmem]);
			}
		}

		if (newNode == NULL)
			return false;
		
		newNodeList.push_back(newNode);
		nodeNameReplace[oldNodeName] = newNode->getName();
	}

	// Step 2 : Create the new subcircuit ---------------------------------
	Subcircuit * NewSubcircuit = new Subcircuit(name);
	subcircuit.push_back(NewSubcircuit);

	vector<Component*> newCreatedComp;
	map<Component*, Component*> new_old_comp_map;

	// Step 3 : Copy the components ---------------------------------------
	// On ajoute chaque composant du nouveau sous-circuit au circuit maître
	ComponentManager& compManager = Circuit->m_componentManager;
	ComponentManager& manager = Circuit->m_componentManager;
	compManager.ForEachComponent([&](auto curComp)
	{
		if (curComp == nullptr)
		{
			return;
		}

		// On évite d'avoir un nouveau nom de composant en conflit avec
		// ceux déjà présent dans le circuit maître.
		ComponentType curComponentType = curComp->getType();
		string tempName = curComp->getName();
		tempName = name + "_" + tempName;
		tempName = NameHandler.ValidateName(tempName, ElementType::Component, curComponentType);

		vector<string> oldNodeNames = curComp->getNodeNames();
		vector<string> curNodeNames;
		for (int j = 0; j < oldNodeNames.size(); ++j)
		{
			curNodeNames.push_back(nodeNameReplace[oldNodeNames[j]]);
		}

		// Ici, on change le nom des noeuds
		string newCompName;
		CircuitErrorCode errorCode;
		vector<double> values;

		addComponent<std::remove_pointer_t<decltype(curComp)>>(curComp, errorCode);

		if (!errorCode.IsValid())
		{
			return;
		}

		Component* newComp = NameHandler.getComponent(newCompName);
		if (newComp == NULL)
			return;
		new_old_comp_map[curComp] = newComp;

		// Arrange node
		vector<Node*> newCompNodes = curComp->getNodes();
		vector<Node*> newCompNewNodes;
		for (int i = 0; i < newCompNodes.size(); ++i)
		{
			int pos = Circuit->NameHandler.getNodePosition(newCompNodes[i]->getName());
			Node * curNode = newNodeList[pos];
			newCompNewNodes.push_back(curNode);
		}

		newComp->setNodes(newCompNewNodes);

		newCreatedComp.push_back(newComp);

		if (!curComp->inSubcircuit())
		{
			// TODO : propagate error
		}

	});  /* Components */

	// Step 4 : Add controls ----------------------------------------------
	vector<Control*> newCreatedControl;
	for (int i=0; i<Circuit->control.size(); ++i)
	{
		Control * oldCtrl = Circuit->control[i];
		if (oldCtrl == NULL)
			continue;

		// On évite d'avoir un nouveau nom de composant en conflit avec
		// ceux déjà présents dans le circuit maître.
		string newCtrlName = name + "_" + oldCtrl->getName();
		printf("\ngetting name");
		newCtrlName = addControl(Circuit->control[i], newCtrlName);
		printf("\nNewControl %s", newCtrlName.c_str());

		// On mets à jour le nom des composants du contrôle.
		Control* curCtrl = NameHandler.getControl(newCtrlName);
		if (curCtrl == NULL)
			return false;

		vector<Component*> oldinComp = oldCtrl->getInComponent();
		vector<Component*> newinComp;
		for (int j=0; j<oldinComp.size(); ++j)
		{
			Component* comp_repl = new_old_comp_map[oldinComp[j]];
			newinComp.push_back(comp_repl);
		}
		curCtrl->setInputComponents(newinComp);

		vector<Component*> oldoutComp = oldCtrl->getOutComponent();
		vector<Component*> newoutComp;
		for (int j=0; j<oldoutComp.size(); ++j)
		{
			Component* comp_repl = new_old_comp_map[oldoutComp[j]];
			newoutComp.push_back(comp_repl);
		}
		curCtrl->setOutputComponents(newoutComp);

		newCreatedControl.push_back(curCtrl);

		curCtrl->updateSlaves();

		if (!oldCtrl->inSubcircuit())
		{
			// TODO : propagate error
		}
	}

	// Step 5 : Add IONodes -----------------------------------------------
	// On définit les noeuds d'interface (I/O) du sous-circuit qui
	// serviront aux accès pour les courants, puissances, etc.
	vector<Node*> IONodes_obj;
	for (int i=0; i<IONodes.size(); ++i)
	{
		IONodes_obj.push_back(NameHandler.getNode(IONodes[i]));
	}
	NewSubcircuit->setIONodes(IONodes_obj);

	// Step 6 : Restore comp and controls to subcircuits ------------------
	vector<Subcircuit*> newSubcircuitList;
	for (int i=0; i<Circuit->subcircuit.size(); i++)
	{
		Subcircuit * curSubcircuit = Circuit->subcircuit[i];
		if (curSubcircuit == NULL)
			continue;

		string newName = curSubcircuit->getName();
		newName = NameHandler.ValidateName(newName, ElementType::Subcircuit);
		Subcircuit * newSubcircuit = new Subcircuit(Circuit->subcircuit[i], newName);

		if (!curSubcircuit->inSubcircuit())
			NewSubcircuit->addSubcircuit(newSubcircuit);  

		// Setup components
		vector<Component*> oldComp = curSubcircuit->getComponents();
		vector<Component*> curComp = newSubcircuit->getComponents();
		vector<Component*> newComp;
		for (int j=0; j<oldComp.size(); ++j)
		{
			int pos = Circuit->NameHandler.getComponentPosition(curComp[j]->getName());
			Component* component = newCreatedComp[pos];
			newComp.push_back(component);
		}
		newSubcircuit->setComponents(newComp);

		// Controls
		vector<Control*> oldCtrl = newSubcircuit->getControls();
		vector<Control*> newCtrl;
		for (int j=0; j<oldCtrl.size(); ++j)
		{
			int pos = Circuit->NameHandler.getControlPosition(oldCtrl[j]->getName());
			Control * ctrl = newCreatedControl[pos];
			newCtrl.push_back(ctrl);
		}
		newSubcircuit->setControls(newCtrl);

		newSubcircuitList.push_back(newSubcircuit);
		subcircuit.push_back(newSubcircuit);
	}

	// Step 7 : Restore subcircuit's subcircuits --------------------------
	for (int i=0; i<newSubcircuitList.size(); i++)
	{
		vector<Subcircuit*> subSub = newSubcircuitList[i]->getSubcircuit();
		vector<Subcircuit*> newsubSub;
		for (int j=0; j<subSub.size(); ++j)
		{
			Subcircuit * subSubToAdd = subSub[j];
			for (int k=0; k<Circuit->subcircuit.size(); k++)
			{
				if (subSub[j]->getName() == Circuit->subcircuit[k]->getName())
				{
					subSubToAdd = newSubcircuitList[k];
				}
			}
			newsubSub.push_back(subSubToAdd);
		}
		newSubcircuitList[i]->setSubcircuits(newsubSub);
	}

	// Step 8 : Update new subcircuit's slaves ----------------------------
	NewSubcircuit->updateSlaves();
	for (int i=0; i<newSubcircuitList.size(); i++)
	{
		newSubcircuitList[i]->updateSlaves();
	}
	
	return true;
}

