
#pragma once

#include <string>
#include <vector>

struct DrawInfoItem
{
	std::string Name;
	std::string Info;
	std::vector<std::string> NodeNames;
};

struct DrawInfo
{
	void Add(DrawInfoItem&& item)
	{
		ComponentDrawInfoArray.push_back(std::move(item));
	}

	std::vector<DrawInfoItem> ComponentDrawInfoArray;
	std::vector<std::string> DrawStrings;
	std::string DrawParam;
};
