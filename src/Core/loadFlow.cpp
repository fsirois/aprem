/*! \file *********************************************************************
 *
 * \brief  Calcule l'écoulement de puissance dans le circuit.
 *
 *      Il est essentiel d'utiliser la fonction "enumerate()" avant d'utiliser
 *  "loadFlow()".
 *
 * \author
 *      Frédérick Cyr
 *      École Polytechnique de Montréal
 *
 *      Crédits à Philippe Ossoucah Jr. pour l'origine du projet APREM.
 *
 *****************************************************************************/

#include "loadFlow.h"

#include <math.h>
#include <cmath> 
#include <time.h>
#include <limits>

#include "Node.h"

using namespace std;

void circuit::loadFlow(CircuitErrorCode& errorCode)
{
	if (!enumerated)
	{
		errorCode.SetErrorMessage("Circuit has not been enumerated. Please call circuit.enumerate() before calling circuit.loadFlow().");
	}

	// Initialisation des variables.
	ComponentManager& mgr = m_componentManager;
	const int n_ini = 2u * (cnodes + CountOn<Component>(mgr) - CountOn<Impedance, CurrentSource, PQLoad>(mgr));
	const int n2 = 2u * (cnodes + CountOn<Component>(mgr) - CountOn<Impedance, CurrentSource>(mgr));

	SparseMatrixXd Yini(n_ini,n_ini);
	Yini.reserve(4 * n_ini);

	Eigen::VectorXd B = Eigen::VectorXd::Zero(n2);
	Eigen::VectorXd Xn = Eigen::VectorXd::Zero(n2);
	Eigen::VectorXd bv = Eigen::VectorXd::Zero(n2);
	SparseMatrixXd mainMatrix(n2,n2);
	mainMatrix.reserve(4 * n2);
	SparseMatrixXd jacobian(n2, n2);
	jacobian.reserve(4 * n2);

	// On crée la matrice de départ avec les éléments qui ne varieront pas à chaque itération de la méthode de Newton-Raphson.
	buildMatrix(B, Yini, mainMatrix, jacobian);
	
	// On calcule le point de départ pour la méthode Newton-Raphson.
	Xn = computeXini(B, Yini);

	// On utilise la méthode Newton-Raphson afin de résoudre le système d'équations non-linéaires que représente le circuit.
	const bool bHasError = newton(B, mainMatrix, Xn, jacobian);
	if (bHasError)
	{
		const string msg = "Invalid topology! Please check the circuit for parallel voltage sources, series current sources, current sources in an open loop or subcircuits without ground node.";
		errorCode.SetErrorMessage(msg);
	}

	// Retour du vecteur de tensions/courants calculé.
	collectData(Xn);
}

/*! \brief buildMatrix : Prépare la "souche" constante de la matrice du système
*                        d'équation et le vecteur B.
*
*	\param  c      : une structure de pointeurs vers un objet cCircuit.
*	        matrix : la matrice constante.
*           jacob  : la matrice jacobienne.
*           B      : le vecteur B constant.
*   \return void   : la fonction ne retourne rien mais modifie matrix, jacob 
*                    et B.
*/
void circuit::buildMatrix(Eigen::VectorXd & B,
						  Eigen::SparseMatrix<double> & Yini,
						  Eigen::SparseMatrix<double> & mainMatrix,
						  Eigen::SparseMatrix<double> & jacobian)
{
	ComponentManager& mgr = m_componentManager;
	const auto& impedances = GetContainer<Impedance*>(mgr);

	// Ajout de l'effet des impédances sur la matrice.
	for (int i = 0; i < impedances.size(); ++i)
	{
		const Impedance& imp = *impedances[i];
		if (imp.isOn())
		{
			// Admittance. 
			const double tempDouble = (imp.getZr() * imp.getZr() + imp.getZi() * imp.getZi());
			const double g = imp.getZr() / tempDouble;
			const double b = -imp.getZi() / tempDouble;

			// Indices pour accéder à la matrice.
			const vector<Node*>& comp_node = imp.getNode();
			const int iAr = 2 * comp_node[0]->getNumber() - 2;
			const int iAi = iAr + 1;
			const int iBr = 2 * comp_node[1]->getNumber() - 2;
			const int iBi = iBr + 1;

			// Modification de la matrice avec vérification d'un noeud à la masse.
			if (iAr >= 0)
			{
				mainMatrix.coeffRef(iAr, iAr) += g;
				mainMatrix.coeffRef(iAr, iAi) -= b;
				mainMatrix.coeffRef(iAi, iAr) += b;
				mainMatrix.coeffRef(iAi, iAi) += g;

				if (iBr >= 0)
				{
					mainMatrix.coeffRef(iAr, iBr) -= g;
					mainMatrix.coeffRef(iAr, iBi) += b;
					mainMatrix.coeffRef(iAi, iBr) -= b;
					mainMatrix.coeffRef(iAi, iBi) -= g;

					mainMatrix.coeffRef(iBr, iAr) -= g;
					mainMatrix.coeffRef(iBr, iAi) += b;
					mainMatrix.coeffRef(iBi, iAr) -= b;
					mainMatrix.coeffRef(iBi, iAi) -= g;
				}
			}
			if (iBr >= 0)
			{
				mainMatrix.coeffRef(iBr, iBr) += g;
				mainMatrix.coeffRef(iBr, iBi) -= b;
				mainMatrix.coeffRef(iBi, iBr) += b;
				mainMatrix.coeffRef(iBi, iBi) += g;
			}
		}
	} /* Impédances. */

	// Section ajoutée par F. Sirois pour corriger le bug des branches PQ isolées (donc temporairement retirées du circuit)
	// Idée: j'ajoute systématiquement les impédances équivalentes sur ces bus, et on utilise une équation de courant nul dedans
	// Si à la place la branche est désactivée, dans ce cas, on ne met pas les impédances, et on pose le courant initial à zéro
	// Ceci est fait à la procédure d'initialisation

	// Ajout de l'effet des charges PQ sur la matrice.
	const auto& PQLoads = GetContainer<PQLoad*>(mgr);
	for (int i = 0; i < PQLoads.size(); ++i)
	{
		// On ne considére pas la charge PQ dans le calcul du point de départ si celle-ci est désactivée volontairement.
		const PQLoad& PQLoad = *PQLoads[i];
		if (PQLoad.getTempDisabled() && PQLoad.isOn())
		{
			// Admittance.
			const double V2 = PQLoad.getVr() * PQLoad.getVr() + PQLoad.getVi() * PQLoad.getVi();
			const double g = PQLoad.getP() / V2;
			const double b = -PQLoad.getQ() / V2;

			// Indices pour accéder à la matrice.
			const vector<Node*>& comp_node = PQLoad.getNode();
			const int iAr = 2 * comp_node[0]->getNumber() - 2;
			const int iAi = iAr + 1;
			const int iBr = 2 * comp_node[1]->getNumber() - 2;
			const int iBi = iBr + 1;

			// Modification de la matrice avec vérification d'un noeud à la masse.
			if (iAr >= 0)
			{
				mainMatrix.coeffRef(iAr, iAr) += g;
				mainMatrix.coeffRef(iAr, iAi) -= b;
				mainMatrix.coeffRef(iAi, iAr) += b;
				mainMatrix.coeffRef(iAi, iAi) += g;

				if (iBr >= 0)
				{
					mainMatrix.coeffRef(iAr, iBr) -= g;
					mainMatrix.coeffRef(iAr, iBi) += b;
					mainMatrix.coeffRef(iAi, iBr) -= b;
					mainMatrix.coeffRef(iAi, iBi) -= g;

					mainMatrix.coeffRef(iBr, iAr) -= g;
					mainMatrix.coeffRef(iBr, iAi) += b;
					mainMatrix.coeffRef(iBi, iAr) -= b;
					mainMatrix.coeffRef(iBi, iAi) -= g;
				}
			}

			if (iBr >= 0)
			{
				mainMatrix.coeffRef(iBr, iBr) += g;
				mainMatrix.coeffRef(iBr, iBi) -= b;
				mainMatrix.coeffRef(iBi, iBr) += b;
				mainMatrix.coeffRef(iBi, iBi) +=  g;
			}
		}
	} /* Charges PQ. */

	// Ajout de l'effet des sources de courant sur le vecteur B.
	const auto& currentSources = GetContainer<CurrentSource*>(mgr);
	for (int i = 0; i < currentSources.size(); ++i)
	{
		const CurrentSource& currentSource = *currentSources[i];
		if (currentSource.isOn())
		{
			// Indices pour accéder à la matrice.
			const vector<Node*>& comp_node = currentSource.getNode();
			const int iAr = 2 * comp_node[0]->getNumber() - 2;
			const int iAi = iAr + 1;
			const int iBr = 2 * comp_node[1]->getNumber() - 2;
			const int iBi = iBr + 1;

			// Modification de la matrice avec vérification d'un noeud à la masse.
			if (iAr >= 0)
			{
				B[iAr] += currentSource.getIr();
				B[iAi] += currentSource.getIi();
			}
			if (iBr >= 0)
			{
				B[iBr] -= currentSource.getIr();
				B[iBi] -= currentSource.getIi();
			}
		}
	} /* Sources de courant. */

	// Tracks indices of real values in the matrix
	int ir = 2 * cnodes;

	// Ajout de l'effet des transformateurs sur la matrice.
	const auto& transformers = GetContainer<Transformer*>(mgr);
	for (int i = 0; i < transformers.size(); ++i, ir+=2)
	{
		const int ii = ir + 1;

		const Transformer& transformer = *transformers[i];
		if (transformer.isOn())
		{
			const double rr = transformer.getRatior();
			const double ri = transformer.getRatioi();

			// Indices pour accéder à la matrice.
			vector<Node*> comp_node = transformer.getNode();
			const int iAr = 2 * comp_node[0]->getNumber() - 2;
			const int iAi = iAr + 1;
			const int iBr = 2 * comp_node[1]->getNumber() - 2;
			const int iBi = iBr + 1;
			const int iCr = 2 * comp_node[2]->getNumber() - 2;
			const int iCi = iCr + 1;
			const int iDr = 2 * comp_node[3]->getNumber() - 2;
			const int iDi = iDr + 1;

			// Modification de la matrice avec vérification d'un noeud à la masse.
			if (iAr >= 0)
			{
				// Sous-matrice D
				mainMatrix.coeffRef(ir, iAr) += 1;
				mainMatrix.coeffRef(ii, iAi) += 1;
				
				// Sous-matrice A
				mainMatrix.coeffRef(iAr, ir) += 1;
				mainMatrix.coeffRef(iAi, ii) += 1;
			}

			if (iBr >= 0)
			{
				// Sous-matrice D
				mainMatrix.coeffRef(ir, iBr) += -1;
				mainMatrix.coeffRef(ii, iBi) += -1;
				
				// Sous-matrice A
				mainMatrix.coeffRef(iBr, ir) += -1;
				mainMatrix.coeffRef(iBi, ii) += -1;
				
			}

			if (iCr >= 0)
			{
				// Sous-matrice D
				mainMatrix.coeffRef(ir, iCr) += -rr;
				mainMatrix.coeffRef(ir, iCi) +=  ri;
				mainMatrix.coeffRef(ii, iCr) += -ri;
				mainMatrix.coeffRef(ii, iCi) += -rr;
				
				// Sous-matrice A
				mainMatrix.coeffRef(iCr, ir) += -rr;
				mainMatrix.coeffRef(iCr, ii) += -ri;
				mainMatrix.coeffRef(iCi, ir) += ri;
				mainMatrix.coeffRef(iCi, ii) += -rr;
			}

			if (iDr >= 0)
			{
				// Sous-matrice D
				mainMatrix.coeffRef(ir, iDr) +=  rr;
				mainMatrix.coeffRef(ir, iDi) += -ri;
				mainMatrix.coeffRef(ii, iDr) +=  ri;
				mainMatrix.coeffRef(ii, iDi) +=  rr;
				
				// Sous-matrice A
				mainMatrix.coeffRef(iDr, ir) += rr;
				mainMatrix.coeffRef(iDr, ii) += ri;
				mainMatrix.coeffRef(iDi, ir) += -ri;
				mainMatrix.coeffRef(iDi, ii) += rr;
			}
		}
		else
		{
			// Si le transfo est disabled, on met le courant primaire égal à zéro en mettant les bons coefficients dans la sous-matrice C
			// Sous-matrice C:
			mainMatrix.coeffRef(ir, ir) = 1;
			mainMatrix.coeffRef(ii, ii) = 1;
			
		}
	} /* Transformateurs. */

	// Ajout de l'effet des sources de tension sur la matrice.
	const auto& voltageSources = GetContainer<VoltageSource*>(mgr);
	for (int i = 0; i < voltageSources.size(); ++i, ir+=2)
	{
		const int ii = ir + 1;

		const VoltageSource& voltageSource = *voltageSources[i];
		if (voltageSource.isOn())
		{
			// Indices pour accéder à la matrice.
			const vector<Node*>& comp_node = voltageSource.getNode();
			const int iAr = 2 * comp_node[0]->getNumber() - 2;
			const int iAi = iAr + 1;
			const int iBr = 2 * comp_node[1]->getNumber() - 2;
			const int iBi = iBr + 1;  

			// Modification de la matrice avec vérification d'un noeud à la masse.
			if (iAr >= 0)
			{
				// Sous-matrice D:
				mainMatrix.coeffRef(ir, iAr) = 1;
				mainMatrix.coeffRef(ii, iAi) = 1;

				// Sous-matrice A:
				mainMatrix.coeffRef(iAr, ir) = 1;
				mainMatrix.coeffRef(iAi, ii) = 1;
				
			}

			if (iBr >= 0)
			{
				// Sous-matrice D
				mainMatrix.coeffRef(ir, iBr) = -1;
				mainMatrix.coeffRef(ii, iBi) = -1;
				
				// Sous-matrice A
				mainMatrix.coeffRef(iBr, ir) = -1;
				mainMatrix.coeffRef(iBi, ii) = -1;
			}

			B[ir] = voltageSource.getVr();
			B[ii] = voltageSource.getVi();
		}
		else
		{
			mainMatrix.coeffRef(ir, ir) = 1;
			mainMatrix.coeffRef(ii, ii) = 1;
		}
	} /* Sources de tension. */

	// Ajout de l'effet des interrupteurs sur la matrice.
	const auto& switches = GetContainer<Switch*>(mgr);
	for (int i = 0; i < switches.size(); ++i, ir += 2)
	{
		const int ii = ir + 1;
		const Switch& curSwitch = *(switches[i]);

		// Indices pour accéder à la matrice.
		const vector<Node*>& comp_node = curSwitch.getNode();
		const int iAr = 2 * comp_node[0]->getNumber() - 2;
		const int iAi = iAr + 1;
		const int iBr = 2 * comp_node[1]->getNumber() - 2;
		const int iBi = iBr + 1;  

		// Modification de la matrice avec vérification d'un noeud à la masse.
		if (iAr >= 0)
		{
			// Sous-matrice D
			if ((curSwitch.isOn()) && (curSwitch.getState())) // Interrupteur fermé (le courant passe).
			{
				mainMatrix.coeffRef(ir, iAr) = 1;
				mainMatrix.coeffRef(ii, iAi) = 1;
			}

			// Sous-matrice A
			mainMatrix.coeffRef(iAr, ir) = 1;
			mainMatrix.coeffRef(iAi, ii) = 1;
		}

		if (iBr >= 0)
		{
			if ((curSwitch.isOn()) && (curSwitch.getState())) // Interrupteur fermé (le courant passe).
			{
				mainMatrix.coeffRef(ir, iBr) = -1;
				mainMatrix.coeffRef(ii, iBi) = -1;
			}

			// Sous-matrice A
			mainMatrix.coeffRef(iBr, ir) = -1;
			mainMatrix.coeffRef(iBi, ii) = -1;
		}

		// Sous-matrice C
		if ((!curSwitch.isOn()) || (curSwitch.getState())) // Interrupteur ouvert (le courant ne passe pas).
		{
			mainMatrix.coeffRef(ir, ir) = 1;
			mainMatrix.coeffRef(ii, ii) = 1;
		}
	} /* Interrupteurs. */
	
	jacobian = mainMatrix;

	Yini = mainMatrix.block(0,0,Yini.rows(),Yini.rows());
}


/*! \brief computeXini : Calcule le point de départ pour l'algorithme de calcul.
*
*	\param c       : une structure de pointeurs vers un objet cCircuit.
*	       y_ini   : la copie de la matrice constante.
*          mxY_ini : la version mxArray de la copie de la matrice constante.
*          B       : le vecteur B constant.
*          x_ini   : le vecteur X dans lequel stocker le point de départ.
*          mxX_ini : la version mxArray du vecteur X.
*    \return int   : la fonction ne retourne rien mais modifie le contenu de 
*                    x_ini et mxX_ini.
*/
Eigen::VectorXd circuit::computeXini(const Eigen::VectorXd & B, SparseMatrixXd & Yini)
{
	// Initialisation des variables
	ComponentManager& mgr = m_componentManager;
	const int n_ini = 2u * (cnodes + CountOn<Component>(mgr) - CountOn<Impedance, CurrentSource, PQLoad>(mgr));
	const int n2 = 2u * (cnodes + CountOn<Component>(mgr) - CountOn<Impedance, CurrentSource>(mgr));

	int ir = n_ini - 2u * CountOn<PVSource>(mgr);

	Eigen::VectorXd Bini = Eigen::VectorXd::Zero(n_ini);
	for (int i = 0; i < n_ini; ++i)
	{
		Bini(i) = B(i);
	}

	// Ajout de l'effet des charges PQ sur la matrice.
	const auto& PQLoads = GetContainer<PQLoad*>(mgr);
	for (int i = 0; i < PQLoads.size(); ++i)
	{
		const PQLoad& load = *(PQLoads[i]);
		// On ne considère pas la charge PQ dans le calcul du point de départ si celle-ci est temporairement désactivée.
		if ((load.getTempDisabled() == 0) && (load.isOn()))
		{
			// Admittance.
			const double V2 = load.getVr() * load.getVr() + load.getVi() * load.getVi();
			const double g = load.getP() / V2;
			const double b = -load.getQ() / V2;

			// Indices pour accéder à la matrice.
			const vector<Node*>& comp_node = load.getNode();
			const int iAr = 2 * comp_node[0]->getNumber() - 2;
			const int iAi = iAr + 1;
			const int iBr = 2 * comp_node[1]->getNumber() - 2;
			const int iBi = iBr + 1;

			// Modification de la matrice avec vérification d'un noeud à la masse.
			if (iAr >= 0)
			{
				Yini.coeffRef(iAr, iAr) += g;
				Yini.coeffRef(iAr, iAi) -= b;
				Yini.coeffRef(iAi, iAr) += b;
				Yini.coeffRef(iAi, iAi) +=  g;

				if (iBr >= 0)
				{
					Yini.coeffRef(iAr, iBr) -= g;
					Yini.coeffRef(iAr, iBi) += b;
					Yini.coeffRef(iAi, iBr) -= b;
					Yini.coeffRef(iAi, iBi) -=  g;
					
					Yini.coeffRef(iBr, iAr) -= g;
					Yini.coeffRef(iBr, iAi) += b;
					Yini.coeffRef(iBi, iAr) -= b;
					Yini.coeffRef(iBi, iAi) -=  g;
				}
			}
			if (iBr >= 0)
			{
				Yini.coeffRef(iBr, iBr) += g;
				Yini.coeffRef(iBr, iBi) -= b;
				Yini.coeffRef(iBi, iBr) += b;
				Yini.coeffRef(iBi, iBi) += g;
			}
		}
	} /* Charges PQ. */

	// Ajout de l'effet des sources PV sur la matrice.
	const auto& PVSources = GetContainer<PVSource*>(mgr);
	for (int i = 0; i < PVSources.size(); ++i, ir += 2)
	{
		const int ii = ir + 1;

		const PVSource& source = *PVSources[i];
		if (source.isOn())
		{
			// Indices pour accéder à la matrice.
			const vector<Node*>& comp_node = source.getNode();
			const int iAr = 2 * comp_node[0]->getNumber() - 2;
			const int iAi = iAr + 1;
			const int iBr = 2 * comp_node[1]->getNumber() - 2;
			const int iBi = iBr + 1;  

			// Modification de la matrice avec vérification d'un noeud à la masse.
			if (iAr >= 0)
			{
				// Sous-matrice D
				Yini.coeffRef(ir, iAr) = 1;
				Yini.coeffRef(ii, iAi) = 1;

				// Sous-matrice A
				Yini.coeffRef(iAr, ir) = -1;
				Yini.coeffRef(iAi, ii) = -1;
			}
			if (iBr >= 0)
			{
				// Sous-matrice D
				Yini.coeffRef(ir, iBr) = -1;
				Yini.coeffRef(ii, iBi) = -1;

				// Sous-matrice A
				Yini.coeffRef(ir, iBr) = -1;
				Yini.coeffRef(ii, iBi) = -1;
			}
			Bini(ir) = source.getVr();
			Bini(ii) = source.getVi();
		}
		else
		{
			Yini.coeffRef(ir, ir) = 1;
			Yini.coeffRef(ii, ii) = 1;
		}
	} /* Sources PV. */

	// Compute the numerical factorization 
	// Calcul de Xini.
	Eigen::VectorXd Xini_cal;
	Xini_cal = solver.solve(Yini, Bini);

	Eigen::VectorXd Xini(n2);
	for (int i = 0; i < Xini_cal.size(); ++i)
	{
		Xini(i) = Xini_cal(i);
	}

	// Calcul des courants initiaux à travers les charges PQ.
	for (int i = 0; i < PQLoads.size(); ++i, ir += 2)
	{
		const int ii = ir + 1;

		const PQLoad& load = *PQLoads[i];

		// Admittance.
		const double V2 = (load.getVr()) * (load.getVr()) + (load.getVi()) * (load.getVi());
		const double g = load.getP() / V2;
		const double b = -load.getQ() / V2;

		// Indices pour accéder à la matrice.
		const vector<Node*>& comp_node = load.getNode();
		const int iAr = 2 * comp_node[0]->getNumber() - 2;
		const int iAi = iAr + 1;
		const int iBr = 2 * comp_node[1]->getNumber() - 2;
		const int iBi = iBr + 1;

		// Calcul du courant initial.
		double tempVa_r = 0.0;
		double tempVa_i = 0.0;
		if (iAr >= 0)
		{
			tempVa_r = Xini(iAr);
			tempVa_i = Xini(iAi);
		}

		double tempVb_r = 0.0;
		double tempVb_i = 0.0;
		if (iBr >= 0)
		{
			tempVb_r = Xini(iBr);
			tempVb_i = Xini(iBi);
		}

		Xini(ir) = (tempVa_r - tempVb_r) * g - (tempVa_i - tempVb_i) * b;
		Xini(ii) = (tempVa_r - tempVb_r) * b + (tempVa_i - tempVb_i) * g;    
	} /* Courants charges PQ. */

	return Xini;
}

/*! \brief newton : Fait une itération de la méthode Newton-Raphson sur le 
*                   système non linéaire à partir de la matrice "souche" 
*                   constante.
*	\param  matrix   : la matrice constante.
*           jacob    : la matrice jacobienne.  Cette matrice sera modifiée lors
*                      de l'exécution de cette fonction.
*           B        : le vecteur B constant.
*           x_n      : vecteur X contenant le point de départ.  Les résultats 
*                      seront stocké dans cette variable.
*   \return bool     : Retourne true si la deuxième partie de la vérification 
*                      topologique échoue et modifie x_n
*/
bool circuit::newton(Eigen::VectorXd& B,
					 Eigen::SparseMatrix<double>& mainMatrix,
					 Eigen::VectorXd& Xn,
					 Eigen::SparseMatrix<double>& jacobian)
{
	// On place les sources PV qui ont été choisies comme slack bus à
	// l'extérieur de la boucle afin d'éviter de répéter des opérations superflues.

	ComponentManager& mgr = m_componentManager;
	const unsigned int index_Ini = 2u * (cnodes + CountOn<Component>(mgr) - CountOn<Impedance, CurrentSource, PQLoad, PVSource>(mgr));
	unsigned int ir = index_Ini;

	// Ajout de l'effet des sources PV sur la matrice "constante".
	const auto& PVSources = GetContainer<PVSource*>(mgr);
	for (int i = 0; i < PVSources.size(); ++i, ir += 2)
	{
		const unsigned int ii = ir + 1;

		const PVSource& source = *PVSources[i];

		// Si la source PV sert de slack bus.
		if (source.isUsedAsVoltageSource() != 0)
		{
			if (source.isOn())
			{
				// Indices pour accéder à la matrice.
				const vector<Node*>& comp_node = source.getNode();
				const int iAr = 2 * comp_node[0]->getNumber() - 2;
				const int iAi = iAr + 1;
				const int iBr = 2 * comp_node[1]->getNumber() - 2;
				const int iBi = iBr + 1;

				// Modification de la matrice avec vérification d'un noeud à la masse.
				if (iAr >= 0)
				{
					// Sous-matrice D
					mainMatrix.coeffRef(ir, iAr) = 1;
					mainMatrix.coeffRef(ii, iAi) = 1;

					jacobian.coeffRef(ir, iAr) = 1;
					jacobian.coeffRef(ii, iAi) = 1;

					// Sous-matrice A
					mainMatrix.coeffRef(iAr, ir) = -1;
					mainMatrix.coeffRef(iAi, ii) = -1;
					
					jacobian.coeffRef(iAr, ir) = -1;
					jacobian.coeffRef(iAi, ii) = -1;
				}

				if (iBr >= 0)
				{
					// Sous-matrice D
					mainMatrix.coeffRef(ir, iBr) = -1;
					mainMatrix.coeffRef(ii, iBi) = -1;

					jacobian.coeffRef(ir, iBr) = -1;
					jacobian.coeffRef(ii, iBi) = -1;

					// Sous-matrice A
					mainMatrix.coeffRef(iBr, ir) = 1;
					mainMatrix.coeffRef(iBi, ii) = 1;

					jacobian.coeffRef(iBr, ir) = 1;
					jacobian.coeffRef(iBi, ii) = 1;
				}

				// On pose la source PV comme étant un slack bus à 0 degré.
				B[ir] = sqrt(source.getVr() * source.getVr() + source.getVi() * source.getVi());
				B[ii] = 0;
			}
			else
			{
				mainMatrix.coeffRef(ir, ir) = 1;
				mainMatrix.coeffRef(ii, ii) = 1;
				
				jacobian.coeffRef(ir, ir) = 1;
				jacobian.coeffRef(ii, ii) = 1;
			}
		}
	} /* Sources PV. */

	// Boucle principale des itérations de Newton-Raphson.
	bool bHasConverged = false;
	for(int nIter = 0; (nIter < N_ITER_MAX) && !bHasConverged; ++nIter)
	{
		ir = index_Ini;
		const unsigned int ii = ir + 1;

		// Ajout de l'effet des sources PV sur la matrice.
		const auto& PVSources = GetContainer<PVSource*>(mgr);
		for (int i = 0; i < PVSources.size(); ++i, ir += 2)
		{
			const unsigned int ii = ir + 1;
			const PVSource& source = *PVSources[i];

			// Si la source PV ne sert pas de slack bus.
			if (source.isUsedAsVoltageSource() == 0)
			{
				if (source.isOn())
				{
					// Indices pour accéder à la matrice.
					const vector<Node*>& comp_node = source.getNode();
					const int iAr = 2 * comp_node[0]->getNumber() - 2;
					const int iAi = iAr + 1;
					const int iBr = 2 * comp_node[1]->getNumber() - 2;
					const int iBi = iBr + 1;  

					// Calcul des caractéristiques de la source de tension.
					double tempVa_r = 0.0;
					double tempVa_i = 0.0;
					if (iAr >= 0)
					{
						tempVa_r = Xn(iAr);
						tempVa_i = Xn(iAi);
					}

					double tempVb_r = 0.0;
					double tempVb_i = 0.0;
					if (iBr >= 0)
					{
						tempVb_r = Xn(iBr);
						tempVb_i = Xn(iBi);
					}

					const double u = tempVa_r - tempVb_r;
					const double v = tempVa_i - tempVb_i;
					const double absV = sqrt((u * u) + (v * v));
					const double u_ = u / absV;
					const double v_ = v / absV;

					// Modification de la matrice avec vérification d'un noeud à la masse.
					if (iAr >= 0)
					{
						// Sous-matrice D
						mainMatrix.coeffRef(ir, iAr) = Xn(ir);
						mainMatrix.coeffRef(ir, iAi) = Xn(ii);
						mainMatrix.coeffRef(ii, iAr) = u_;
						mainMatrix.coeffRef(ii, iAi) = v_;

						jacobian.coeffRef(ir, iAr) = Xn(ir);
						jacobian.coeffRef(ir, iAi) = Xn(ii);
						jacobian.coeffRef(ii, iAr) = u_;
						jacobian.coeffRef(ii, iAi) = v_;

						// Sous-matrice A
						mainMatrix.coeffRef(iAr, ir) = -1;
						mainMatrix.coeffRef(iAi, ii) = -1;

						jacobian.coeffRef(iAr, ir) = -1;
						jacobian.coeffRef(iAi, ii) = -1;
					}
					if (iBr >= 0)
					{
						// Sous-matrice D
						mainMatrix.coeffRef(ir, iBr) = -Xn(ir);
						mainMatrix.coeffRef(ir, iBi) = -Xn(ii);
						mainMatrix.coeffRef(ii, iBr) = -u_;
						mainMatrix.coeffRef(ii, iBi) = -v_;

						jacobian.coeffRef(ir, iBr) = -Xn(ir);
						jacobian.coeffRef(ir, iBi) = -Xn(ii);
						jacobian.coeffRef(ii, iBr) = -u_;
						jacobian.coeffRef(ii, iBi) = -v_;

						// Sous-matrice A
						mainMatrix.coeffRef(iBr, ir) = 1;
						mainMatrix.coeffRef(iBi, ii) = 1;

						jacobian.coeffRef(iBr, ir) = 1;
						jacobian.coeffRef(iBi, ii) = 1;
					}

					// Vecteur B.
					B[ir] = source.getP();
					B[ii] = source.getVabs();

					// Sous-matrice C du jacobien.
					jacobian.coeffRef(ir, ir) = u;
					jacobian.coeffRef(ir, ii) = v;
				}
				else
				{
					mainMatrix.coeffRef(ir, ir) = 1;
					mainMatrix.coeffRef(ii, ii) = 1;

					jacobian.coeffRef(ir, ir) = 1;
					jacobian.coeffRef(ii, ii) = 1;
				}
			}

		} /* Sources PV. */

		// Ajout de l'effet des charges PQ sur la matrice.
		const auto& PQLoads = GetContainer<PQLoad*>(mgr);
		for (int i = 0; i < PQLoads.size(); ++i, ir += 2)
		{
			const unsigned int ii = ir + 1;

			const PQLoad& load = *PQLoads[i];

			// Indices pour accéder à la matrice.
			const vector<Node*>& comp_node = load.getNode();
			const int iAr = 2 * comp_node[0]->getNumber() - 2;
			const int iAi = iAr + 1;
			const int iBr = 2 * comp_node[1]->getNumber() - 2;
			const int iBi = iBr + 1;

			// Modification de la matrice avec vérification d'un noeud à la masse.
			if (iAr >= 0)
			{
				// Sous-matrice D.
				mainMatrix.coeffRef(ir, iAr) = Xn(ir);
				mainMatrix.coeffRef(ir, iAi) = Xn(ii);
				mainMatrix.coeffRef(ii, iAr) = -Xn(ii);
				mainMatrix.coeffRef(ii, iAi) =  Xn(ir);

				jacobian.coeffRef(ir, iAr) = Xn(ir);
				jacobian.coeffRef(ir, iAi) = Xn(ii);
				jacobian.coeffRef(ii, iAr) = -Xn(ii);
				jacobian.coeffRef(ii, iAi) =  Xn(ir);

				// Sous-matrice A
				mainMatrix.coeffRef(iAr, ir) = 1;
				mainMatrix.coeffRef(iAi, ii) = 1;

				jacobian.coeffRef(iAr, ir) = 1;
				jacobian.coeffRef(iAi, ii) = 1;
			}

			if (iBr >= 0)
			{
				// Sous-matrice D
				mainMatrix.coeffRef(ir, iBr) = -Xn(ir);
				mainMatrix.coeffRef(ir, iBi) = -Xn(ii);
				mainMatrix.coeffRef(ii, iBr) = Xn(ii);
				mainMatrix.coeffRef(ii, iBi) =  -Xn(ir);
				
				jacobian.coeffRef(ir, iBr) = -Xn(ir);
				jacobian.coeffRef(ir, iBi) = -Xn(ii);
				jacobian.coeffRef(ii, iBr) = Xn(ii);
				jacobian.coeffRef(ii, iBi) =  -Xn(ir);

				// Sous-matrice A
				mainMatrix.coeffRef(iBr, ir) = -1;
				mainMatrix.coeffRef(iBi, ii) = -1;

				jacobian.coeffRef(iBr, ir) = -1;
				jacobian.coeffRef(iBi, ii) = -1;
			}

			// Vecteur B
			if ((load.getTempDisabled() == 0) && (load.isOn()))
			{
				B[ir] = load.getP();
				B[ii] = load.getQ();
			}
			else
			{
				B[ir] = 0;
				B[ii] = 0;
			}

			// Sous-matrice C du jacobien
			if ((load.getTempDisabled()) || (load.isOn()==0)) // Au cas où la branche PQ soit isolée ou retirée
			{
				jacobian.coeffRef(ir, ir) = 1;
				jacobian.coeffRef(ir, ii) = 0;
				jacobian.coeffRef(ii, ir) = 0;
				jacobian.coeffRef(ii, ii) = 1;
			}
			else
			{
				double tempVa_r = 0.0;
				double tempVa_i = 0.0;
				if (iAr >=0)
				{
					tempVa_r = Xn(iAr);
					tempVa_i = Xn(iAi);
				}

				double tempVb_r = 0.0;
				double tempVb_i = 0.0;
				if (iBr >=0)
				{
					tempVb_r = Xn(iBr);
					tempVb_i = Xn(iBi);
				}

				jacobian.coeffRef(ir, ir) = tempVa_r - tempVb_r;
				jacobian.coeffRef(ir, ii) = tempVa_i - tempVb_i;
				jacobian.coeffRef(ii, ir) = tempVa_i - tempVb_i;
				jacobian.coeffRef(ii, ii) = tempVb_r - tempVa_r;  // Vb - Va volontaire.
			}
		} /* Charges PQ. */

		// Deuxième partie de la vérification topologique.
		if (checkMatrixSingularity(jacobian))
		{
			return true;
		}

		// Calcul de X_(n+1)
		const Eigen::VectorXd MxF = B - mainMatrix * Xn;
		const Eigen::VectorXd Mxdx = solver.solve(jacobian, MxF); 

		Xn += Mxdx;

		// Vérification des critères d'arrêt.
		bHasConverged = (Mxdx.norm() <= TOLERANCE * 1000 || MxF.norm() <= TOLERANCE * 1000); /* Mx1[0] = sum(abs(dx)). */ /* Mx1[1] = sum(abs(F)). */

	} /* Itérations de newton. */

	return !bHasConverged;
}


/*! \brief checkMatrixSingularity : Dernier morceau de vérification topologique:
*                                   on vérifie si le déterminant de la matrice 
*                                   partiellement augmentée est égal à zéro afin
*                                   de s'assurer que le circuit ne présente pas 
*                                   d'impossibilité topologique.
*	\param  prhs[0]  : un objet cCircuit.
*	        mxY_ini  : la matrice d'initialisation..
*   \return int      : retourne 1 en cas de matrice singulière et 0 autrement.
*/
bool circuit::checkMatrixSingularity(Eigen::SparseMatrix<double>& Y_ini)
{
	bool bHasSingularity = false;

	// On s'assure que la première partie de la vérification topologique a été
	// faite mais que la deuxième ne l'a pas encore été.
	if (topologyChanged)
	{
		// Calcul du conditionnement de la matrice.
		Eigen::VectorXd AbsDiagonal = Y_ini.diagonal();
		AbsDiagonal = AbsDiagonal.cwiseAbs();

		double Minimum = AbsDiagonal.minCoeff();
		double Maximum = AbsDiagonal.maxCoeff();
		double ConditionEstimate = Minimum/Maximum;

		// Message d'erreur si la matrice est singulière.  La matrice est singulière si cond(Matrice) = inf.
		if (isnan(ConditionEstimate) || isinf(ConditionEstimate))
		{
			bHasSingularity = true;
		}

		// Autrement, on met le "flag" topology_changed sur la position "topologie vérifiée".
		else
		{
			topologyChanged = false;
		}
	}

	return bHasSingularity;
}
