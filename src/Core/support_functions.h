
#pragma once

#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include <cctype>
#include <iterator>
#include <math.h>

#ifndef PI
#define PI 3.141592653589793238462643383279
#endif

#include "Types.h"

class ComponentManager;

namespace circuit_ns
{
//-------------------------------------------------------------------------
// Complex number operations
//-------------------------------------------------------------------------
void complex_multiply(double A, double B, double C, double D,
					  double * Out_r, double * Out_i);

void complex_division(double A, double B, double C, double D,
					  double * Out_r, double * Out_i);

// get_complex_number --> deprecated
void get_complex_number(const double * in_r, const double * in_i, double * out_r, double * out_i);


void get_complex_number(const std::vector<double>& in, double& out_r, double& out_i);
double norm(double real, double imag);

template <typename T>
const T angle(const T real, const T imag)
{
	return (atan2(imag,real));
}

template <typename T>
constexpr void from_angle(const T norm, const T angle, T& xCoordinateOut, T& yCoordinateOut)
{
	xCoordinateOut = norm * cos(angle);
	yCoordinateOut = norm * sin(angle);
}

constexpr double kDegreeToRadianMultiplier = PI / 180.0;
constexpr double kRadianToDegreeMultiplier = 180.0 / PI;

template <typename T>
constexpr T to_degree(const T value)
{
	return value * static_cast<T> (kRadianToDegreeMultiplier);
}

template <typename T>
constexpr T to_radian(const T value)
{
	return value * static_cast<T> (kDegreeToRadianMultiplier);
}

//-------------------------------------------------------------------------
// Get position of elements in different arrays
//-------------------------------------------------------------------------
int get_position(const ComponentManager& container, const std::string& name);

template<typename T, typename std::enable_if<std::is_pointer<T>::value>::type* = nullptr>
int get_position(const std::vector<T>& container, const std::string& name)
{
	for (int i = 0; i<container.size(); ++i)
	{
		if (container[i]->getName() == name)
			return i;
	}
	return -1;
}

template<typename T, typename std::enable_if<!std::is_pointer<T>::value>::type* = nullptr>
int get_position(const std::vector<T>& container, const std::string& name)
{
	for (int i = 0; i < container.size(); ++i)
	{
		if (container[i].getName() == name)
			return i;
	}
	return -1;
}

template <typename ContainerType>
bool contains(const std::vector<ContainerType>& Container,
			  const std::string& name)
{
	return (get_position(Container, name) >= 0);
}

template <typename T>
int CountOn(const T& Container)
{
	int Count = 0;
	for (int i = 0; i < Container.size(); ++i)
	{
		if (Container[i]->isOn())
		{
			Count++;
		}
	}
	return Count;
}

//-------------------------------------------------------------------------
// Miscellanious functions
//-------------------------------------------------------------------------
// Override bug in to_string allowing the use of only unsigned long long
std::string IntToString(int in);

std::string & left_trim(std::string & s);
std::string & right_trim(std::string & s);
std::string & trim(std::string & s);

template <class T>
void split(const std::string& str, std::vector<T>& cont, char delim = ' ', char delim2 = '\t')
{
	size_t previous = 0;
	size_t current = std::min(str.find(delim), str.find(delim2));
	while (current != std::string::npos)
	{
		T substr = str.substr(previous, current - previous);
		if (substr.size() > 0)
		{
			cont.push_back(substr);
		}

		previous = current + 1;
		current = std::min(str.find(delim, previous), str.find(delim2, previous));
	}

	T sub = str.substr(previous, current - previous);

	if (sub.size() > 0)
	{
		cont.push_back(sub);
	}
}


double round( double value );

template <typename T>
void remove_at(std::vector<T*> & Container, int pos)
{
	Container.erase(Container.begin()+pos, Container.begin()+pos + 1);
}

enum class PropertyType
{
	Voltage = 0,
	Current = 1,
	Invalid = 3
};

struct CircuitOutput
{
	std::string m_sComponentName;
	PropertyType m_eType;
};


PropertyType ToPropertyType(const std::string& name);

std::string ToString(const PropertyType type);

#define REPEAT(N, macro) REPEAT_##N(macro)
#define REPEAT_0(macro)
#define REPEAT_1(macro) REPEAT_0(macro) macro(0)
#define REPEAT_2(macro) REPEAT_1(macro) macro(1)
#define REPEAT_3(macro) REPEAT_2(macro) macro(2)
#define REPEAT_4(macro) REPEAT_3(macro) macro(3)
#define REPEAT_5(macro) REPEAT_4(macro) macro(4)
#define REPEAT_6(macro) REPEAT_5(macro) macro(5)
#define REPEAT_7(macro) REPEAT_6(macro) macro(6)
#define REPEAT_8(macro) REPEAT_7(macro) macro(7)
#define REPEAT_9(macro) REPEAT_8(macro) macro(8)
#define REPEAT_10(macro) REPEAT_9(macro) macro(9)
#define REPEAT_11(macro) REPEAT_10(macro) macro(10)
#define REPEAT_12(macro) REPEAT_11(macro) macro(11)
#define REPEAT_13(macro) REPEAT_12(macro) macro(12)
#define REPEAT_14(macro) REPEAT_13(macro) macro(13)
#define REPEAT_15(macro) REPEAT_14(macro) macro(14)
#define REPEAT_16(macro) REPEAT_15(macro) macro(15)
#define REPEAT_17(macro) REPEAT_16(macro) macro(16)
#define REPEAT_18(macro) REPEAT_17(macro) macro(17)
#define REPEAT_19(macro) REPEAT_18(macro) macro(18)
#define REPEAT_20(macro) REPEAT_19(macro) macro(19)
#define REPEAT_MAX 20

} /*circuit_ns*/

