
#pragma once

#include <Eigen>
#include <Sparse>

typedef Eigen::SparseMatrix<double, Eigen::ColMajor, int> SparseMatrixXd;

typedef Eigen::SparseQR<SparseMatrixXd, Eigen::COLAMDOrdering<int> > SparseQRSolver;
typedef Eigen::SparseLU<SparseMatrixXd, Eigen::COLAMDOrdering<int> > SparseLUSolver;
typedef Eigen::BiCGSTAB<Eigen::SparseMatrix<double>, Eigen::IncompleteLUT<double> > BiCGSTABSolver;

class Solver
{
public:
	Solver() = default;

	Solver(const std::string& solver_type);
	Eigen::VectorXd solve(SparseMatrixXd& matrix, const Eigen::VectorXd& vector);

private:
	std::string type;
};

