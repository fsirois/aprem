
#pragma once

#include "CircuitComponents.h"

#include "support_functions.h"

template<typename T>
class ComponentContainer
{
public:
	const T& operator[](const size_t index) const
	{
		return m_components[index];
	}

	T& operator[](const size_t index)
	{
		return m_components[index];
	}

	size_t size() const
	{
		return m_components.size();
	}

	void push_back(const T& newComponent)
	{
		m_components.push_back(newComponent);
	}

	template<typename FuncType>
	void ForEachComponent(FuncType func) const
	{
		for (const auto& component : m_components)
		{
			func(component);
		}
	}

	template<typename FuncType>
	void ForEachComponent(FuncType func)
	{
		for (auto& component : m_components)
		{
			func(component);
		}
	}

	std::vector<T>& GetComponents()
	{
		return m_components;
	}

private:
	std::vector<T> m_components;
};

template<ComponentType Index>
using ComponentTypeSelector = typename std::tuple_element<(size_t)Index, std::tuple<
	Impedance,
	CurrentSource,
	PVSource,
	PQLoad,
	VoltageSource,
	Transformer,
	Switch,
	DummyComponent,
	DummyComponent,
	DummyComponent,
	DummyComponent,
	DummyComponent,
	DummyComponent,
	DummyComponent,
	DummyComponent,
	DummyComponent,
	DummyComponent,
	DummyComponent,
	DummyComponent,
	DummyComponent>>::type;

//--------------------------------------------------
class ComponentManager
{
public:
	ComponentManager() = default;
	ComponentManager(const ComponentManager&) = default;
	ComponentManager(ComponentManager&&) = default;
	ComponentManager& operator=(const ComponentManager&) = default;
	ComponentManager& operator=(ComponentManager&&) = default;

	void RemoveComponent(Component* component)
	{
		if (pLastAdded == component)
		{
			pLastAdded = nullptr;
		}

		ForEachType([component](auto&& container)
		{
			// TODO : use std find with pointer directly
			const int pos = circuit_ns::get_position(container.GetComponents(), component->getName());
			if (pos != -1)
			{
				circuit_ns::remove_at(container.GetComponents(), pos);
			}
		});
	}

	template <typename FuncType>
	void ForEachComponent(FuncType func) const
	{
		ForEachType([func](auto&& Container)
		{
			Container.ForEachComponent(func);
		});
	}

	template <typename FuncType>
	void ForEachComponent(FuncType func)
	{
		ForEachType([func](auto&& Container)
		{
			Container.ForEachComponent(func);
		});
	}

	const Component* operator[](const size_t index) const
	{
		size_t currentIndex = 0;
		Component* out = nullptr;

		ForEachType([&currentIndex, &out, index](const auto& container)
		{
			const size_t localIndex = index - currentIndex;
			if (localIndex >= 0 && localIndex < container.size())
			{
				out = static_cast<Component*>(container[localIndex]);
			}

			currentIndex += container.size();
		});

		return out;
	}

	Component* operator[](const size_t index)
	{
		size_t currentIndex = 0;
		Component* out = nullptr;

		ForEachType([&currentIndex, &out, index](auto& container)
		{
			const size_t localIndex = index - currentIndex;
			if (localIndex >= 0 && localIndex < container.size())
			{
				out = static_cast<Component*>(container[localIndex]);
			}

			currentIndex += container.size();
		});

		return out;
	}

	size_t size() const
	{
		size_t totalSize = 0;
		ForEachType([&totalSize](const auto& container)
		{
			totalSize += container.size();
		});

		return totalSize;
	}

	template <typename FuncType>
	void ForEachType(FuncType Func)
	{
		Func(Impedances);
		Func(CurrentSources);
		Func(VoltageSources);
		Func(PVSources);
		Func(PQLoads);
		Func(Transformers);
		Func(Switches);
	}

	template <typename FuncType>
	void ForEachType(FuncType Func) const
	{
		Func(Impedances);
		Func(CurrentSources);
		Func(VoltageSources);
		Func(PVSources);
		Func(PQLoads);
		Func(Transformers);
		Func(Switches);
	}

public:
	Component* pLastAdded = nullptr;

	ComponentContainer<Impedance*> Impedances;
	ComponentContainer<CurrentSource*> CurrentSources;
	ComponentContainer<VoltageSource*> VoltageSources;
	ComponentContainer<Transformer*> Transformers;
	ComponentContainer<PQLoad*> PQLoads;
	ComponentContainer<PVSource*> PVSources;
	ComponentContainer<Switch*> Switches;

	ComponentContainer<DummyComponent*> Dummies;
};

template <typename T>
ComponentContainer<T>& GetContainer(ComponentManager& mgr)
{
	static_assert(true, "Error");
}

template <>
inline ComponentContainer<DummyComponent*>& GetContainer<DummyComponent*>(ComponentManager& mgr)
{
	return mgr.Dummies;
}

template <>
inline ComponentContainer<Impedance*>& GetContainer<Impedance*>(ComponentManager& mgr)
{
	return mgr.Impedances;
}

template <>
inline ComponentContainer<CurrentSource*>& GetContainer<CurrentSource*>(ComponentManager& mgr)
{
	return mgr.CurrentSources;
}

template <>
inline ComponentContainer<VoltageSource*>& GetContainer<VoltageSource*>(ComponentManager& mgr)
{
	return mgr.VoltageSources;
}

template<>
inline ComponentContainer<Transformer*>& GetContainer<Transformer*>(ComponentManager& mgr)
{
	return mgr.Transformers;
}

template<>
inline ComponentContainer<PQLoad*>& GetContainer<PQLoad*>(ComponentManager& mgr)
{
	return mgr.PQLoads;
}

template<>
inline ComponentContainer<PVSource*>& GetContainer<PVSource*>(ComponentManager& mgr)
{
	return mgr.PVSources;
}

template<>
inline ComponentContainer<Switch*>& GetContainer<Switch*>(ComponentManager& mgr)
{
	return mgr.Switches;
}


template<ComponentType type>
auto GetContainer(ComponentManager& mgr)
{
	using T = ComponentTypeSelector<type>;
	return GetContainer<T*>(mgr);
}

template<unsigned int type>
auto GetContainer(ComponentManager& mgr)
{
	using T = ComponentTypeSelector<static_cast<ComponentType>(type)>;
	return GetContainer<T*>(mgr);
}

template<typename T>
unsigned int CountOn(ComponentManager& mgr)
{
	return circuit_ns::CountOn(GetContainer<T*>(mgr));
}

template<>
inline unsigned int CountOn<Component>(ComponentManager& mgr)
{
	return circuit_ns::CountOn(mgr);
}

template<typename T, typename... A, typename = typename std::enable_if< sizeof...(A) != 0, void >::type>
unsigned int CountOn(ComponentManager& mgr)
{
	return CountOn<T>(mgr) + CountOn<A...>(mgr);
}

template<typename T>
void push_back(ComponentManager& mgr, const T& component)
{
	GetContainer<T>(mgr).push_back(component);
	mgr.pLastAdded = component;
}

template <typename FuncType, typename... ContainerType>
void ForEachComponentByTypeOrder(ComponentManager& mgr, FuncType func)
{
	const auto runForEachComp = [func](auto& Container)
	{
		Container.ForEachComponent(func);
	};

	const int _[] = { (runForEachComp(GetContainer<ContainerType>(mgr)), 0)... };
}
