
#pragma once

#include "ComponentManager.h"

#include "CircuitElements.h"

#include <map>

class circuitNameHandler
{
public:
	circuitNameHandler();
	circuitNameHandler(const std::vector<Node*> * node,
					   const ComponentManager* compManager,
					   const std::vector<Control*> * control,
					   const std::vector<Subcircuit*> * subcircuit,
					   const std::map<std::string, Component*> * comp_dict);

	// Verify the existence of elements in the circuit
	bool nodeExist(const std::string& node_name)             const;
	bool componentExist(const std::string& comp_name)        const;
	bool controlExist(const std::string& control_name)       const;
	bool subcircuitExist(const std::string& subcircuit_name) const;
	bool elementExist(const std::string& comp_name)          const;

	// Automatic rename of elements with numbers to eliminate duplicates
	std::string correctName(std::string input_name, int starting_number = 1)     const;
	std::string correctNodeName(std::string input_name, int starting_number = 1) const;

	// Automatic name of elements, using different rules
	std::string ValidateName(const std::string& name, const ElementType elem_type, const ComponentType type = ComponentType::Impedance);
	std::string ValidateNodeName(const std::string& name);
	std::vector<std::string> ValidateNodeNames(const std::vector<std::string> & names);

	// Get methods
	std::vector<std::string> getComponentNames() const;
	std::vector<std::string> getNodeNames() const;
	std::vector<std::string> getSubcircuitNames() const;
	std::vector<std::string> getControlNames()    const;

	// Get elements
	Component* getComponent(const std::string & comp_name)      const;
	Control * getControl(std::string control_name) const;
	Subcircuit * getSubcircuit(std::string subcircuit_name) const;
	Element * getElement(std::string elem_name) const;
	Node * getNode(std::string node_name)      const;
	int getComponentPosition(std::string comp_name) const;
	int getNodePosition(std::string node_name) const;
	int getNodeNumber(std::string node_name)   const;
	int getControlPosition(std::string control_name) const;
	int getSubcircuitPosition(std::string subcircuit_name) const;

	ElementType getElementType(std::string elem_name) const;

	template<typename Container>
	std::vector<std::string> getVectorNames(const Container& v) const
	{
		std::vector<std::string> Output(v.size());
		for (int i=0; i<v.size(); i++)
		{
			Output[i] = v[i]->getName().c_str();
		}
		return (Output);
	}

private:
	int nextAutoComponent(ComponentType type);
	int nextAutoElem(ElementType type);

	const ComponentManager* componentManager;
	const std::map<std::string, Component*>* comp_dict; 
	const std::vector<Node*>* node;
	const std::vector<Control*>* control;
	const std::vector<Subcircuit*>* subcircuit;

	int nAutoComponents[static_cast<size_t> (ComponentType::Invalid)] = {0};
	int nAutoControl;
	int nAutoSubcircuit;
	int nAutoElement;
	int nAutoNodes;
};

