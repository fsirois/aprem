//-------------------------------------------------------------------------
// Printer methods for circuit class
// The methods implemented here are the ones that print useful data for
// a user or (mostly) for a programmer debugging.
//-------------------------------------------------------------------------

#include "circuitClass.h"

#include "Node.h"

using namespace std;

// Print components, with their associated nodes --------------------------
void circuit::printComponent() const
{
	const ComponentManager& comp = m_componentManager;

	printf("\nPrinting components :\n");
	for (int i=0; i<comp.size(); i++)
	{
		const Component* component = comp[i];

		if (component == nullptr)
		{
			continue;
		}

		vector<Node*> comp_node = component->getNode();
		string compName = component->getName();
		printf("%s : %d ", compName.c_str(), static_cast<int> (comp_node.size()));
		if (comp[i]->isOn())
		{
			printf(" (On) ");
		}
		else
		{
			printf(" (Off) ");
		}

		printf("nodes -->");
		for (int j=0; j<comp_node.size(); ++j)
		{
			int out = 0;
			if (comp_node[j] != nullptr)
			{
				out = comp_node[j]->getNumber();
			}

			printf(" %d",out);
		}
		printf("\n");
	}
}

// Printing nodes, with adjacent data -------------------------------------
void circuit::printNodes() const
{
	printf("There are %d NODES\n", static_cast<int> (node.size()));
	for (int i=0; i<node.size(); i++)
	{
		printf("%s -->\nadj_comp : ",node[i]->getName().c_str());
		vector<int> adj_comp = node[i]->getAdjComponent();
		for (int j = 0; j < adj_comp.size(); ++j)
		{
			printf(" %d", adj_comp[j]);
		}

		printf("\nadj_pos : ");
		vector<int> adj_pos = node[i]->getAdjPos();
		for (int j = 0; j < adj_pos.size(); ++j)
		{
			printf(" %d", adj_pos[j]);
		}

		printf("\nadj_nodes : ");
		vector<int> adj_nodes = node[i]->getAdjNode();
		for (int j = 0; j < adj_nodes.size(); ++j)
		{
			printf(" %d", adj_nodes[j]);
		}

		printf("\n");
	}
}

// Printing node voltage --------------------------------------------------
void circuit::printNodeVoltage() const
{
	for (int i=0; i<node.size(); i++)
	{
		printf("\n %s  V : %f %f ",node[i]->getName().c_str(), node[i]->getVr(), node[i]->getVi());
	}
}


void circuit::printControl() const
{
	printf("\n\n--------------------------\n");
	printf("Controls\n--------------------------\n");
	for (int i=0; i<control.size(); i++)
	{
		Control* curControl = control[i];

		printf("%s : ",curControl->getName().c_str());
		printf("\nFunctionName : %s", curControl->getFunctionName().c_str());

		printf("\nInputs");
		pair<vector<string>,vector<string> > OUT = curControl->getInputs();
		vector<string> Comp = OUT.first;
		vector<string> Att  = OUT.second;
		for (int j = 0; j < Comp.size(); ++j)
		{
			printf("\n   %s.%s", Comp[j].c_str(), Att[j].c_str());
		}

		printf("\nOutputs");
		OUT = curControl->getOutputs();
		Comp = OUT.first;
		Att  = OUT.second;
		for (int j = 0; j < Comp.size(); ++j)
		{
			printf("\n   %s.%s", Comp[j].c_str(), Att[j].c_str());
		}

		printf("\nData");
		vector<double> FctDat  = curControl->getFunctionData();
		for (int j = 0; j < FctDat.size(); ++j)
		{
			printf("\n-->%f", FctDat[j]);
		}

		printf("\n--------------------------\n\n");
	}
}

void circuit::printSubcircuit() const
{
	printf("\n\n--------------------------\n");
	printf("Subcircuits\n--------------------------\n");
	for (int i=0; i<subcircuit.size(); i++)
	{
		Subcircuit* curSubcircuit = subcircuit[i];
		if (curSubcircuit == nullptr)
		{
			continue;
		}

		string subName = curSubcircuit->getName();
		printf("%s : ",subName.c_str());

		vector<Subcircuit*> subLst = curSubcircuit->getSubcircuits();
		if (subLst.size() > 0)
		{
			printf("\nSubcircuits");
			for (int j=0; j<subLst.size(); ++j)
			{
				if (subLst[j] == nullptr)
				{
					continue;
				}

				subName = subLst[j]->getName();
				printf("\n    %s",subName.c_str());
			}
		}

		vector<Component*> compLst = curSubcircuit->getComponents();
		if (compLst.size() > 0)
		{
			printf("\nComponents");
			for (int j=0; j<compLst.size(); ++j)
			{
				if (compLst[j] == nullptr)
				{
					continue;
				}

				string compName = compLst[j]->getName();
				printf("\n    %s",compName.c_str());
			}
		}
		
		vector<Control*> ctrlLst = curSubcircuit->getControls();
		if (ctrlLst.size() > 0)   
		{
			printf("\nControls");
			for (int j=0; j<ctrlLst.size(); ++j)
			{
				if (ctrlLst[j] == nullptr)
				{
					continue;
				}

				string ctrlName = ctrlLst[j]->getName();
				printf("\n    %s",ctrlName.c_str());
			}
		}
		
		printf("\n--------------------------\n\n");
	}
}

void circuit::print(std::string option) const
{
	if      (option == "NodeVoltage") printNodeVoltage();
	else if (option == "Nodes")       printNodes();
	else if (option == "Comp")        printComponent();
	else if (option == "Control")     printControl();
	else if (option == "Subcircuit")  printSubcircuit();
}

