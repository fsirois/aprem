
#include "circuitClass.h"

#include "Node.h"

using namespace std;

bool circuit::deleteControl(string ctrlName)
{
	int ctrlPos = NameHandler.getControlPosition(ctrlName);
	if (ctrlPos == -1)
		return false;

	Control * curCtrl = control[ctrlPos];

	// Delete control from subcircuit
	Subcircuit * compSubcircuit = curCtrl->getSubcircuit();
	if (compSubcircuit != NULL)
	{
		compSubcircuit->removeControl(curCtrl->getName());
	}

	// Tell the components they do not have a controller anymore
	vector<Component*> comp_vec = curCtrl->getInComponent();
	for (int i=0; i<comp_vec.size(); ++i)
		comp_vec[i]->setControl(NULL);

	comp_vec = curCtrl->getOutComponent();
	for (int i=0; i<comp_vec.size(); ++i)
		comp_vec[i]->setControl(NULL);

	// Delete the control and remove it from main circuit
	delete curCtrl;
	circuit_ns::remove_at(control, ctrlPos);

	return true;
}

bool circuit::deleteControls(vector<string> ctrlNames)
{
	for (int i=0; i<ctrlNames.size(); ++i)
		if (!deleteControl(ctrlNames[i]))
			return false;
	return true;
}

bool circuit::deleteSubcircuit(string subName, bool deleteChildren)
{
	int subPos = NameHandler.getSubcircuitPosition(subName);
	if (subPos == -1)
		return true;

	Subcircuit * curSub = subcircuit[subPos];
	circuit_ns::remove_at(subcircuit, subPos);

	// Delete subcircuit's components (and tell them not to delete
	// their subcircuit when it is empty, because we are currently
	// deleting it)
	vector<string> subCompLst = curSub->getComponentNames();
	deleteComponents(subCompLst, false /*deleteSubcircuit*/);

	// Delete current subcircuit's subcircuits
	if (deleteChildren)
	{
		vector<Subcircuit*> subSubLst = curSub->getSubcircuit();
		for (int i=0; i<subSubLst.size(); ++i)
			deleteSubcircuit(subSubLst[i]->getName());
	}

	// Remove current subcircuit from master, and delete master if empty
	Subcircuit * curMaster = curSub->getMaster();
	if (curMaster != NULL)
	{
		curMaster->removeSubcircuit(curSub->getName());
		
		if (curMaster->isEmpty())
		{
			deleteSubcircuit(curMaster->getName(), false /* deleteChildren */);
		}
	}

	delete curSub;
	return true;
}

bool circuit::deleteSubcircuits(vector<string> subNames)
{
	for (int i=0; i<subNames.size(); ++i)
		if (!deleteSubcircuit(subNames[i]))
			return false;
	enumerate();
	return true;
}

// Delete one component ---------------------------------------------------
// This method deletes one given component. If the component does not
// exist, nothing is done. It removes the element from the list of
// components and the list of components of its type.
// It does not delete the element's nodes if they are not used anymore.
bool circuit::deleteComponent(string curComp_name, bool del_sub)
{
	int position = NameHandler.getComponentPosition(curComp_name);
	if (position == -1)
		return false;

	// Step 1 : delete the component
	Component* curComp = nullptr; // comp[position];
	
	// Delete control
	Control * compControl = curComp->getControl();
	if (compControl != NULL)
	{
		deleteControl(compControl->getName());
	}

	// Delete subcircuit
	if (del_sub)
	{
		Subcircuit * compSubcircuit = curComp->getSubcircuit();
		if (compSubcircuit != NULL)
		{
			compSubcircuit->removeComponent(curComp->getName());
			
			if (compSubcircuit->isEmpty())
			{
				deleteSubcircuit(compSubcircuit->getName(), false /*deleteChildren*/);
			}
		}
	}

	// Step 2 : erase pointer from both vector it is in + dict
	comp_dict.erase(curComp_name); 
	m_componentManager.RemoveComponent(curComp);
	delete curComp;

	topologyChanged = 1;
	enumerated = false;
	return (true);
}

// Delete components ------------------------------------------------------
// This method deletes the elements listed in comp_to_delete. If the
// component does not exist, nothing will be done. The components will be
// removed from the list using it.
// If the nodes of the elements are not used anymore, they will be deleted
// automatically and removed from the list of nodes, except if it is the
// ground.
bool circuit::deleteComponents(vector<string> comp_to_delete, bool del_sub)
{
	if (comp_to_delete.size() == 0)
		return true;

	std::set<int> nodeNumberSet;
	
	//*****************************
	// Step 1 - Delete components *
	//*****************************
	for (int i=0; i<comp_to_delete.size(); i++)
	{
		Component* curComp = NameHandler.getComponent(comp_to_delete[i]);
		
		if (curComp != NULL)
		{
		// Memorize nodes
		vector<Node*> compNodes = curComp->getNode();
		for (int j=0; j<compNodes.size(); ++j)
		{
			// Do not add if it is the ground
			if (compNodes[i] != Ground)   
				nodeNumberSet.insert(compNodes[j]->getNumber());
		}
		deleteComponent(curComp->getName(), del_sub);
		}
	}

	//************************
	// Step 2 - Delete nodes *
	//************************
	// Enumerate to get neighbor's data
	if (!enumerated)
		enumerate();

	// Delete unused nodes : Start from the last in the node vector, since
	// it will not change the position of the previous nodes in the vector
	// and we do not have to enumerate for every node deleted.
	std::set<int>::reverse_iterator It;
	for (It = nodeNumberSet.rbegin(); It != nodeNumberSet.rend(); ++It)
	{
		int pos = *It - 1;
		
		// Do not delete ground
		if (pos == -1)
			continue;

		// If the only adjacent component of the node is the current
		// component, delete this node since it will not be used
		if (node[pos]->getAdjComponent().size() == 0)
		{
			delete node[pos];
			circuit_ns::remove_at(node, pos);
		}
	}

	enumerate();
	return (true);
}

// Delete elements ------------------------------------------------------
// Most generic way to delete items of the circuit, this function will
// delete controls, subcircuit or components
bool circuit::deleteElements(vector<string> names)
{
	vector<string> comp_names;
	vector<string> subcircuit_names;
	vector<string> control_names;

	for (int i=0; i<names.size(); ++i)
	{
		ElementType elem_type = getElementType(names[i]);
		if (elem_type == ElementType::Invalid)
			continue;
		
		if (elem_type == ElementType::Component)
			comp_names.push_back(names[i]);

		else if (elem_type == ElementType::Control)
			control_names.push_back(names[i]);
		
		else if (elem_type == ElementType::Subcircuit)
			subcircuit_names.push_back(names[i]);
	}

	deleteSubcircuits(subcircuit_names);
	deleteControls(control_names);
	deleteComponents(comp_names);

	return 1;
}

