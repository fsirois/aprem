//-------------------------------------------------------------------------
// Setter/getter methods implementation for circuitClass
//-------------------------------------------------------------------------

#include "circuitClass.h"

#include "Node.h"


#if defined(_WIN64) || defined(_WIN32)
#define OPENMP_ENABLED 1
#else
#define OPENMP_ENABLED 0
#endif

#if OPENMP_ENABLED
#include "omp.h"
#endif

using namespace std;

//-------------------------------------------------------------------------
// Setter methods
//-------------------------------------------------------------------------

bool circuit::setComponents(const vector<string> & elem,
							const vector<string> & param,
								  vector<pair<double,double> > & values)
{
	if (elem.size() != param.size() || elem.size() != values.size())
		return false;
	
	bool error = false;

#if OPENMP_ENABLED
	#pragma omp parallel
	{
	#pragma omp for
#endif
	for (int i=0; i<elem.size(); ++i)
	{
		bool OK = set(elem[i], param[i], values[i].first, values[i].second);
		if (!OK) error = true;
	}

#if OPENMP_ENABLED
	}
#endif

	return (!error);
}


// Set node ---------------------------------------------------------------
bool circuit::setNode(string node_name, string field_to_set, int value)
{
	Node * curNode = NameHandler.getNode(node_name);

	bool returnValue = true;
	if (curNode == NULL)
		return false;

	ComponentManager& comp = m_componentManager;

	if (field_to_set == "tempGND")
		curNode->setAsTempGND( value!=0 );
	else if (field_to_set == "on")
	{
		if (!enumerated)
		{
			enumerate();
		}

		// Si le noeud est activé et qu'on le désactive.
		if (curNode->getOn() > 0 && value == 0)
		{
			vector<int> adj_comp = curNode->getAdjComponent();

			// Pour tous les composants liés au noeud.
			for (int j=0; j<adj_comp.size(); ++j)
			{
				Component* curComp = comp[adj_comp[j]-1];
				vector<Node*> compNodes = curComp->getNodes();

				// On remplace l'occurence du noeud auprès
				// de ces composants par un nouveau noeud.
				for (int k=0; k<compNodes.size(); ++k)
				{
					if (compNodes[k]->getName() == curNode->getName())
					{
						// On crée un nom de noeud automatique.
						string tempNodeName = NameHandler.ValidateNodeName("");

						// Création du noeud "fragment".
						Node * newNode = addNode(tempNodeName);
						
						// On indique quel noeud est à l'origine du noeud fragment.
						newNode->setOriginalNode(curNode);

						// Mise à jour des noeuds du composant.
						compNodes[k] = newNode;
						curComp->setNodes(compNodes);

						// Inscription du noeud "fragment" dans une liste pour
						// pouvoir le supprimer lorsqu'on r�activera le noeud original.
						curNode->addFragment(newNode);
					}
				}
			}
		}

		// Si le noeud est désactivé et qu'on l'active.
		else if (curNode->getOn() == 0 && value > 0)
		{
			// On récupère les noeuds fragments qui découlent de sa désactivation.
			vector<Node*> tempNodeFragments = curNode->getFragments();
			
			// On récupère les composants voisins aux noeuds fragmentés et
			// on modifie leur liste de noeuds pour remplacer les noeuds
			// fragmentés par le noeud original.

			for (int j=0; j <tempNodeFragments.size(); ++j)
			{
				Node * nodeFragment = tempNodeFragments[j];
				// Avant de procéder, on s'assure que les noeuds
				// "fragments" ne sont pas fragmentés eux-même.
				if (nodeFragment->hasFragments())
				{
					setNode(nodeFragment->getName(), "on", 1);
					//enumerate();
				}

				vector<int> adj_comp = nodeFragment->getAdjComponent();
				for (int k=0; k<adj_comp.size(); ++k)
				{
					Component* curComp = comp[adj_comp[k]-1];
					vector<Node*> compNodes = curComp->getNodes();
					for (int l=0; l<compNodes.size(); ++l)
					{
						if (compNodes[l]->getName() == nodeFragment->getName())
						{
							compNodes[l] = curNode;
							curComp->setNodes(compNodes);
						}
					}
				}

				// Delete fragment (should create a method deleteNode)
				int index = NameHandler.getNodePosition(nodeFragment->getName());
				circuit_ns::remove_at(node, index);
				delete nodeFragment;
			}
		}

		curNode->setOn( (unsigned char) value );
		enumerate();
	}

	else // error
		returnValue = false;

	return (returnValue);
}

bool circuit::setIONodes(vector<string> new_input_output_names)
{
	input_output_nodes.clear();

	for (int i=0; i<new_input_output_names.size(); ++i)
	{
		Node * curNode = NameHandler.getNode(new_input_output_names[i]);
		
		if (curNode == NULL)
			continue;

		input_output_nodes.push_back(curNode);
	}

	return true;
}

vector<string> circuit::getIONodeNames() const
{
	return NameHandler.getVectorNames(input_output_nodes);
}

int circuit::get_nIONodes() const
{
	return static_cast<int> (input_output_nodes.size());
}

// Set component ----------------------------------------------------------
bool circuit::set(string comp_name, string field, double in_r, double in_i)
{
	// Gets element interface for comp, control or subcircuit
	Component* curComp = NameHandler.getComponent(comp_name);
	if (curComp == NULL)
		return false;

	// Automatically calls the proper get method
	bool FieldIsCorrect = curComp->set(field, in_r, in_i);

	return FieldIsCorrect;
}

bool circuit::setControlOn(string control_name,
						   bool on)
						   
{
	Control * curCtrl = NameHandler.getControl(control_name);
	if (curCtrl == NULL)
		return false;
	
	curCtrl->setOn(on);
	return true;
}

#if COMPILE_C_VERSION
bool circuit::setControlFunction(string control_name,
								 controlFunction function_data)
{
	FunctionHandler::RegisterFunction(control_name, function_data);
	return true;
}
#endif

bool circuit::setControlFunctionName(string control_name,
									 string function_name)
{
	Control * curCtrl = NameHandler.getControl(control_name);
	if (curCtrl == NULL)
		return false;

	curCtrl->setFunctionName(function_name);
	return true;
}

bool circuit::setControlFunctionData(string control_name,
									 vector<double> function_data)
{
	Control * curCtrl = NameHandler.getControl(control_name);
	if (curCtrl == NULL)
		return false;

	curCtrl->setFunctionData(function_data);
	return true;
}

bool circuit::setControl(string field_to_set,
						 string control_name,
						 vector<string> ComponentNames,
						 vector<string> Attributes)
{

	Control * curCtrl = NameHandler.getControl(control_name);

	bool FieldIsCorrect = true;
	vector<Component*> Components;

	for (int i = 0; i < ComponentNames.size(); i++)
	{
		Component* curComp = NameHandler.getComponent(ComponentNames[i]);
		Components.push_back(curComp);
	}

	if (field_to_set == "inputs")
		curCtrl->setInputs(Components, Attributes);
	else if (field_to_set == "outputs")
		curCtrl->setOutputs(Components, Attributes);
	else if (field_to_set == "inputComponents")
		curCtrl->setInputComponents(Components);
	else if (field_to_set == "outputComponents")
		curCtrl->setOutputComponents(Components);
	else
		FieldIsCorrect = false;

	return FieldIsCorrect;
} 


//-------------------------------------------------------------------------
// Get
//-------------------------------------------------------------------------

// Get node voltage -------------------------------------------------------
pair<double,double> circuit::getNodeV(string node_name) const
{
	pair<double,double> Output;

	Node * curNode = NameHandler.getNode(node_name);
	if (curNode == NULL)
	{
		Output.first = 0.0;
		Output.second = 0.0;
	}
	else
	{
		Output.first = curNode->getVr();
		Output.second = curNode->getVi();
	}
	return (Output);
}

// General get for node ---------------------------------------------------
vector<double> circuit::getNode(string node_name, string field) const
{
	Node * curNode = NameHandler.getNode(node_name);

	vector<double> OutputVector;
	if (curNode == NULL)
	{
		OutputVector.push_back(0.0);
	}
	else
	{
		OutputVector = curNode->get(field);
	}
	return (OutputVector);
}

// Get component node names -----------------------------------------------
vector<string> circuit::getComponentNodeNames(string comp_name) const
{
	Component* curComp = NameHandler.getComponent(comp_name);
	vector<string> OutNames;
	if (curComp != NULL)
	{
		OutNames = curComp->getNodeNames();
	}
	return(OutNames);
}

// Get component voltage --------------------------------------------------
vector<pair<double,double> > circuit::getV(const string & comp_name) const
{
	Component* curComp = NameHandler.getComponent(comp_name);
	
	vector<pair<double,double> > Out;
	if (curComp != NULL)
	{
		if (!curComp->isOn())
		{
			Out.push_back(pair<double,double>(0.0, 0.0));
			return Out;
		}

		Out = curComp->getV();
	}
	return (Out);
}

// General get for components ---------------------------------------------
vector<pair<double,double> > circuit::get(const string & comp_name,
										  const string & attribute_name) const
{
	vector<pair<double,double> > OutputVector;

	// Gets element interface for comp, control or subcircuit
	Component* curComp = NameHandler.getComponent(comp_name);
	if (curComp == NULL)
		return (OutputVector);

	if (!curComp->isOn())
	{
		OutputVector.push_back(pair<double,double>(0.0, 0.0));
		return OutputVector;
	}

	// Automatically get the correct get method
	OutputVector = curComp->get(attribute_name);
	return (OutputVector);
}

// General get for components ---------------------------------------------
vector<pair<double,double> > circuit::getComponents(const vector<string> & comp,
													const vector<string> & param) const
{
	vector<pair<double,double> > out;

	if (comp.size() != param.size())
		return out;

	out.resize(comp.size());
	
	#pragma omp parallel
	{
		
	#pragma omp for
	for (int i=0; i<comp.size(); ++i)
	{
		out[i] = get(comp[i], param[i])[0];
	}
	
	}
	
	return out;
}

// General get for components ---------------------------------------------
void circuit::getComponents(const vector<string> & comp,
							const vector<string> & param,
								  vector<pair<double,double> > & out) const
{
	if (comp.size() != param.size())
		return;

	out.resize(comp.size());

	#pragma omp parallel
	{

	#pragma omp for
	for (int i=0; i<comp.size(); ++i)
	{
		out[i] = get(comp[i], param[i])[0];
	}

	}
}

// Get names
vector<string> circuit::getNames(string option) const
{
	if      (option == "Comp")       return getComponentNames();
	else if (option == "Node")       return getNodeNames();
	else if (option == "IONode")     return getIONodeNames();
	else if (option == "Control")    return getControlNames();  
	else if (option == "Subcircuit") return getSubcircuitNames();

	// specific component types
	const ComponentType typeEnum = circuit_ns::StringToComponentType(option);
	return getComponentNames(typeEnum);
}



