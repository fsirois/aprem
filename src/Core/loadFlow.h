/*! \file *********************************************************************
 *
 * \brief  Calcule l'écoulement de puissance dans le circuit.
 *
 *      Il est essentiel d'utiliser la fonction "enumerate()" avant d'utiliser "loadFlow()".
 *
 * \author
 *      Frédérick Cyr
 *      École Polytechnique de Montréal
 *
 *      Crédits à Philippe Ossoucah Jr. pour l'origine du projet APREM.
 *
 *****************************************************************************/

#pragma once

#include "circuitClass.h"

bool _isinf(double N) {return N == std::numeric_limits<double>::infinity();};

constexpr unsigned int N_ITER_MAX = 50; // Nombre maximal d'itérations.
constexpr double TOLERANCE = 1.0e-6; // Tolérance (critère d'arrêt).
constexpr double DROP_TOL = 0.00000001;

