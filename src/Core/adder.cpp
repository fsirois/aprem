
#include "circuitClass.h"

#include "Node.h"

using namespace std;

//-------------------------------------------------------------------------
// Main elements of the circuit
//-------------------------------------------------------------------------
Node* circuit::addNode(const string& name)
{
	string outputName = NameHandler.ValidateNodeName(name);

	Node* outputNode = NameHandler.getNode(outputName);
	if (outputNode != NULL)
	{
		return (outputNode);
	}

	if (outputName != "0")
	{
		outputNode = new Node(outputName);
		outputNode->setNumber(static_cast<int> (node.size()) + 1);
		node.push_back(outputNode);
	}
	else
	{
		outputNode = Ground;
	}
	
	return (outputNode);
}

string circuit::addComponent(const string& name,
							 const vector<string>& nodeList,
							 const vector<double>& params,
							 const string& type_str,
							 CircuitErrorCode& errorCode)
{
	ComponentType comp_type = circuit_ns::StringToComponentType(type_str);
	return addComponent(name, nodeList, params, comp_type, errorCode);
}

string circuit::addComponent(const string& name,
							 const vector<string>& nodeList,
							 const vector<double>& params,
							 const ComponentType comp_type,
							 CircuitErrorCode& errorCode)
{
	#define ADD_COMPONENT_MACRO(N) case N: return addComponent<N>(name, nodeList, params, errorCode);

	const unsigned int comp_index = static_cast<unsigned int>(comp_type);
	switch (comp_index)
	{
		REPEAT(20, ADD_COMPONENT_MACRO)
	}

	return "";
}

//-------------------------------------------------------------------------
// Controls
//-------------------------------------------------------------------------
// By value
string circuit::addControl(vector<string>  inputComponents,
						   vector<string>  inputAttributes,
						   vector<string>  outputComponents,
						   vector<string>  outputAttributes,
						   vector<double>  functionData,
						   string          functionName,
						   string          controlName,
						   controlFunction functionPtr)
{  
	string OutputName = NameHandler.ValidateName(controlName, ElementType::Control);
	
	vector<Component*> inputComp;
	for (int i=0; i<inputComponents.size(); ++i)
	{
		Component* curComp = NameHandler.getComponent(inputComponents[i]);
		if (curComp == NULL)
			return ("");
		inputComp.push_back(curComp);
	}
	
	vector<Component*> outputComp;
	for (int i=0; i<outputComponents.size(); ++i)
	{
		Component* curComp = NameHandler.getComponent(outputComponents[i]);
		if (curComp == NULL)
			return ("");
		outputComp.push_back(curComp);
	}
	
	Control * NewControl = new Control(inputComp,
									   inputAttributes,
									   outputComp,
									   outputAttributes,
									   functionData,
									   functionName,
									   controlName);

#if COMPILE_C_VERSION
	if (functionPtr)
		FunctionHandler::RegisterFunction(functionName, functionPtr);
#endif

	// TODO : Validate this
	NewControl->updateSlaves();

	control.push_back(NewControl);

	return OutputName;
}

// By copy
string circuit::addControl(Control * control_in, string controlName)
{
	string newName = NameHandler.ValidateName(controlName, ElementType::Control);

	Control * NewControl = new Control(control_in, newName);
	control.push_back(NewControl);
	
	return newName;
}

