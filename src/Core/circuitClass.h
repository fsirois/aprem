
#pragma once

#include <math.h>
#include <string.h>
#include <vector>
#include <set>
#include <algorithm>
#include <stdio.h>

#include "support_functions.h"
#include "CircuitElements.h"
#include "circuitNameHandler.h"
#include "call_control_function.h"

#include "CircuitInterface.h"
#include "DrawInfo.h"

#include "compilerOptions.h"
#if COMPILE_C_VERSION
	#include "FunctionHandler.h"
#endif

#include "ComponentManager.h"

#include "Solver.h"

// Definition of the regular path for INI file depends on exp sys
#if defined(_WIN64) || defined(_WIN32)
	static const std::string kIniPathDefault = "C:\\";
#elif defined(__linux)
	static const std::string kIniPathDefault = "/usr/";
#else
	static const std::string kIniPathDefault = "";
#endif

#define MAX_ITER 100

struct ParametrizedProperty
{
	ParametrizedProperty(const std::string& name, const double multiplier) :
		m_sProperty{name},
		m_fMultiplier{multiplier}
	{
	}

	std::string m_sProperty;
	double m_fMultiplier;
};

class ParametrizedComponent
{
public:
	ParametrizedComponent(const std::string& name, const std::string& propertyName, const double multiplier) :
		m_sName{name}
	{
		m_vsProperties.emplace_back(propertyName, multiplier);
	}

	const std::string& getName() const { return m_sName; }
	std::vector<ParametrizedProperty> m_vsProperties;

private:
	std::string m_sName;
};

class circuit : public CircuitInterface
{
//-------------------------------------------------------------------------
// Public methods
//-------------------------------------------------------------------------
public:
	circuit();
	circuit(const circuit * const Circuit);
	~circuit();
	
	bool readIni(std::string iniFilePath = kIniPathDefault);
	void BuildOutput(std::vector<std::string>& varNames, std::vector<std::pair<double, double>>& values);

	// Principal methods
	virtual CircuitErrorCode solve() override;
	void enumerate();
	virtual DrawInfo Load(const std::string& fileName, CircuitErrorCode& errorCode) override;
	virtual CircuitErrorCode Validate() const override;

	template<typename T>
	std::string addComponent(T* new_comp, const std::vector<std::string>& node_names, CircuitErrorCode& errorCode)
	{
		// Creating nodes if they do not exist
		std::vector<Node*> NewNodes;
		for (const std::string& nodeName : node_names)
		{
			NewNodes.push_back(addNode(nodeName));
		}

		new_comp->setNodes(NewNodes);
		push_back(m_componentManager, new_comp);

		// Rename the component if needed
		const std::string new_name = NameHandler.ValidateName(new_comp->getName(), ElementType::Component, new_comp->getType());
		new_comp->setName(new_name);

		comp_dict[new_name] = new_comp;

		// Invalidate circuit
		topologyChanged = 1;
		enumerated = 0;

		errorCode = new_comp->Validate();

		return new_name;
	}

	template<typename T>
	std::string addComponent(const std::string& name, const std::vector<std::string>& node_names, const std::vector<double>& values, CircuitErrorCode& errorCode)
	{
		T* new_comp = new T(values, name);
		return addComponent<T>(new_comp, node_names, errorCode);
	}

	template<ComponentType type>
	std::string addComponent(const std::string& name, const std::vector<std::string>& node_names, const std::vector<double>& values, CircuitErrorCode& errorCode)
	{
		using T = ComponentTypeSelector<type>;
		return addComponent<T>(name, node_names, values, errorCode);
	}

	template<unsigned int type>
	std::string addComponent(const std::string& name, const std::vector<std::string>& node_names, const std::vector<double>& values, CircuitErrorCode& errorCode)
	{
		using T = ComponentTypeSelector<static_cast<ComponentType>(type)>;
		return addComponent<T>(name, node_names, values, errorCode);
	}

	template<typename T>
	std::string addComponent(const T* comp, CircuitErrorCode& errorCode)
	{
		T* new_comp = new T(*comp);
		return addComponent<T>(new_comp, comp->getNodeNames(), errorCode);
	}

	template<ComponentType type>
	std::string addComponent(const Component* comp, CircuitErrorCode& errorCode)
	{
		using T = ComponentTypeSelector<type>;
		T* new_comp = new T(*(static_cast<const T*>(comp)));
		return addComponent<T>(new_comp, comp->getNodeNames(), errorCode);
	}

	std::string addComponent(const std::string& name, const std::vector<std::string>& node, const std::vector<double>& values, const std::string& type_str, CircuitErrorCode& errorCode) override;
	std::string addComponent(const std::string& name, const std::vector<std::string>& node, const std::vector<double>& values, const ComponentType type, CircuitErrorCode& errorCode);

	std::string addControl(Control * control_in, std::string controlName = "");
	std::string addControl(std::vector<std::string> inputComponents,
						   std::vector<std::string> inputAttributes,
						   std::vector<std::string> outputComponents,
						   std::vector<std::string> outputAttributes,
						   std::vector<double> functionData,
						   std::string functionName,
						   std::string controlName,
						   controlFunction functionPtr = nullptr) override;
	
	// Add subcircuit (addSubcircuit.cpp)
	bool addSubcircuit(circuit* Circuit, std::string name, std::vector<std::string> IONodes);
	bool addSubcircuit(CircuitInterface* Circuit, std::string name, std::vector<std::string> IONodes) override;
	
	// Set (setter_getter.cpp)
	bool setComponents(const std::vector<std::string> & comp,
					   const std::vector<std::string> & param,
							 std::vector<std::pair<double,double> > & values) override;
	bool setNode(std::string node_name, std::string field_to_set, int value) override;
	bool setIONodes(std::vector<std::string> new_input_output_names) override;
	bool setControlOn(std::string control_name, bool on) override;
	bool setControlFunctionName(std::string control_name,
								std::string function_name);
	bool setControlFunctionData(std::string control_name,
								std::vector<double> function_data) override;
	bool setControl(std::string field_to_set,
					std::string control_name,
					std::vector<std::string> ComponentNames,
					std::vector<std::string> Attributes) override;
#if COMPILE_C_VERSION
	bool setControlFunction(std::string control_name,
							controlFunction function_data);
#endif
	bool set(std::string comp_name, std::string field, double in_r, double in_i = 0.0) override;
	Component* GetComponentByName(const std::string& componentName) { return NameHandler.getComponent(componentName); }

	// Get (setter_getter.cpp)
	std::vector<std::pair<double,double> > getComponents(const std::vector<std::string> & comp,
							 const std::vector<std::string> & param) const override;
	void getComponents(const std::vector<std::string> & comp,
					   const std::vector<std::string> & param,
					   std::vector<std::pair<double,double> > & out) const override;
	std::vector<double> getNode(std::string node_name, std::string field) const override;
	std::pair<double,double> getNodeV(std::string field) const override;
	std::vector<std::string> getComponentNodeNames(std::string comp_name) const;
	std::vector<std::pair<double,double> > get(const std::string & comp_name,
						   const std::string & field) const override;
	std::vector<std::pair<double,double> > getV(const std::string & name) const override;
	
	ElementType getElementType(std::string elem_name) const;
	int  get_nIONodes() const;
	std::vector<std::string> getIONodeNames() const;
	
	
	// Delete methods (deleter.cpp)
	bool deleteElements(std::vector<std::string> elements_to_delete) override;
	bool deleteComponents(std::vector<std::string> comp_to_delete, bool deleteSubcircuit = true);
	bool deleteComponent(std::string comp_to_delete, bool deleteSubcircuit = true);
	bool deleteControl(std::string ctrlName);
	bool deleteControls(std::vector<std::string> ctrlNames);
	bool deleteSubcircuit(std::string subName, bool deleteChildren = true);
	bool deleteSubcircuits(std::vector<std::string> subNames);

	// Printing values (printer.cpp)
	void printNodeVoltage() const;
	void printComponent() const;
	void printNodes() const;
	void printControl() const;
	void printSubcircuit() const;
	void print(std::string option) const override;

	template<typename T>
	std::vector<std::string> getComponentNames() const { return NameHandler.getVectorNames(GetContainer<T*>(m_componentManager)); };
	std::vector<std::string> getComponentNames(const ComponentType type) const
	{
		std::vector<std::string> output;
		m_componentManager.ForEachComponent([&output](const auto& comp)
		{
			output.push_back(comp->getName());
		});

		return output;
	}

	std::vector<std::string> getComponentNames() const {return NameHandler.getComponentNames();}
	std::vector<std::string> getNodeNames() const {return NameHandler.getNodeNames();}
	std::vector<std::string> getSubcircuitNames() const {return NameHandler.getSubcircuitNames();}
	std::vector<std::string> getControlNames() const {return NameHandler.getControlNames();}
	std::vector<std::string> getNames(std::string Type) const override;

//-------------------------------------------------------------------------
// Private methods
//-------------------------------------------------------------------------   
private:
	Node* addNode(const std::string& name);

	void loadFlow(CircuitErrorCode& errorCode);
	CircuitErrorCode checkTopology();
	void resetFlags();

	// Call matlab function (circuitClass.cpp)
	std::pair<bool,std::vector<std::pair<double,double> > > callControlFunction(
								std::string functionName,
								std::vector<double> functionData,
								std::vector<std::pair<double,double> > data_in,
								std::vector<std::pair<double,double> > data_out) override;

	void collectData(Eigen::VectorXd X); // TODO : put this in the solver some way
	bool checkMatrixSingularity(Eigen::SparseMatrix<double> & Yini);
	
	void buildMatrix(Eigen::VectorXd & B, Eigen::SparseMatrix<double> & Yini,
					 Eigen::SparseMatrix<double> & mainMatrix,
					 Eigen::SparseMatrix<double> & jacobian);

	Eigen::VectorXd computeXini(const Eigen::VectorXd & B,
								SparseMatrixXd & Yini);

	bool newton(Eigen::VectorXd & B,
				Eigen::SparseMatrix<double> & mainMatrix,
				Eigen::VectorXd & Xn,
				Eigen::SparseMatrix<double> & jacobian);

public:
	std::vector<circuit_ns::CircuitOutput> m_Outputs;
	std::vector<ParametrizedComponent> m_ParametrizedComponents;
	std::string m_sCircuitTag;
	std::string m_sCircuitName;

private:
	// Name Handler to help manage names of various elements
	circuitNameHandler NameHandler;
	Solver solver;

	// Main state flags of the circuit
	bool topologyChanged;
	bool enumerated;

	// All elements of the circuit
	Node * Ground;      // Ground node

	std::map<std::string, Component*> comp_dict; // Comp dict, to find them easily
	std::vector<Node*> node; // Nodes of the circuit (excluding ground)
	std::vector<Node*> input_output_nodes;

public:
	ComponentManager m_componentManager;

private:
	std::vector<Control*> control;
	std::vector<Subcircuit*> subcircuit;

	int maxIterControl;

	int cnodes;
};

