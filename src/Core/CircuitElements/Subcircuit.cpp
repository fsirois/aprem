//-------------------------------------------------------------------------
// Subcircuit implementation
//-------------------------------------------------------------------------

#include "Subcircuit.h"

#include "support_functions.h"

using namespace std;

Subcircuit::Subcircuit():Element(ElementType::Subcircuit)
{
	GND = 0; // ???
	slackBus = 0; // ???
	master_subcircuit = NULL;
}

Subcircuit::Subcircuit(string name_in):Element(ElementType::Subcircuit)
{
	name = name_in;
	GND = 0; // ???
	slackBus = 0; // ???
	master_subcircuit = NULL;
}

Subcircuit::Subcircuit(Subcircuit* subCircuit, string new_name):Element(ElementType::Subcircuit)
{
	GND = subCircuit->GND; // ???
	slackBus = subCircuit->slackBus; // ???

	comp = subCircuit->comp;
	control = subCircuit->control;
	
	subcircuit.clear();
	for (int i=0; i<subCircuit->subcircuit.size(); ++i)
	{
		Subcircuit * curSubcircuit = subCircuit->subcircuit[i];
		{
			subcircuit.push_back(curSubcircuit);
		}
	}
	
	name = new_name;
	
	master_subcircuit = NULL;
}

//-------------------------------------------------------------------------
// Add/remove
//-------------------------------------------------------------------------
pair<bool,string> Subcircuit::addComponent(Component* comp_to_add)
{
	pair<bool,string> Output;
	Output.first = true;

	for (int i=0; i<comp.size(); ++i)
	{
		if (comp_to_add->getName() == comp[i]->getName())
		{
			Output.second = "The component already exist.";
			Output.first = false;
			return Output;
		}
	}

	comp.push_back(comp_to_add);
	
	return Output;
}

pair<bool,string> Subcircuit::addControl(Control* Control_in)
{
	pair<bool,string> Output;
	Output.first = true;
	
	for (int i=0; i<control.size(); ++i)
	{
		if (Control_in->getName() == control[i]->getName())
		{
			Output.second = "The control already exist.";
			Output.first = false;
			return Output;
		}
	}

	control.push_back(Control_in);
	return Output;
}

void Subcircuit::addSubcircuit(Subcircuit * subcircuit_in)
{
	subcircuit.push_back(subcircuit_in);
}
  
bool Subcircuit::removeSubcircuit(std::string subcircuit_name)
{
	int pos = circuit_ns::get_position(subcircuit, subcircuit_name);
	
	if (pos == -1)
		return false;
	
	circuit_ns::remove_at(subcircuit, pos);
	return true;
}

bool Subcircuit::removeComponent(std::string component_name)
{
	int pos = circuit_ns::get_position(comp, component_name);
	
	if (pos == -1)
		return false;
	
	circuit_ns::remove_at(comp, pos);
	return true;
}

bool Subcircuit::removeControl(std::string control_name)
{
	int pos = circuit_ns::get_position(control, control_name);
	
	if (pos == -1)
		return false;
	
	circuit_ns::remove_at(control, pos);
	return true;
}


//-------------------------------------------------------------------------
// Set methods
//-------------------------------------------------------------------------
void Subcircuit::setOn(bool on_in)
{
	on = on_in;
}

void Subcircuit::setName(const string& name_in)
{
	name = name_in;
}

void Subcircuit::setIONodes(vector<Node*> nodes_input)
{
	IONodes = nodes_input;
}

void Subcircuit::setMaster(Subcircuit * master)
{
	master_subcircuit = master;
}

void Subcircuit::setComponents(vector<Component*> newComp)
{
	comp = newComp;
}

void Subcircuit::setControls(std::vector<Control*> newCtrl)
{
	control = newCtrl;
}

void Subcircuit::setSubcircuits(vector<Subcircuit*> newSubcircuit)
{
	subcircuit = newSubcircuit;
}

void Subcircuit::updateSlaves()
{
	for (int i = 0; i<comp.size(); ++i)
		comp[i]->setSubcircuit(this);
	for (int i = 0; i<control.size(); ++i)
		control[i]->setSubcircuit(this);
	for (int i = 0; i<subcircuit.size(); ++i)
		subcircuit[i]->setMaster(this);
}

//-------------------------------------------------------------------------
// Get methods
//-------------------------------------------------------------------------

bool Subcircuit::inSubcircuit() const
{
	return master_subcircuit != NULL;
}

bool Subcircuit::hasComponents() const
{
	return !comp.empty();
}

bool Subcircuit::hasSubcircuit() const
{
	return !subcircuit.empty();
}

bool Subcircuit::isEmpty() const
{
	return (!hasSubcircuit() && !hasComponents());
}

vector<Component*> Subcircuit::getComponents() const
{
	return comp;
}

vector<Subcircuit*> Subcircuit::getSubcircuits() const
{
	return subcircuit;
}

vector<Control*> Subcircuit::getControls() const
{
	return control;
}

vector<string> Subcircuit::getComponentNames() const
{
	vector<string> returnValue;
	for (int i=0; i<comp.size(); ++i)
	{
		returnValue.push_back(comp[i]->getName());
	}
	return returnValue;
}

vector<string> Subcircuit::getSubcircuitNames() const
{
	vector<string> returnValue;
	for (int i=0; i<subcircuit.size(); ++i)
	{
		returnValue.push_back(subcircuit[i]->getName());
	}
	return returnValue;
}

