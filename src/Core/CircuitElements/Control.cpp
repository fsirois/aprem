//-------------------------------------------------------------------------
// Control implementation
//-------------------------------------------------------------------------
#include "Control.h"

using namespace std;

//-------------------------------------------------------------------------
// Construction
//-------------------------------------------------------------------------
Control::Control():Element(ElementType::Control)
{
	subcircuit = NULL;
	on = true;
}

Control::Control(Control* OriginalControl, string name_in):Element(ElementType::Control)
{
	if (name_in.size() != 0)
		name = name_in;
	else
		name = OriginalControl->name;
	
	inputComponents  = OriginalControl->inputComponents;
	outputComponents = OriginalControl->outputComponents;
	inputAttributes  = OriginalControl->inputAttributes;
	outputAttributes = OriginalControl->outputAttributes;
	
	functionData = OriginalControl->functionData;
	functionName = OriginalControl->functionName;
	
	subcircuit = NULL;
	on = true;
}

Control::Control(vector<Component*> inputComponents_in,
				 vector<string> inputAttributes_in,
				 vector<Component*> outputComponents_in,
				 vector<string> outputAttributes_in,
				 vector<double> funtionData_in,
				 string functionName_in,
				 string controlName):Element(ElementType::Control)
{
	inputComponents  = inputComponents_in;
	inputAttributes  = inputAttributes_in;
	outputComponents = outputComponents_in;
	outputAttributes = outputAttributes_in;
	
	functionData = funtionData_in;
	functionName = functionName_in;
	
	name = controlName;
	
	subcircuit = NULL;
	on = true;
}
				 
Control::~Control()
{}

//-------------------------------------------------------------------------
// Set methods
//-------------------------------------------------------------------------
void Control::setOn(bool on_in)
{
	on = on_in;
}

void Control::setName(const string& name_in)
{
	name = name_in;
}

void Control::setFunctionName(string functionName_in)
{
	functionName = functionName_in;
}

void Control::setFunctionData(vector<double> functionData_in)
{
	functionData = functionData_in;
}

void Control::setInputs(vector<Component*> inputComponents_in,
						vector<string> inputAttributes_in)
{
	inputComponents = inputComponents_in;
	inputAttributes = inputAttributes_in;
}

void Control::setOutputs(vector<Component*> outputComponents_in,
						 vector<string> outputAttributes_in)
{
	outputComponents = outputComponents_in;
	outputAttributes = outputAttributes_in;
}

void Control::setInputComponents(vector<Component*> inputComponents_in)
{
	inputComponents = inputComponents_in;
}

void Control::setOutputComponents(vector<Component*> outputComponents_in)
{
	outputComponents = outputComponents_in;
}

void Control::setSubcircuit(Subcircuit * sub)
{
	subcircuit = sub;
}

//-------------------------------------------------------------------------
// Get methods
//-------------------------------------------------------------------------
bool Control::inSubcircuit() const
{
	return (subcircuit != NULL);
}
 
vector<double> Control::getFunctionData() const
{
	return (functionData);
}

pair<vector<string>,vector<string> > Control::getInputs() const
{
	pair<vector<string>,vector<string> > Output;
	Output.second = inputAttributes;
	
	for (int i = 0; i<inputAttributes.size(); ++i)
	{
		Output.first.push_back(inputComponents[i]->getName());
	}
	return (Output);
}

pair<vector<string>,vector<string> > Control::getOutputs() const
{
	pair<vector<string>,vector<string> > Output;
	for (int i = 0; i<outputAttributes.size(); ++i)
	{
		Output.first.push_back(outputComponents[i]->getName());
	}
	Output.second = outputAttributes;
	return (Output);
}

vector<string> Control::getInputComponents() const
{
	vector<string> Output;
	for (int i = 0; i<inputComponents.size(); ++i)
	{
		Output.push_back(inputComponents[i]->getName());
	}
	return (Output);
}

vector<string> Control::getOutputComponents() const
{
	vector<string> Output;
	for (int i = 0; i<outputComponents.size(); ++i)
	{
		Output.push_back(outputComponents[i]->getName());
	}
	return (Output);
}
   
std::vector<Component*> Control::getInComponent() const
{
	return inputComponents;
}

std::vector<Component*> Control::getOutComponent() const
{
	return outputComponents;
}

void Control::updateSlaves()
{
	for (int i=0; i<inputComponents.size(); ++i)
		inputComponents[i]->setControl(this);
	
	for (int i=0; i<outputComponents.size(); ++i)
		outputComponents[i]->setControl(this);
}
	
