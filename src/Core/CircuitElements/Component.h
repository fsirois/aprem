
#pragma once

#include <vector>

#include "Element.h"
#include "Validation.h"

class Control;
class Subcircuit;
class Node;

class Component : public Element
{
public:
	Component();
	Component(const std::string& name);
	Component(const Component& oldComp);

	Component(Component&&) = default;
	Component& operator=(const Component&) = delete;
	Component& operator=(Component&&) = delete;
	virtual ~Component() = default;

	virtual CircuitErrorCode Validate() const = 0;
	virtual bool set(const std::string& field, const double in_r, const double in_i = 0.0);
	virtual std::vector<std::pair<double, double> > get(const std::string & att_name) const;

	// Element implementation
	void setOn(const bool on_in) override final;
	void setName(const std::string& name) override final;

	inline bool isOn() const override final
	{
		return m_isOn;
	}

	inline std::string getName() const override final
	{
		return m_name;
	}

	// set/get methods
	bool setNodes(std::vector<Node*> nodes_in);
	void setIr(double I);
	void setIi(double I);
	void setControl(Control * ctrl);
	void setSubcircuit(Subcircuit * sub);

	const std::vector<Node*>& getNodes() const;
	std::vector<std::string> getNodeNames() const;
	std::pair<double, double> getTensionBetweenNodes(int n1, int n2) const;
	std::pair<double, double> getTensionOfNode(int n) const;
	std::pair<double, double> getCurrent() const;
	std::vector<std::pair<double,double> > getV() const;

	unsigned char isSwitch()   const;
	unsigned char isVoltageSource() const;
	bool inSubcircuit()    const;

	inline double getIr()               const {return m_currentReal;}
	inline double getIi()               const {return m_currentImaginary;}
	inline ComponentType getType()           const {return m_componentType;}

	inline unsigned char isGrounded()   const {return m_isGrounded;}
	inline unsigned char getState()     const {return state;}
	inline const std::vector<Node*>& getNode() const {return m_nodes;}
	inline Control * getControl()       const {return m_controllers;} 
	inline Subcircuit * getSubcircuit() const {return m_subcircuits;}

public:
	static const unsigned int NodeCount = 2u;

protected:
	ComponentType m_componentType;
	double m_currentReal = 0.0;         // current real (primary for Txfo)
	double m_currentImaginary = 0.0;    // current imaginary (primary for Txfo)
	bool m_isOn;

	std::vector<Node*> m_nodes; // Component node
	unsigned char state;     // TODO : Validate the use of this

private:
	unsigned char m_isGrounded;

	Control* m_controllers = nullptr;
	Subcircuit* m_subcircuits = nullptr;

	std::string m_name;
};

class DummyComponent : public Component
{
public:
	DummyComponent() = default;
	DummyComponent(const std::vector<double>& state_in, std::string name_in) {};
	DummyComponent(const DummyComponent&) = default;
	DummyComponent(DummyComponent&&) = default;
	DummyComponent& operator=(DummyComponent&&) = delete;
	DummyComponent& operator=(const DummyComponent&) = delete;

	CircuitErrorCode Validate() const override final
	{
		return CircuitErrorCode();
	};
};
