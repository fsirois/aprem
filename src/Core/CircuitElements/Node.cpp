//-------------------------------------------------------------------------
// Node - Implementation
//-------------------------------------------------------------------------
#include "Node.h"

using namespace std;

// Constructors -----------------------------------------------------------
Node::Node()
{
    on = 1;
    number = 0;
    tempGND = false;
    originalNode = NULL;
}

Node::Node(string name_in)
{
    on = 1;
    number = 0;
    name = name_in;
    tempGND = false;
    originalNode = NULL;
}

Node::Node(Node * OldNode, string new_name)
{
    on = OldNode->on;
    number = 0;
    name = new_name;
    tempGND = false;
    
    originalNode = NULL;
    
    adj_comp = OldNode->adj_comp;  
    adj_nodes = OldNode->adj_nodes;
    adj_pos = OldNode->adj_pos;
}

// Adjacent data methods --------------------------------------------------
void Node::AddAdjComponent(int AdjComp, int AdjPos)
{
    if (find(adj_comp.begin(), adj_comp.end(), AdjComp) == adj_comp.end())
    {
        adj_comp.push_back(AdjComp);
        adj_pos.push_back(AdjPos);
	}
}

void Node::AddAdjNode(int AdjNode)
{
    if (find(adj_nodes.begin(), adj_nodes.end(), AdjNode) == adj_nodes.end() &&
        AdjNode != number)
    {
        adj_nodes.push_back(AdjNode);
	}
}

void Node::ClearAdjacentData()
{
    adj_comp.clear();
    adj_nodes.clear();
    adj_pos.clear();
}

// Set methods ------------------------------------------------------------
void Node::setAsTempGND(bool tempGND_in)
{
    tempGND = tempGND_in;
}

void Node::setVr(double V)
{
    V_r = V;
}

void Node::setVi(double V)
{
    V_i = V;
}

void Node::setNumber(int n)
{
    number = n;
}

void Node::setName(string new_name)
{
    name = new_name;
}

void Node::setOn(unsigned char on_in)
{   
    if (on == 0 && on_in > 0)
        nodeFragments.clear();
    
	on = on_in;   
}


// Get method -------------------------------------------------------------
vector<double> Node::get(string field)
{
    vector<double> Output;
    if (field == "adj_comp")
    {
        for (int i=0; i<adj_comp.size(); ++i)
        {
            Output.push_back(adj_comp[i]);
        }
    }
    else if (field == "adj_pos")
    {
        for (int i=0; i<adj_comp.size(); ++i)
        {
            Output.push_back(adj_pos[i]);
        }
    }
    else if (field == "no")
    {
        Output.push_back(number);
    }
    else
    {
        Output.push_back(0.0);
    }
    return (Output);
}


// Functions linked to node fragmentation ---------------------------------
bool Node::isFragment()
{
    return originalNode != NULL;
}

bool Node::hasFragments()
{
    return !nodeFragments.empty();
}

vector<Node*> Node::getFragments()
{
    return nodeFragments;
}

Node* Node::getOriginalNode()
{
    return originalNode;
}

void Node::setOriginalNode(Node * Orig)
{
    originalNode = Orig;
}

void Node::addFragment(Node * fragment)
{
    nodeFragments.push_back(fragment);
}
















