//-------------------------------------------------------------------------
// Subcircuit declaration
//-------------------------------------------------------------------------
// See if can remove GND and or slackbus

#ifndef SUBCIRCUIT
#define SUBCIRCUIT

#include "Component.h"
#include "Control.h"

class Comp;    // Forward declaration
class Control; // Forward declaration

class Subcircuit : public Element
{
public:
    
    // Constructor
    Subcircuit();
    Subcircuit(std::string name);
    Subcircuit(Subcircuit * subcircuit, std::string new_name);
    ~Subcircuit(){}
    
    // Add/remove items from subcircuit
    std::pair<bool,std::string> addComponent(Component* comp_to_add);
    void addSubcircuit(Subcircuit * subcircuit_in);
    std::pair<bool,std::string> addControl(Control* Control_in);
    bool removeSubcircuit(std::string subcircuit_name);
    bool removeComponent(std::string component_name);
    bool removeControl(std::string control_name);
    
    // Set methods
    void setIONodes(std::vector<Node*> nodes_in);
    void setComponents(std::vector<Component*> newComp);
    void setControls(std::vector<Control*> newCtrl);
    void setSubcircuits(std::vector<Subcircuit*> newSubcircuit);
    void setMaster(Subcircuit * master);
    void setName(const std::string& name_in) override;
    void setOn(const bool on_in) override;

    // Get methods
    bool isEmpty() const;
    bool hasComponents() const;
    bool hasSubcircuit() const;
    bool inSubcircuit() const;
    
    std::vector<Component*> getComponents() const;
    std::vector<Subcircuit*> getSubcircuits() const;
    std::vector<std::string> getComponentNames() const;
    std::vector<std::string> getSubcircuitNames() const;
    std::vector<Control*> getControls() const;

    inline std::string getName() const override {return name;}
    inline Subcircuit * getMaster() const {return master_subcircuit;}
    inline std::vector<Subcircuit*> getSubcircuit() const {return subcircuit;}
    inline bool isOn() const override {return on;}
    
    // Updates components and controls in subcircuit to know their current
    // subcircuit is current subcircuit
    void updateSlaves();
    
private:
    unsigned char GND;      // TODO: Validate
    unsigned char slackBus; // TODO: Validate
    
    std::vector<Component*> comp; // Components specifically in this subcircuit
    std::vector<Node*> IONodes; // Input output nodes
    
    std::vector<Control*> control;
        
    Subcircuit* master_subcircuit;
    std::vector<Subcircuit*> subcircuit;

    std::string name;
    bool on;
};

#endif