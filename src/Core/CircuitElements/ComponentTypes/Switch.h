
#pragma once

#include "Component.h"

class Switch : public Component
{
public:
	Switch();
	Switch(const std::vector<double>& state_in, std::string name_in);
	Switch(const Switch& OldSw);
	Switch(Switch&&) = default;
	Switch& operator=(Switch&&) = delete;
	Switch& operator=(const Switch&) = delete;

	CircuitErrorCode Validate() const override final
	{
		return CircuitErrorCode(); // TODO
	};

	bool set(const std::string& field, const double in_r, const double in_i = 0.0) override final;
	void setState(int state_in);
};

