
#pragma once

#include "Component.h"

class PQLoad : public Component
{
public:
	PQLoad();
	PQLoad(const std::vector<double>& values, std::string name_in);
	PQLoad(const PQLoad& OldPQ);
	PQLoad(PQLoad&&) = default;
	PQLoad& operator=(const PQLoad&) = delete;
	PQLoad& operator=(PQLoad&&) = delete;

	CircuitErrorCode Validate() const override final
	{
		return CircuitErrorCode(); // TODO
	};

	// Set
	bool set(const std::string& field, const double in_r, const double in_i = 0.0) override final;
	void setTempDisable(bool tempDisable_in);

	// Get methods
	inline bool   getTempDisabled() const {return tempDisable;}
	inline double getP()  const {return P;}
	inline double getQ()  const {return Q;}
	inline double getVr() const {return V_r;}
	inline double getVi() const {return V_i;}

private:
	bool tempDisable;

	double P;
	double Q;
	double V_r;
	double V_i;
};
