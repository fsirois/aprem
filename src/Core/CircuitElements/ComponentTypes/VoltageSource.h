
#pragma once

#include "Component.h"

class VoltageSource : public Component
{
public:
	VoltageSource();
	VoltageSource(const std::vector<double>& values, std::string name);
	VoltageSource(const VoltageSource& OldVsrc);
	VoltageSource(VoltageSource&&) = default;
	VoltageSource& operator=(VoltageSource&&) = delete;
	VoltageSource& operator=(const VoltageSource&) = delete;

	CircuitErrorCode Validate() const override final
	{
		return CircuitErrorCode(); // TODO
	};

	bool set(const std::string& field, const double in_r, const double in_i = 0.0) override final;
	std::vector<std::pair<double,double>> get(const std::string& field) const override final;

	inline double getVr()   const {return V_r;}
	inline double getVi()   const {return V_i;}
	inline double getVabs() const {return Vabs;}

private:
	double V_r;
	double V_i;
	double Vabs;
};

