
#include "Transformer.h"

#include "support_functions.h"

using namespace std;

// Constructors -----------------------------------------------------------
Transformer::Transformer()
{
	ratio_r = 1.0;
	ratio_i = 1.0;
	m_componentType = ComponentType::Transformer;
}

Transformer::Transformer(const vector<double>& values, string name_in):Component(name_in)
{
	const double norm = values[0];
	const double angle = values[1];

	ratio_r = norm * cos(circuit_ns::to_radian(angle));
	ratio_i = norm * sin(circuit_ns::to_radian(angle));

	m_componentType = ComponentType::Transformer;
}

Transformer::Transformer(const Transformer& OldTxfo):Component(OldTxfo)
{
	ratio_r = OldTxfo.ratio_r;
	ratio_i = OldTxfo.ratio_i;
	m_componentType = ComponentType::Transformer;
}

// Set --------------------------------------------------------------------
bool Transformer::set(const string& field, const double in_r, const double in_i)
{ 
	if (field == "ratio")
	{
		ratio_r = in_r;
		ratio_i = in_i;
		return (true);
	}
	else if (field == "angle")
	{
		const double Norm = circuit_ns::norm(ratio_r, ratio_i);
		ratio_r = Norm * cos(circuit_ns::to_radian(in_r));
		ratio_i = Norm * sin(circuit_ns::to_radian(in_r));
		return (true);
	}
	else
	{
		return Component::set(field, in_r, in_i);
	}
	return (false);
}

// Get --------------------------------------------------------------------
vector<pair<double,double> > Transformer::get(const string & field) const
{
	vector<pair<double,double> > OutputVector;
	if (field == "I")
	{
		pair<double,double> PrimaryCurrent = getCurrent();
		pair<double,double> SecondaryCurrent = getSecondaryCurrent(PrimaryCurrent);
		OutputVector.push_back(PrimaryCurrent);
		OutputVector.push_back(SecondaryCurrent);
	}
	else if(field == "S")
	{
		pair<double,double> TP1 = getTensionBetweenNodes(0,1);
		pair<double,double> TP2 = getTensionBetweenNodes(2,3);
		pair<double,double> I1 = getCurrent();
		pair<double,double> I2 = getSecondaryCurrent(I1);

		circuit_ns::complex_multiply(TP1.first, TP1.second,
									 I1.first, -I1.second,
									 &TP1.first, &TP1.second);

		circuit_ns::complex_multiply(TP2.first, TP2.second,
									 I2.first, -I2.second,
									 &TP2.first, &TP2.second);

		OutputVector.push_back(TP1);
		OutputVector.push_back(TP2);
	}
	else if (field == "Vb")
	{
		OutputVector.push_back(getTensionBetweenNodes(0,1));
		OutputVector.push_back(getTensionBetweenNodes(2,3));
	}
	else if (field == "angle")
	{
		pair<double,double> angle;
		angle.first = circuit_ns::to_degree(circuit_ns::angle(ratio_r, ratio_i));
		OutputVector.push_back(angle);
	}
	else
	{
		return Component::get(field);
	}
	return (OutputVector);
}

pair<double,double> Transformer::getSecondaryCurrent() const
{
	pair<double,double> PrimaryCurrent = getCurrent();
	return (getSecondaryCurrent(PrimaryCurrent));
}

pair<double,double> Transformer::getSecondaryCurrent(pair<double,double> PrimaryCurrent) const
{
	pair<double,double> SecondaryCurrent;
	circuit_ns::complex_multiply(-PrimaryCurrent.first, -PrimaryCurrent.second,
								 ratio_r, -ratio_i,
								 &SecondaryCurrent.first, &SecondaryCurrent.second);
	return (SecondaryCurrent);
}





