
#include "Impedance.h"

#include "Node.h"
#include "support_functions.h"

using namespace std;

// Constructeurs ----------------------------------------------------------
Impedance::Impedance()
{
	Z_r = 0.0;
	Z_i = 0.0;
	m_componentType = ComponentType::Impedance;
}

Impedance::Impedance(vector<double> values, string name_in):Component(name_in)
{
	circuit_ns::get_complex_number(values, Z_r, Z_i);
	m_componentType = ComponentType::Impedance;
}

Impedance::Impedance(const Impedance& OldImp):Component(OldImp)
{
	Z_r = OldImp.Z_r;
	Z_i = OldImp.Z_i;
	m_componentType = ComponentType::Impedance;
}

// Set --------------------------------------------------------------------
bool Impedance::set(const string& field, const double in_r, const double in_i)
{ 
	if (field == "Z")
	{
		Z_r = in_r;
		Z_i = in_i;
		return true;
	}
	else
	{
		return Component::set(field, in_r, in_i);
	}

	return false;
}

// Get --------------------------------------------------------------------
vector<pair<double,double>> Impedance::get(const string & att_name) const
{
	pair<double,double> out;
	out.first = 0.0;
	out.second = 0.0;
	
	//calculateCurrent();
	
	if (!att_name.compare("Z"))
	{
		out.first = Z_r;
		out.second = Z_i;
	}
	else if (att_name == "R")
	{
		out.first = Z_r;
		out.second = 0.0;
	}
	else if (att_name == "X")
	{
		out.first = Z_i;
		out.second = 0.0;
	}
	else
	{
		return Component::get(att_name);
	}

	vector<pair<double,double> > OutputVector;
	OutputVector.push_back(out);
	return (OutputVector);
}

CircuitErrorCode Impedance::Validate() const
{
	if (abs(Z_r) <= std::numeric_limits<double>::epsilon() &&
		abs(Z_i) <= std::numeric_limits<double>::epsilon())
	{
		char buffer[50];
		sprintf(buffer, "Impedance value cannot be 0. Z = %f + j%f", Z_r, Z_i);
		return CircuitErrorCode(buffer);
	}

	return CircuitErrorCode();
}

// Others -----------------------------------------------------------------
void Impedance::calculateCurrent()
{
	double dV_r, dV_i;

	dV_r = m_nodes[0]->getVr() - m_nodes[1]->getVr();
	dV_i = m_nodes[0]->getVi() - m_nodes[1]->getVi();

	circuit_ns::complex_division(dV_r, dV_i, Z_r, Z_i, &m_currentReal, &m_currentImaginary);
}





