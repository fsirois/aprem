
#pragma once

#include "Component.h"

class Transformer : public Component
{
public:
	Transformer();
	Transformer(const std::vector<double>& values, std::string name);
	Transformer(const Transformer& OldTxfo);
	Transformer(Transformer&&) = default;
	Transformer& operator=(Transformer&&) = delete;
	Transformer& operator=(const Transformer&) = delete;

	CircuitErrorCode Validate() const override final
	{
		return CircuitErrorCode(); // TODO
	};

	bool set(const std::string& field, const double in_r, const double in_i = 0.0) override final;

	std::vector<std::pair<double,double>> get(const std::string& field) const override final;
	std::pair<double,double> getSecondaryCurrent() const;
	std::pair<double,double> getSecondaryCurrent(std::pair<double,double> I1) const;

	inline double getRatior() const {return ratio_r;}
	inline double getRatioi() const {return ratio_i;}

	static const unsigned int NodeCount = 4u;

private:
	double ratio_r;
	double ratio_i;
};

