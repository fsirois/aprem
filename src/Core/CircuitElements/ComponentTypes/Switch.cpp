
#include "Switch.h"

using namespace std;

// Constructor ------------------------------------------------------------
Switch::Switch()
{
	m_componentType = ComponentType::Switch;
}

Switch::Switch(const std::vector<double>& state_in, string name_in):Component(name_in)
{
	state = (int) state_in[0];
	m_componentType = ComponentType::Switch;
}

Switch::Switch(const Switch& OldSw):Component(OldSw)
{
	m_componentType = ComponentType::Switch;
}

// Set functions ----------------------------------------------------------
bool Switch::set(const string& field, const double in_r, const double in_i)
{
	return Component::set(field, in_r, in_i);
}

void Switch::setState(int state_in)
{
	state = state_in;
}

