
#include "VoltageSource.h"

#include "support_functions.h"

using namespace std;

// Constructors -----------------------------------------------------------
VoltageSource::VoltageSource()
{
	V_r = 0.0;
	V_i = 0.0;
	Vabs = 0.0;
	m_componentType = ComponentType::VoltageSource;
}

VoltageSource::VoltageSource(const vector<double>& values, string name_in):Component(name_in)
{
	Vabs = values[0];
	const double angle = values[1];

	V_r = Vabs * cos(circuit_ns::to_radian(angle));
	V_i = Vabs * sin(circuit_ns::to_radian(angle));

	m_componentType = ComponentType::VoltageSource;
}

VoltageSource::VoltageSource(const VoltageSource& OldVsrc):Component(OldVsrc)
{
	V_r = OldVsrc.V_r;
	V_i = OldVsrc.V_i;
	Vabs = OldVsrc.Vabs;
	m_componentType = ComponentType::VoltageSource;
}

// Set --------------------------------------------------------------------
bool VoltageSource::set(const string& field, const double in_r, const double in_i)
{ 
	if (field == "V")
	{
		V_r = in_r;
		V_i = in_i;
		Vabs = circuit_ns::norm(V_r, V_i);
		return (true);
	}
	else if (field == "angle")
	{
		V_r = Vabs * cos(circuit_ns::to_radian(in_r));
		V_i = Vabs * sin(circuit_ns::to_radian(in_r));
		return (true);
	}
	else if (field == "Vabs")
	{
		double OldV_r = V_r;
		double OldV_i = V_i;
		V_r = in_r * cos(circuit_ns::angle(OldV_r,OldV_i));
		V_i = in_r * sin(circuit_ns::angle(OldV_r,OldV_i));
		return (true);
	}
	else
	{
		return Component::set(field, in_r, in_i);
	}

	return (false);
}

// Get --------------------------------------------------------------------
vector<pair<double,double> > VoltageSource::get(const string & att_name) const
{
	pair<double, double> output;
	output.first = 0.0;
	output.second = 0.0;
	if (att_name == "Vabs")
	{
		output.first = Vabs;
	}
	else if (att_name == "angle")
	{
		output.first = circuit_ns::to_degree(circuit_ns::angle(V_r,V_i));
	}
	else if (att_name == "Vs")
	{
		output.first = V_r;
		output.second = V_i;
	}
	else if (att_name == "S_in")
	{
		output = getTensionOfNode(1);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else if (att_name == "S_out")
	{
		output = getTensionOfNode(0);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else if (att_name == "S")
	{
		output = getTensionBetweenNodes(0,1);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else
	{
		return Component::get(att_name);
	}
	
	vector<pair<double,double> > OutputVector;
	OutputVector.push_back(output);
	return OutputVector;
}

