
#include "PVSource.h"

#include "support_functions.h"

using namespace std;

// Constructors -----------------------------------------------------------
PVSource::PVSource()
{
	P = 0.0;
	V_r = 0.0;
	V_i = 0.0;
	Vabs = 0.0;
	useAsVsrc = 0;
	m_componentType = ComponentType::PVSource;
}

PVSource::PVSource(const vector<double>& values, string name_in):Component(name_in)
{
	P = values[0];
	V_r = values[1];
	V_i = values.size() > 1 ? values[2] : 0.0;
	Vabs = circuit_ns::norm(V_r, V_i);
	useAsVsrc = 0;
	m_componentType = ComponentType::PVSource;
}

PVSource::PVSource(const PVSource& OldPV):Component(OldPV)
{
	P = OldPV.P;
	Vabs = OldPV.Vabs;
	V_r = OldPV.V_r;
	V_i = OldPV.V_i;

	useAsVsrc = OldPV.useAsVsrc;
	m_componentType = ComponentType::PVSource;
}

// Set --------------------------------------------------------------------
bool PVSource::set(const string& field, const double in_r, const double in_i)
{
	if (field == "V")
	{
		V_r = in_r;
		V_i = in_i;
		Vabs = circuit_ns::norm(V_r, V_i);
		return (true);
	}
	else if (field == "Vabs")
	{
		double OldV_r = V_r;
		double OldV_i = V_i;
		V_r = in_r * cos(circuit_ns::angle(OldV_r,OldV_i));
		V_i = in_r * sin(circuit_ns::angle(OldV_r,OldV_i));
		return (true);
	}
	else if(field == "P")
	{
		P = in_r;
		return (true);
	}
	else
	{
		return Component::set(field, in_r, in_i);
	}
	return (false);
}

void PVSource::setUseAsVoltageSource(bool useAsVsrc_in)
{
	useAsVsrc = static_cast<int> (useAsVsrc_in);
}

// Get --------------------------------------------------------------------
vector<pair<double, double> > PVSource::get(const string & att_name) const
{
	pair<double, double> output;
	output.first = 0.0;
	output.second = 0.0;
	if (att_name == "Vabs")
	{  
		output.first = Vabs;
	}
	else if (att_name == "angle")
	{
		output.first = circuit_ns::to_degree(circuit_ns::angle(V_r, V_i));
	}
	else if (att_name == "P")
	{
		output.first = P;
	}
	else if (att_name == "Vs")
	{
		output.first = V_r;
		output.second = V_i;
	}
	else if (att_name == "S_in")
	{
		output = getTensionOfNode(1);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else if (att_name == "S_out")
	{
		output = getTensionOfNode(0);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else if (att_name == "S")
	{
		output = getTensionBetweenNodes(1,0);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else
	{
		return Component::get(att_name);
	}
	
	std::vector<std::pair<double,double> > OutputVector;
	OutputVector.push_back(output);
	return (OutputVector);
}


