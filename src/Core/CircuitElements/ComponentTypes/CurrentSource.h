
#pragma once

#include "Component.h"

class CurrentSource : public Component
{
public:
	CurrentSource();
	CurrentSource(const std::vector<double>& values, std::string name_in);
	CurrentSource(const CurrentSource& OldIsrc);
	CurrentSource(CurrentSource&& OldIsrc) = default;
	CurrentSource& operator=(CurrentSource&&) = delete;
	CurrentSource& operator=(const CurrentSource&) = delete;

	CircuitErrorCode Validate() const override final
	{
		return CircuitErrorCode(); // TODO
	};

	bool set(const std::string& field, const double in_r, const double in_i = 0.0) override final;
	std::vector<std::pair<double,double> > get(const std::string & field) const override final;

private:
	double Iabs;
};
