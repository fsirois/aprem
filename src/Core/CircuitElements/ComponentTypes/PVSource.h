
#pragma once

#include "Component.h"

class PVSource : public Component
{
public:
	PVSource();
	PVSource(const std::vector<double>& values, std::string name_in);
	PVSource(const PVSource& OldPV);
	PVSource(PVSource&&) = default;
	PVSource& operator=(const PVSource&) = delete;
	PVSource& operator=(PVSource&&) = delete;

	CircuitErrorCode Validate() const override final
	{
		return CircuitErrorCode(); // TODO
	};

	bool set(const std::string& field, const double in_r, const double in_i = 0.0) override final;
	void setUseAsVoltageSource(bool useAsVsrc_in);

	std::vector<std::pair<double,double> > get(const std::string & input) const override final;

	inline double getP()         const {return P;}
	inline double getVabs()      const {return Vabs;}
	inline double getVr()        const {return V_r;}
	inline double getVi()        const {return V_i;}
	inline int    isUsedAsVoltageSource() const {return useAsVsrc;}

private:
	int useAsVsrc;

	double P;
	double Vabs;
	double V_r;
	double V_i;
};

