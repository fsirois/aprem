
#include "PQLoad.h"

#include "support_functions.h"

using namespace std;

// Constructors -----------------------------------------------------------
PQLoad::PQLoad()
{
	P = 0.0;
	Q = 0.0;
	V_r = 0.0;
	V_i = 0.0;

	tempDisable = false;

	m_componentType = ComponentType::PQLoad;
}

PQLoad::PQLoad(const vector<double>& values, string name_in) :Component(name_in)
{
	P = values[0];
	Q = values[1];
	V_r = values[2];
	V_i = values.size() > 2 ? values[3] : 0.0;

	tempDisable = false; 

	m_componentType = ComponentType::PQLoad;
}

PQLoad::PQLoad(const PQLoad& OldPQ):Component(OldPQ)
{
	P = OldPQ.P;
	Q = OldPQ.Q;
	V_r = OldPQ.V_r;
	V_i = OldPQ.V_i;

	tempDisable = false;
	m_componentType = ComponentType::PQLoad;
}

// Set functions ----------------------------------------------------------
void PQLoad::setTempDisable(bool tempDisable_in)
{
	tempDisable = tempDisable_in;
}

bool PQLoad::set(const string& field, const double in_r, const double in_i)
{ 
	if (field == "P")
	{
		P = in_r;
		return (true);
	}
	else if (field == "Q")
	{
		Q = in_r;
		return (true);
	}
	else if (field == "V")
	{
		V_r = in_r;
		V_i = in_i;
		return (true);
	}
	else
	{
		return Component::set(field, in_r, in_i);
	}
	return (false);
}


