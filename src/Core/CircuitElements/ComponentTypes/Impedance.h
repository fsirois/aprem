
#pragma once

#include "Component.h"

class Impedance : public Component
{
public:
	Impedance();
	Impedance(std::vector<double> values, std::string name);
	Impedance(const Impedance& OldImp);
	Impedance(Impedance&&) = default;
	Impedance& operator=(const Impedance&) = delete;
	Impedance& operator=(Impedance&&) = delete;

	CircuitErrorCode Validate() const override final;

	bool set(const std::string& field, const double in_r, const double in_i = 0.0) override final;
	std::vector<std::pair<double,double>> get(const std::string & field) const override final;
	void calculateCurrent();

	inline double getZr() const {return Z_r;}
	inline double getZi() const {return Z_i;}

private:
	double Z_r;
	double Z_i;
};
