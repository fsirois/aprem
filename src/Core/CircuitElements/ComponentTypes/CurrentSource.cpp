
#include "CurrentSource.h"

#include "support_functions.h"

using namespace std;

// Constructors -----------------------------------------------------------
CurrentSource::CurrentSource()
{
	Iabs = 0.0;
	m_componentType = ComponentType::CurrentSource;
}

CurrentSource::CurrentSource(const vector<double>& values, string name_in):Component(name_in)
{
	Iabs = values[0];
	const double angle = values[1];

	m_currentReal = Iabs * cos(circuit_ns::to_radian(angle));
	m_currentImaginary = Iabs * sin(circuit_ns::to_radian(angle));

	m_componentType = ComponentType::CurrentSource;
}

CurrentSource::CurrentSource(const CurrentSource& OldIsrc):Component(OldIsrc)
{
	m_componentType = ComponentType::CurrentSource;
}

// Set --------------------------------------------------------------------
bool CurrentSource::set(const string& field, const double in_r, const double in_i)
{ 
	if (field == "I")
	{
		circuit_ns::get_complex_number(&in_r, &in_i, &m_currentReal, &m_currentImaginary);
		return (true);
	}
	else if (field == "angle")
	{
		m_currentReal = Iabs * cos(circuit_ns::to_radian(in_r));
		m_currentImaginary = Iabs * sin(circuit_ns::to_radian(in_r));
		return (true);
	}
	else if (field == "Iabs")
	{
		Iabs = in_r;
		double OldI_r = m_currentReal;
		double OldI_i = m_currentImaginary;
		m_currentReal = Iabs * cos(circuit_ns::angle(OldI_r,OldI_i));
		m_currentImaginary = Iabs * sin(circuit_ns::angle(OldI_r,OldI_i));
		return (true);
	}
	else
	{
		return Component::set(field, in_r, in_i);
	}

	return (false);
}

// Get --------------------------------------------------------------------
vector<pair<double, double> > CurrentSource::get(const string & field) const
{
	pair<double, double> output;
	output.first = 0.0;
	output.second = 0.0;
	
	if (field == "I")
	{
		output = getCurrent();
		output.first *= -1;
		output.second *= -1;
	}
	else if (field == "Is")
	{
		output = getCurrent();
	}
	else if (field == "Iabs")
	{
		if (m_isOn)
		{
			output.first = circuit_ns::norm(m_currentReal, m_currentImaginary);
		}
	}
	else if (field == "Inom")
	{
		output.first = circuit_ns::norm(m_currentReal, m_currentImaginary);
	}
	else if (field == "angle")
	{
		output.first = circuit_ns::to_degree(circuit_ns::angle(m_currentReal, m_currentImaginary));
	}
	else if (field == "S_in")
	{
		output = getTensionOfNode(1);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else if (field == "S_out")
	{
		output = getTensionOfNode(0);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else if (field == "S")
	{
		output = getTensionBetweenNodes(1,0);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else
	{
		return Component::get(field);
	}
	
	vector<pair<double,double> > OutputVector;
	OutputVector.push_back(output);
	return (OutputVector);
}





