//-------------------------------------------------------------------------
// Control declaration
//-------------------------------------------------------------------------

#ifndef CONTROL_HEADER
#define CONTROL_HEADER

#include <utility>

#include "Component.h"

class Comp;       // Forward declaration
class Subcircuit; // Forward declaration

class Control : public Element
{
public:
    
    // Constructor methods
    Control();
    Control(Control * OriginalControl, std::string name = "");
    Control(std::vector<Component*> inputComponents,
            std::vector<std::string> inputAttributes,
            std::vector<Component*> outputComponents,
            std::vector<std::string> outputAttributes,
            std::vector<double> functionData,
            std::string functionName,
            std::string controlName);
    ~Control();
    
    // Set methods
    void setOn(const bool on_in) override;
    void setFunctionName(std::string functionName_in);
    void setFunctionData(std::vector<double> functionData_in);
    void setInputs(std::vector<Component*> inputComponents_in,
                   std::vector<std::string> inputAttributes_in);
    void setOutputs(std::vector<Component*> outputComponents_in,
                    std::vector<std::string> outputAttributes_in);
    void setInputComponents(std::vector<Component*> inputComponents_in);
    void setOutputComponents(std::vector<Component*> outputComponents_in);
    void setName(const std::string& name_in) override;
    void setSubcircuit(Subcircuit * sub);
    
    // Get methods
    bool inSubcircuit()    const;
    
    std::vector<double>      getFunctionData() const;
    std::vector<std::string> getInputComponents() const ;
    std::vector<std::string> getOutputComponents() const;
    std::pair<std::vector<std::string>,std::vector<std::string> > getInputs() const;
    std::pair<std::vector<std::string>,std::vector<std::string> > getOutputs() const;
    
    std::vector<Component*> getInComponent() const;
    std::vector<Component*> getOutComponent() const;
    
    inline bool isOn() const override {return on;}
    inline std::string getName() const override {return name;}
    inline std::string getFunctionName() const {return functionName;}
    inline Subcircuit * getSubcircuit() const {return subcircuit;}
   
    // Update components (input and output) for they know they are
    // controlled by current control.
    void updateSlaves();
    
private:
    
    // Basic information
    bool on;
    std::string name;         // Control name
    std::string functionName; // Matlab function file name
    
    // Detailed control data
    std::vector<double> functionData;
    std::vector<Component*> inputComponents;
    std::vector<Component*> outputComponents;
    std::vector<std::string> inputAttributes;
    std::vector<std::string> outputAttributes;
    
    Subcircuit * subcircuit; // Control's subcircuit
};

#endif /* CONTROL_HEADER */



