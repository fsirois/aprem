
#include "Component.h"
#include <stdio.h>

#include "Node.h"
#include "Element.h"
#include "Control.h"
#include "Subcircuit.h"
using namespace std;

//-------------------------------------------------------------------------
// Constructors
//-------------------------------------------------------------------------
Component::Component():Element(ElementType::Component)
{
	m_isOn = 1;
	state = 1;
	m_isGrounded = 0;
	m_controllers = nullptr;
	m_subcircuits = nullptr;
}

Component::Component(const string& name_in):Element(ElementType::Component)
{
	m_isOn = 1;
	state = 1;
	m_isGrounded = 0;
	m_controllers = nullptr;
	m_subcircuits = nullptr;
	m_name = name_in;
}

Component::Component(const Component& OldComp):Element(ElementType::Component)
{
	m_isOn = OldComp.m_isOn;
	state = OldComp.state;
	m_isGrounded = OldComp.m_isGrounded;

	m_componentType = OldComp.m_componentType;
	m_name = OldComp.m_name;

	m_controllers = nullptr;
	m_subcircuits = nullptr;
}


//-------------------------------------------------------------------------
// Master get/set functions 
//-------------------------------------------------------------------------
bool Component::set(const std::string& field, const double in_r, const double in_i)
{
	if (field == "on")
	{
		if ((int) in_r > 0.0)
			setOn(true);
		else
			setOn(false);
		return true;
	}
	return false;
}

vector<pair<double, double> > Component::get(const string & att_name) const
{
	pair<double, double> output;
	if (!att_name.compare("I"))
	{
		output = getCurrent();
	}
	else if (!att_name.compare("Vb"))
	{
		output = getTensionBetweenNodes(0,1);
	}
	else if (!att_name.compare("Va"))
	{
		output = getTensionBetweenNodes(1,0);
	}
	else if (att_name == "S_in")
	{
		output = getTensionOfNode(0);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else if (att_name == "S_out")
	{
		output = getTensionOfNode(1);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else if (att_name == "S")
	{
		output = getTensionBetweenNodes(0,1);
		circuit_ns::complex_multiply(output.first, output.second,
									 m_currentReal, -m_currentImaginary,
									 &output.first, &output.second);
	}
	else
	{
		output = make_pair(0.0, 0.0);
	}
	
	vector<pair<double,double> > OutputVector;
	OutputVector.push_back(output);
	return (OutputVector);
}


//-------------------------------------------------------------------------
// Set methods
//-------------------------------------------------------------------------
void Component::setIr(double I)
{
	m_currentReal = I;
}

void Component::setIi(double I)
{
	m_currentImaginary = I;
}

void Component::setName(const string& new_name)
{
	m_name = new_name;
}

void Component::setOn(const bool onInput)
{
	m_isOn = onInput;
}

bool Component::setNodes(vector<Node*> nodes_in)
{
	if (m_nodes.size() <= nodes_in.size())
	{
		m_nodes.resize(nodes_in.size());
	}

	for (int i=0; i<nodes_in.size(); i++)
	{
		if (nodes_in[i] == nullptr)
		{
			m_isGrounded = 1;
		}

		m_nodes[i] = nodes_in[i];
	}

	return (true);
}

void Component::setControl(Control * ctrl)
{
	m_controllers = ctrl;
}

void Component::setSubcircuit(Subcircuit * sub)
{
	m_subcircuits = sub;
}


//-------------------------------------------------------------------------
// Get methods
//-------------------------------------------------------------------------
unsigned char Component::isSwitch()   const
{
	bool isSwitch = m_componentType == ComponentType::Switch;
	return (static_cast<unsigned char> (isSwitch));
}

unsigned char Component::isVoltageSource() const
{
	bool isVoltageSource = m_componentType == ComponentType::VoltageSource;
	return (static_cast<unsigned char> (isVoltageSource));
}

bool Component::inSubcircuit() const
{
	return (m_subcircuits != nullptr);
}

const vector<Node*>& Component::getNodes() const
{
	return m_nodes;
}

vector<string> Component::getNodeNames() const
{
	vector<string> out;
	for (int i = 0; i < m_nodes.size(); i++)
	{
		out.push_back(m_nodes[i]->getName());
	}

	return (out);
}

pair<double, double> Component::getTensionOfNode(int n) const
{
	pair<double, double> output;
	output.first = 0.0;
	output.second = 0.0;

	if (n > m_nodes.size())
	{
		return output;
	}

	if (m_nodes[n] != nullptr)
	{
		output.first = m_nodes[n]->getVr();
		output.second = m_nodes[n]->getVi();
	}

	return output;
}

pair<double, double> Component::getTensionBetweenNodes(int n1, int n2) const
{
	pair<double, double> output = getTensionOfNode(n1);
	pair<double, double> tmp = getTensionOfNode(n2);

	output.first -= tmp.first;
	output.second -= tmp.second;

	return output;
}

pair<double, double> Component::getCurrent() const
{
	pair <double, double> out;
	out.first = 0.0;
	out.second = 0.0;
	if (m_isOn)
	{
		out.first = m_currentReal;
		out.second = m_currentImaginary;
	}
	return (out);
}

vector<pair<double,double> > Component::getV() const
{
	vector<pair<double,double> > OutputVector;
	for (int i=0; i<m_nodes.size(); i++)
	{
		pair<double,double> V;
		V.first = m_nodes[i]->getVr();
		V.second = m_nodes[i]->getVi();
		OutputVector.push_back(V);
	}
	return (OutputVector);
}






