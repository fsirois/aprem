//-------------------------------------------------------------------------
// Node - Declaration
//-------------------------------------------------------------------------
#ifndef CIRCUIT_NODE_H
#define CIRCUIT_NODE_H

#include "support_functions.h"

class Node
{
public:
    Node();
    Node(std::string name_in);
    Node(Node * OldNode, std::string name_in);
    
    // Neighbor's methods
    void AddAdjComponent(int AdjComp, int AdjPos);
    void AddAdjNode(int AdjNode);
    void ClearAdjacentData();
    
    // Setter
    void setAsTempGND(bool tempGND_in);
    void setOn(unsigned char on_in);
    void setVr(double V);
    void setVi(double V);
    void setNumber(int n);
    void setName(std::string new_name);
    
    // Get methods
    std::vector<double> get(std::string field);
    inline std::string      getName()    const {return name;}
    inline double           getVr()      const {return V_r;}
    inline double           getVi()      const {return V_i;}
    inline int              getNumber()  const {return number;}
    inline unsigned char    getOn()      const {return on;}
    inline std::vector<int> getAdjComponent() const {return adj_comp;}
    inline std::vector<int> getAdjPos()  const {return adj_pos;}
    inline std::vector<int> getAdjNode() const {return adj_nodes;}
    
    // Functions linked to node fragmentation
    void setOriginalNode(Node * Orig);
    void addFragment(Node * fragment);
    
    bool isFragment();
    bool hasFragments();
    std::vector<Node*> getFragments();
    Node* getOriginalNode();
    
private:  
    
    // General information on nodes
    unsigned char on;
    int number;
    std::string name;
    bool tempGND;
    
    // Node voltage
    double V_r;
    double V_i;
    
    // Neighbor's information
    std::vector<int> adj_comp;  // Index of adjacent components
    std::vector<int> adj_nodes; // Number of adjacents nodes
    std::vector<int> adj_pos;
    
    // Fragments
    std::vector<Node*> nodeFragments;
    Node * originalNode;

};

#endif

