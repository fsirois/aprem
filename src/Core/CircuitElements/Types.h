
#pragma once

#include <string>
#include <map>

enum class ComponentType : unsigned short
{
	Impedance,
	CurrentSource,
	PVSource,
	PQLoad,
	VoltageSource,
	Transformer,
	Switch,
	Invalid
};

enum class ElementType
{
	Component,
	Control,
	Subcircuit,
	Invalid
};

std::string SetProperty(ComponentType componentType, int index);

struct InputTypeCounts
{
	InputTypeCounts() = default;

	InputTypeCounts(const unsigned char real, const unsigned char angle, const unsigned char complex) :
		nReal(real),
		nAngle(angle),
		nComplex(complex)
	{
	}

	unsigned char nReal = 0u;
	unsigned char nAngle = 0u;
	unsigned char nComplex = 0u;
};

static std::map<ComponentType, InputTypeCounts> InputCountsMapping =
{
	{ ComponentType::Switch,			InputTypeCounts{ 1u, 0u, 0u }},
	{ ComponentType::Impedance,		InputTypeCounts{0u, 0u, 1u} },
	{ ComponentType::CurrentSource,	InputTypeCounts{1u, 1u, 0u} },
	{ ComponentType::Transformer,	InputTypeCounts{1u, 1u, 0u} },
	{ ComponentType::VoltageSource,	InputTypeCounts{1u, 1u, 0u} },
	{ ComponentType::PVSource,		InputTypeCounts{1u, 0u, 1u} },
	{ ComponentType::PQLoad,		InputTypeCounts{2u, 0u, 1u} },
};

namespace circuit_ns
{
	ComponentType CharToComponentType(const char first, const char second);

	std::string ComponentTypeToString(const ComponentType Type);
	ComponentType StringToComponentType(const std::string& type_name);

	std::string ElementTypeToString(const ElementType Type);
	ElementType StringToElementType(const std::string& type_name);

	unsigned char NodeCount(const ComponentType type);
}