
#pragma once

#include "Types.h"

#include <string>

class Element
{
public:
	Element(const ElementType type)
	{
		m_elementType = type;
	}

	inline ElementType GetElementType()
	{
		return m_elementType;
	}

	virtual ~Element() = default;
	virtual std::string getName() const = 0;
	virtual void setName(const std::string& newName) = 0;
	virtual bool isOn() const = 0;
	virtual void setOn(const bool isOn) = 0;

private:
	ElementType m_elementType;
};
