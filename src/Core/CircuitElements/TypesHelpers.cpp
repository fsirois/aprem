
#include "Types.h"
#include "support_functions.h"
#include "ComponentManager.h"

std::string SetProperty(ComponentType componentType, int index)
{
	switch (componentType)
	{
	case ComponentType::Impedance:
		return "Z";
	case ComponentType::Switch:
		return "on";
	case ComponentType::CurrentSource:
		switch (index)
		{
		case 0:
			return "Iabs";
		case 1:
			return "angle";
		}
	case ComponentType::VoltageSource:
		switch (index)
		{
		case 0:
			return "Vabs";
		case 1:
			return "angle";
		}
	case ComponentType::Transformer:
		switch (index)
		{
		case 0:
			return "ratio";
		case 1:
			return "angle";
		}
	case ComponentType::PVSource:
		switch (index)
		{
		case 0:
			return "P";
		case 1:
			return "V";
		}
	case ComponentType::PQLoad:
		switch (index)
		{
		case 0:
			return "P";
		case 1:
			return "Q";
		case 2:
			return "V";
		}
	default:
		return "";
	}
}

namespace circuit_ns
{
	std::string ComponentTypeToString(const ComponentType Type)
	{
		switch (Type)
		{
		case ComponentType::Impedance:		return("Z");
		case ComponentType::CurrentSource:	return("I");
		case ComponentType::VoltageSource:	return("V");
		case ComponentType::PQLoad:			return("PQ");
		case ComponentType::PVSource:		return("PV");
		case ComponentType::Transformer:	return("T");
		case ComponentType::Switch:			return("S");
		default:	return ("");
		}
	}

	ComponentType StringToComponentType(const std::string& type_name)
	{
		if (type_name == "Imp" || type_name == "Z") return ComponentType::Impedance;
		if (type_name == "Isrc" || type_name == "I") return ComponentType::CurrentSource;
		if (type_name == "Vsrc" || type_name == "V") return ComponentType::VoltageSource;
		if (type_name == "Txfo" || type_name == "T") return ComponentType::Transformer;
		if (type_name == "Sw" || type_name == "S") return ComponentType::Switch;
		if (type_name == "PQ") return ComponentType::PQLoad;
		if (type_name == "PV") return ComponentType::PVSource;
		return ComponentType::Invalid;
	}

	std::string ElementTypeToString(const ElementType Type)
	{
		switch (Type)
		{
		case ElementType::Component:  return("Comp");
		case ElementType::Control:    return("Control");
		case ElementType::Subcircuit: return("Subcircuit");
		default: return ("");
		}
	}

	ElementType StringToElementType(const std::string& type_name)
	{
		if (type_name == "Comp")		return ElementType::Component;
		if (type_name == "Control")		return ElementType::Control;
		if (type_name == "Subcircuit")	return ElementType::Subcircuit;
		return ElementType::Invalid;
	}

	ComponentType CharToComponentType(const char first, const char second)
	{
		switch (first)
		{
		case 'R':
			return ComponentType::Impedance;
		case 'I':
			return ComponentType::CurrentSource;
		case 'V':
			return ComponentType::VoltageSource;
		case 'P':
			switch (second)
			{
			case 'Q':
				return ComponentType::PQLoad;
			case 'V':
				return ComponentType::PVSource;
			default:
				return ComponentType::Invalid;
			}
		case 'T':
			return ComponentType::Transformer;
		case 'S':
			return ComponentType::Switch;
		}

		return ComponentType::Invalid;
	}

	unsigned char NodeCount(const ComponentType type)
	{
		#define GET_NODE_COUNT_MACRO(N) case N: return ComponentTypeSelector<static_cast<ComponentType>(N)>::NodeCount;

		const unsigned int comp_index = static_cast<unsigned int>(type);
		switch (comp_index)
		{
			REPEAT(20, GET_NODE_COUNT_MACRO)
		}

		return 2;
	}
}

