
#include "circuitNameHandler.h"

#include "Node.h"
#include "support_functions.h"

static constexpr unsigned int kMAX_ITER_NAME = 10000;

using namespace std;

int charToInt(char char_in)
{
	switch (char_in)
	{
	case 'a':
	case 'A':
		return 0;
	case 'b':
	case 'B':
		return 1;
	case 'c':
	case 'C':
		return 2;
	case 'd':
	case 'D':
		return 3;
	default:
		return -1;
	}
}

//--------------------------------------------------
circuitNameHandler::circuitNameHandler()
{
	nAutoNodes = 0;
	nAutoControl = 0;
	nAutoSubcircuit = 0;
}

circuitNameHandler::circuitNameHandler(const vector<Node*>* node_vector_ptr,
									   const ComponentManager* comp_vector_ptr,
									   const vector<Control*>* control_vector_ptr,
									   const vector<Subcircuit*>* subcircuit_vector_ptr,
									   const map<string, Component*>* comp_map_ptr)
{
	nAutoNodes = 0;
	nAutoControl = 0;
	nAutoSubcircuit = 0;

	node = node_vector_ptr;
	componentManager = comp_vector_ptr;
	comp_dict = comp_map_ptr;
	control = control_vector_ptr;
	subcircuit = subcircuit_vector_ptr;
}

//--------------------------------------------------
bool circuitNameHandler::nodeExist(const string& node_name) const
{
	return (circuit_ns::contains(*node, node_name));
}

bool circuitNameHandler::componentExist(const string& comp_name) const
{
	return (comp_dict->find(comp_name) != comp_dict->end());
}

bool circuitNameHandler::controlExist(const string& control_name) const
{
	return (circuit_ns::contains(*control, control_name));
}

bool circuitNameHandler::subcircuitExist(const string& subcircuit_name) const
{
	return (circuit_ns::contains(*subcircuit, subcircuit_name));
}

bool circuitNameHandler::elementExist(const string& elem_name) const
{
	return(componentExist(elem_name) || controlExist(elem_name) || subcircuitExist(elem_name));
}

//--------------------------------------------------
string circuitNameHandler::correctName(string input_name, int Index) const
{
	string temp_name = input_name+"_"+circuit_ns::IntToString(Index);

	// Increase the number after the name until the name is not used
	bool nameIsIncorrect = elementExist(temp_name);
	while (nameIsIncorrect && Index < kMAX_ITER_NAME )
	{
		temp_name = input_name+"_"+circuit_ns::IntToString(Index++);
		nameIsIncorrect = elementExist(temp_name);
	 } 
	return (temp_name);
}

string circuitNameHandler::correctNodeName(string input_name, int Index) const
{
	string temp_node = input_name+"_"+circuit_ns::IntToString(Index);
	
	// Increase the number after the name until the name is not used
	bool nameIsIncorrect = nodeExist(temp_node);
	while ( nameIsIncorrect && Index < kMAX_ITER_NAME )
	{
		temp_node = input_name+"_"+circuit_ns::IntToString(Index++);
		nameIsIncorrect = nodeExist(temp_node);
	}
	return (temp_node);
}

//--------------------------------------------------
string circuitNameHandler::ValidateName(const string& name, const ElementType elem_type, const ComponentType type)
{
	string outputName = name;

	if (outputName == "XX")
	{
		if (elem_type == ElementType::Component)
		{
			outputName = circuit_ns::ComponentTypeToString(type);
			outputName = correctName(outputName, nextAutoComponent(type));
		}
		else if (elem_type == ElementType::Control || elem_type == ElementType::Subcircuit)
		{
			outputName = circuit_ns::ElementTypeToString(elem_type);
			outputName = correctName(outputName, nextAutoElem(elem_type));
		}
		else
		{
			outputName = "Elem";
			outputName = correctName(outputName, ++nAutoElement);
		}
	}
	else if (elementExist(outputName))
	{
		outputName = correctName(outputName);
	}

	return outputName;
}

string circuitNameHandler::ValidateNodeName(const string& name)
{
	// 1 - Node with no name, name must be chosen automatically
	if (name.empty())
	{
		++nAutoNodes;
		return correctNodeName("N", nAutoNodes);
	}
	
	// 2 - If node is the ground, return "0"
	else if (name == "GND" || name == "0")
	{
		return "0";
	}
	
	// 3 - Use last component's nodes
	else if (name[0] == ':' && componentManager->pLastAdded != nullptr)
	{
		const vector<Node*> lastCompNodes = componentManager->pLastAdded->getNode();
		int nNodes = static_cast<int> (lastCompNodes.size());

		// If node name is only ":", use last node of last component
		if (name.size() == 1)
		{
			return lastCompNodes[nNodes]->getName();
		}

		// If node name is for example :a, use node a of last component
		else
		{
			int node_no = charToInt(name[1]);
			if (node_no == -1 || node_no > nNodes-1)
			{
				return "";
			}

			return lastCompNodes[node_no]->getName();
		}
	}

	// 4 - Else, do nothing
	return name;
}

vector<string> circuitNameHandler::ValidateNodeNames(const vector<string> & names)
{
	vector<string> temp_names;
	for (int i=0; i<names.size(); ++i)
	{
		temp_names.push_back(ValidateNodeName(names[i]));
	}
	return (temp_names);
}

//--------------------------------------------------
vector<string> circuitNameHandler::getComponentNames() const
{
	vector<string> out;
	componentManager->ForEachComponent([&out](auto& comp)
	{
		out.push_back(comp->getName());
	});

	return out;
}

vector<string> circuitNameHandler::getNodeNames() const
{
	return getVectorNames(*node);
}

vector<string> circuitNameHandler::getControlNames() const
{
	return getVectorNames(*control);
}

vector<string> circuitNameHandler::getSubcircuitNames() const
{
	return getVectorNames(*subcircuit);
}

//--------------------------------------------------
int circuitNameHandler::getComponentPosition(string comp_name) const
{
	return (circuit_ns::get_position(*componentManager,comp_name)); 
}

int circuitNameHandler::getControlPosition(string control_name) const
{
	return (circuit_ns::get_position(*control, control_name));
}

int circuitNameHandler::getSubcircuitPosition(string subcircuit_name) const
{
	return (circuit_ns::get_position(*subcircuit, subcircuit_name));
}

int circuitNameHandler::getNodePosition(string node_name) const
{
	return (circuit_ns::get_position(*node,node_name));
}

Component* circuitNameHandler::getComponent(const string & comp_name) const
{
	map<string, Component*>::const_iterator it = comp_dict->find(comp_name);
	if (it != comp_dict->end())
		return it->second;
	else
		return NULL;
}

Control * circuitNameHandler::getControl(string control_name) const
{
	int ControlPosition = getControlPosition(control_name);
	if (ControlPosition == -1)
		return (NULL);
	return (control->at(ControlPosition));
}

Subcircuit * circuitNameHandler::getSubcircuit(string subcircuit_name) const
{
	int SubcircuitPosition = getSubcircuitPosition(subcircuit_name);
	if (SubcircuitPosition == -1)
		return (NULL);
	return (subcircuit->at(SubcircuitPosition));
}


Element * circuitNameHandler::getElement(string elem_name) const
{
	Element * elem = getComponent(elem_name);
	if (elem != NULL)
		return (elem);
	
	elem = getControl(elem_name);
	if (elem != NULL)
		return (elem);
	
	elem = getSubcircuit(elem_name);
	return (elem);
}

ElementType circuitNameHandler::getElementType(string elem_name) const
{
	Element * elem = getElement(elem_name);
	if (elem == NULL)
		return (ElementType::Invalid);

	return (elem->GetElementType());
}

Node * circuitNameHandler::getNode(string node_name) const
{
	int NodePosition = getNodePosition(node_name);
	if (NodePosition == -1)
		return (NULL);
	return (node->at(NodePosition));
}

int circuitNameHandler::getNodeNumber(string node_name) const
{
	int NodeNumder = 0;
	if (node_name != "0" && node_name != "GND")
	{
		NodeNumder = getNodePosition(node_name);
	
		if (NodeNumder >= 0)
			++NodeNumder;
	}
	return (NodeNumder);
}

//--------------------------------------------------
int circuitNameHandler::nextAutoComponent(const ComponentType type)
{
	return ++nAutoComponents[static_cast<const size_t>(type)];
}

int circuitNameHandler::nextAutoElem(ElementType type)
{
	switch (type)
	{
		case ElementType::Control:
			return ++nAutoControl;
		case ElementType::Subcircuit:
			return ++nAutoSubcircuit;
		default:
			return -1; 
	}
}
