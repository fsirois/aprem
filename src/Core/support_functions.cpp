
#include "ComponentManager.h"
#include "support_functions.h"
#include <sstream>
#include <functional>

using namespace std;

namespace circuit_ns
{
//-------------------------------------------------------------------------
// Complex number operations
//-------------------------------------------------------------------------
void complex_multiply(double A, double B, double C, double D,
					  double * Out_r, double * Out_i)
{
	*Out_r = A*C - B*D;
	*Out_i = (A+B)*(C+D) - A*C - B*D;
}

void complex_division(double A, double B, double C, double D,
					  double * Out_r, double * Out_i)
{
	*Out_r = (A*C + B*D)/(C*C+D*D);
	*Out_i = (B*C - A*D)/(C*C+D*D);
}


void get_complex_number(const std::vector<double>& in, double& out_r, double& out_i)
{
	out_r = in.size() > 0 ? in[0] : 0.0;
	out_i = in.size() > 1 ? in[1] : 0.0;
}

// get_complex_number --> deprecated
void get_complex_number(const double * in_r, const double * in_i, double * out_r, double * out_i)
{
	if (in_r != NULL) *out_r = *in_r;
	else *out_r = 0.0;

	if (in_i != NULL) *out_i = *in_i;
	else *out_i = 0.0;
}

double norm(double real, double imag)
{
	return sqrt(real*real + imag*imag);
}

double round( double value )
{
	return floor( value + 0.5 );
}

//-------------------------------------------------------------------------
// Methods used to return strings
//-------------------------------------------------------------------------

PropertyType ToPropertyType(const string& name)
{
	if (name == "V")
	{
		return PropertyType::Voltage;
	}
	else if (name == "I")
	{
		return PropertyType::Current;
	}

	return PropertyType::Invalid;
}

string ToString(const PropertyType type)
{
	switch (type)
	{
	case PropertyType::Voltage:
		return "V";
	case PropertyType::Current:
		return "I";
	default:
		break;
	}

	return "Invalid";
}

std::string IntToString(int in)
{
	std::ostringstream stm;
	stm << in;
	return stm.str();
}

// trim from start
std::string &left_trim(std::string &s)
{
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

// trim from end
std::string &right_trim(std::string &s)
{
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

// trim from both ends
std::string &trim(std::string &s)
{
	return left_trim(right_trim(s));
}

int get_position(const ComponentManager& container, const std::string& name)
{
	for (int i = 0; i < container.size(); ++i)
	{
		if (container[i]->getName() == name)
			return i;
	}
	return -1;
}

} /*circuit_ns*/



