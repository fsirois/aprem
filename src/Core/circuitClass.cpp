//-------------------------------------------------------------------------
// Circuit Class - Method implementation
// General methods are implemented here. Not all of them.
//-------------------------------------------------------------------------

#include "circuitClass.h"
#include "CircuitParser.h"

#include "Node.h"

using namespace std;

//-------------------------------------------------------------------------
// Constructeur/Destructeur
//-------------------------------------------------------------------------

// Default constructor ----------------------------------------------------
circuit::circuit() :
	NameHandler(&node, &m_componentManager, &control, &subcircuit, &comp_dict)
{
	topologyChanged = true;
	enumerated = 0;
	maxIterControl = MAX_ITER;

	Ground = new Node("0");

	readIni();
}

// Copy constructor -------------------------------------------------------
circuit::circuit(const circuit * const Circuit)
{
	// Main flags
	topologyChanged = Circuit->topologyChanged;
	enumerated = Circuit->enumerated;
	maxIterControl = MAX_ITER;

	// Create all nodes with new - new circuit is responsible for all nodes
	Ground = new Node("0");
	for (int i=0; i<Circuit->node.size(); i++)
	{
		Node * newNode = new Node(Circuit->node[i], Circuit->node[i]->getName());
		node.push_back(newNode);
	}

	// Create components - new circuit responsible for all components
	Circuit->m_componentManager.ForEachComponent([&](auto component)
	{
		using CompType = std::remove_pointer_t<decltype(component)>;
		auto newComponent = new CompType(*component);

		push_back(m_componentManager, newComponent);
		comp_dict[newComponent->getName()] = newComponent;
	});
	
	// Force new components to point to the proper nodes (new ones)
	auto& comp = m_componentManager;
	for (int i=0; i<comp.size(); i++)
	{
		vector<Node*> comp_nodes = Circuit->m_componentManager[i]->getNodes();
		vector<Node*> new_nodes;
		for (int j=0; j<comp_nodes.size(); j++)
		{
			int node_number = comp_nodes[j]->getNumber();
			if (node_number == 0)
				new_nodes.push_back(Ground);
			else
				new_nodes.push_back(node[node_number - 1]);
		}
		comp[i]->setNodes(new_nodes);
	}
	
	for (int i=0; i<Circuit->control.size(); i++)
	{
		Control * newControl = new Control(Circuit->control[i]);
		control.push_back(newControl);
	}
	
	// Force new controls to point to the proper components
	for (int i=0; i<control.size(); i++)
	{
		vector<Component*> in_comp = Circuit->control[i]->getInComponent();
		vector<Component*> out_comp = Circuit->control[i]->getOutComponent();
		vector<Component*> new_in_comp;
		vector<Component*> new_out_comp;
		for (int j=0; j<in_comp.size(); j++)
		{
			int pos = Circuit->NameHandler.getComponentPosition(in_comp[j]->getName());
			new_in_comp.push_back(comp[pos]);
		}
		for (int j=0; j<out_comp.size(); j++)
		{
			int pos = Circuit->NameHandler.getComponentPosition(out_comp[j]->getName());
			new_out_comp.push_back(comp[pos]);
		}
		control[i]->setInputComponents(new_in_comp);
		control[i]->setOutputComponents(new_out_comp);
	}
	
	// Create the name handler on those elements
	NameHandler = circuitNameHandler(&node,&comp,&control,&subcircuit,&comp_dict);
	
	readIni();
}

// Destructor -------------------------------------------------------------
circuit::~circuit()
{
	// TODO : put that in the component manager destructor
	m_componentManager.ForEachComponent([](Component* comp)
	{
		delete comp;
	});

	for (int i=0; i<node.size(); ++i) delete node[i]; // Delete nodes
	for (int i=0; i<control.size(); ++i) delete control[i];
	for (int i=0; i<subcircuit.size(); ++i) delete subcircuit[i];

	delete Ground;
}

//-------------------------------------------------------------------------
// Circuit Load
//-------------------------------------------------------------------------
unsigned char GetParamCount(const ComponentType componentType, unsigned char& realCount, unsigned char& angleCount, unsigned char& complexCount)
{
	InputTypeCounts counts = InputCountsMapping[componentType];
	realCount = counts.nReal;
	angleCount = counts.nAngle;
	complexCount = counts.nComplex;

	return realCount + complexCount + angleCount;
}

unsigned char GetDoubleCount(const ComponentType componentType)
{
	unsigned char realCount;
	unsigned char complexCount;
	unsigned char angleCount;
	GetParamCount(componentType, realCount, angleCount, complexCount);

	return realCount + angleCount + 2u * complexCount;
}

vector<string> ExtractNodes(vector<string>& params, const int nodeCount)
{
	vector<string> nodeNames;
	if (nodeCount > params.size())
	{
		// error
		return vector<string>();
	}

	for (int i = 0; i < nodeCount; i++)
	{
		nodeNames.push_back(params[i]);
	}

	params.erase(params.begin(), params.begin() + nodeCount);

	return nodeNames;
}

DrawInfo circuit::Load(const string& fileName, CircuitErrorCode& errorCode)
{
	CircuitParser parser;
	PackedCircuit data = parser.LoadFile(fileName);

	DrawInfo drawInfo;

	for (const PackedComponent& component : data.m_vComponents)
	{
		const size_t drawInfoPosition = component.m_sParams.find(':');
		string drawInfoString = "";
		if (drawInfoPosition != std::string::npos && component.m_sParams.size() > (drawInfoPosition+1))
		{
			drawInfoString = component.m_sParams.substr(drawInfoPosition+1);
		}

		const string paramsString = component.m_sParams.substr(0, drawInfoPosition);
		vector<string> params;
		circuit_ns::split(paramsString, params);

		const ComponentType componentType = component.m_eType;
		const int nodeCount = circuit_ns::NodeCount(componentType);

		const vector<string> nodeNames = ExtractNodes(params, nodeCount);
		vector<double> values;

		unsigned char realCount = 0u;
		unsigned char complexCount = 0u;
		unsigned char angleCount = 0u;
		unsigned char totalCount = GetParamCount(componentType, realCount, angleCount, complexCount);
		if (totalCount != params.size())
		{
			// return error
			continue;
		}

		bool bSkipValidation = false;

		for (int i = 0; i < params.size(); i++)
		{
			if (params[i][0] == '$')
			{
				bSkipValidation = true;

				string strCopy = params[i];
				const double multiplier = CircuitParser::ToMultiplier(strCopy);

				if (!circuit_ns::contains(m_ParametrizedComponents, component.m_sName))
				{
					m_ParametrizedComponents.emplace_back(component.m_sName, SetProperty(componentType, i), multiplier);
				}
				else
				{
					// TODO : going through array twice is very inefficient
					const int index = circuit_ns::get_position(m_ParametrizedComponents, component.m_sName);
					m_ParametrizedComponents[index].m_vsProperties.emplace_back(SetProperty(componentType, i), multiplier);
				}

				values.push_back(0.0);

				if (i >= (realCount+angleCount))
				{
					values.push_back(0.0);
				}
			}

			// assume all real numbers come before complex ones
			else if (i < realCount)
			{
				double real = CircuitParser::ToDouble(params[i]);
				values.push_back(real);
			}
			else if (i < (realCount + angleCount))
			{
				double angle = CircuitParser::ToAngleDegree(params[i]);
				values.push_back(angle);
			}
			else
			{
				double real = 0.0;
				double imag = 0.0;
				CircuitParser::ToComplex(params[i], real, imag);

				values.push_back(real);
				values.push_back(imag);
			}
		}

		if (GetDoubleCount(componentType) != values.size())
		{
			mexPrintf("Error !");
			continue;
		}

		CircuitErrorCode componentErrorCode;
		addComponent(component.m_sName, nodeNames, values, componentType, componentErrorCode);

		if (!bSkipValidation && !componentErrorCode.IsValid())
		{
			errorCode = CircuitErrorCode(std::move(componentErrorCode));
			return DrawInfo();
		}

		DrawInfoItem drawInfoItem;
		drawInfoItem.Name = component.m_sName;
		drawInfoItem.NodeNames = nodeNames;
		drawInfoItem.Info = drawInfoString;
		drawInfo.Add(std::move(drawInfoItem));
	}

	drawInfo.DrawParam = data.m_sDrawParam;
	drawInfo.DrawStrings = parser.GetDrawStrings();
	m_Outputs = data.m_vCircuitOutput;
	m_sCircuitTag = data.m_sTag;
	m_sCircuitName = data.m_sName;

	return drawInfo;
}

CircuitErrorCode circuit::Validate() const
{
	CircuitErrorCode errorCodeOut;

	m_componentManager.ForEachComponent([&errorCodeOut](const Component* component)
	{
		CircuitErrorCode errorCode = component->Validate();
		if (!errorCode.IsValid())
		{
			errorCodeOut = errorCode;
		}
	});

	return errorCodeOut;
}

//-------------------------------------------------------------------------
// reset Flags
//-------------------------------------------------------------------------
void circuit::resetFlags()
{
	for (int i=0; i<node.size(); i++)
		node[i]->setAsTempGND(false);
	
	ComponentManager& manager = m_componentManager;
	for (int i = 0; i < GetContainer<PVSource*>(manager).size(); i++)
	{
		GetContainer<PVSource*>(manager)[i]->setUseAsVoltageSource(false);
	}

	// Make sure it's OK to NOT set On all components
	//for (int i=0; i<comp.size(); i++)
		//comp[i]->setOn(static_cast<int>(true));

	for (int i = 0; i < GetContainer<PQLoad*>(manager).size(); i++)
	{
		GetContainer<PQLoad*>(manager)[i]->setTempDisable(false);
	}

	topologyChanged = true;
}

//-------------------------------------------------------------------------
// solve 
//-------------------------------------------------------------------------
CircuitErrorCode circuit::solve()
{
	CircuitErrorCode errorCode = Validate();
	if (!errorCode.IsValid())
	{
		return errorCode;
	}

	// Si le circuit est doté de contrôles.
	if (control.size() != 0)
	{
		bool allFlags = false;
		int nIter = 0;
		
		while (!allFlags && (nIter < maxIterControl))
		{
			
			allFlags = true;
			nIter++;

			// Évaluation du circuit
			if (!enumerated)
				enumerate();
			
			if (topologyChanged)
			{
				resetFlags();

				errorCode = checkTopology();
				if (!errorCode.IsValid())
				{
					return errorCode;
				}
			}

			// Main matrix solve
			loadFlow(errorCode);
			if (!errorCode.IsValid())
			{
				return errorCode;
			}

			// Boucle d'évaluation des fonctions contrôle
			for (int i=0; i<control.size(); i++)
			{
				if (control[i]->isOn())
				{
					// Regroupement des variables
					string ctrlFctName = control[i]->getFunctionName();
					vector<double> ctrlFctData = control[i]->getFunctionData();

					// Recueil des variables d'entrée.
					pair<vector<string>,vector<string> > in  = control[i]->getInputs();
					vector<pair<double,double> > data_in;
					for (int j=0; j<in.first.size(); ++j)
					{
						vector<pair<double,double> > cur_val = get(in.first[j], in.second[j]);
						data_in.push_back(cur_val[0]);
					}

					pair<vector<string>,vector<string> > out = control[i]->getOutputs();
					vector<pair<double,double> > data_out;
					for (int j=0; j<out.first.size(); ++j)
					{
						vector<pair<double,double> > cur_val = get(out.first[j], out.second[j]);
						data_out.push_back(cur_val[0]);
					}

					pair<bool, vector<pair<double,double> > > ctrl_out;
					ctrl_out = callControlFunction(ctrlFctName, ctrlFctData, data_in, data_out);
					
					bool flag = ctrl_out.first;

					vector<pair<double,double> > new_data_out = ctrl_out.second;
					for (int j=0; j<new_data_out.size(); j++)
					{
						bool isValid = set(out.first[j], out.second[j], new_data_out[j].first, new_data_out[j].second);
					}

					allFlags = allFlags && flag;
				}
			}
		}

		if (!allFlags)
		{
			errorCode.SetErrorMessage( "Convergence was not achieved." );
			return errorCode;
		}
	}

	// Si le circuit ne contient pas de contrôle.
	else
	{
		if (!enumerated)
		{
			enumerate();
		}

		if (topologyChanged)
		{
			resetFlags();
			errorCode = checkTopology();

			if (!errorCode.IsValid())
			{
				return errorCode;
			}
		}

		loadFlow(errorCode);
		if (!errorCode.IsValid())
		{
			return errorCode;
		}
	}

	return errorCode;
}

//-------------------------------------------------------------------------
// Collect Data
//-------------------------------------------------------------------------
void circuit::collectData(Eigen::VectorXd X)
{
	int index = 2 * cnodes;

	int j=0;
	for (int i=0; i<node.size(); i++)
	{
		// Patch to avoid crashes, should make it smarter
		if (node[i]->getOn())
		{
			node[i]->setVr(X(2*j));
			node[i]->setVi(X(2*j + 1));
			++j;
		}
	}

	const auto setCurrent = [&index, &X](auto comp)
	{
		comp->setIr(X(index++));
		comp->setIi(X(index++));
	};

	// HOWTO : Add component
	// add the component here, in the order for which the Ir and Ii are in the X vector (if they are)
	ForEachComponentByTypeOrder<decltype(setCurrent),
		Transformer*,
		VoltageSource*,
		Switch*,
		PVSource*,
		PQLoad*
		// ADD COMPONENT
		//	if the X vector contains currents for component add it here in correct order
		>(m_componentManager, setCurrent);

	// Impedances are a special case
	ComponentContainer<Impedance*>& impedances = GetContainer<Impedance*>(m_componentManager);
	for (int i=0; i< impedances.size(); ++i)
	{
		impedances[i]->calculateCurrent();
	}
}

//-------------------------------------------------------------------------
// Enumerate
//-------------------------------------------------------------------------
void circuit::enumerate()
{
	int i, j, index[2];
	Component* comp_ptr;
	
	// On vide les voisins des noeuds
	for(i=0; i<node.size(); ++i)
	{
		if (node[i] != NULL)
			node[i]->ClearAdjacentData();
	}
	
	// Première itération sur toutes les composantes du circuit
	// (qu'elles soient à "on" ou à "off").
	ComponentManager& manager = m_componentManager;
	for(i=0; i< manager.size(); i++)
	{
		vector<Node*> comp_node = manager[i]->getNode();
		for(j=0; j<comp_node.size(); j++)
		{
			if (comp_node[j] != NULL)
			{
				comp_node[j]->AddAdjComponent(i+1,j+1);
			}
		}
	}
	
	// Numérotation formelle des noeuds
	cnodes = 0;
	index[0] = 0;
	index[1] = 0;
	Node * CurNode;
	vector<Node*> currentNodes;
	for (i=0; i<node.size(); ++i)
	{
		CurNode = node[i];
		
		if (CurNode == NULL)
			continue;
		
		CurNode->setNumber(0);
		
		if (CurNode->getOn())
		{
			vector<int> adj_comp = CurNode->getAdjComponent();
			for (j=0; j<adj_comp.size(); j++)
			{
				if(manager[j]->isOn() && manager[j]->getState())
				{
					if (CurNode->getNumber() == 0)
					{
						++cnodes;
						CurNode->setNumber(cnodes);
					}
					
					vector<int> adj_pos = CurNode->getAdjPos();
					index[0] = adj_pos[j]-1;
					index[1] = adj_comp[j]-1;
					currentNodes = manager[index[1]]->getNode();
					currentNodes[index[0]]->setNumber(cnodes);
				}
			}
		}
	}

	// Création de la structure des voisins
	// (en prévision de la vérification topologique)
	for (i=0; i<node.size(); ++i)
	{
		CurNode = node[i];
		
		if (CurNode == NULL)
			continue;
		
		vector<int> adj_comp = CurNode->getAdjComponent();
		for (j=0; j<adj_comp.size(); ++j)
		{
			index[0]=0;
			index[1]=1;
	
			comp_ptr = manager[adj_comp[j]-1];
			
			if(comp_ptr->getType() == ComponentType::Transformer)
				{
				vector<int> adj_pos = CurNode->getAdjPos();
				if(adj_pos[j] > 2)
				{
					index[0] = 2;
					index[1] = 3;
				}
			}
			
			if (comp_ptr->isOn() && comp_ptr->getState())
			{
				for (int k=0; k<2; k++)
				{
					int node_no = 0;
					currentNodes = comp_ptr->getNode();
					if (currentNodes[index[k]] != NULL)
						node_no = currentNodes[index[k]]->getNumber();

					node[i]->AddAdjNode(node_no);
				}
			}
		}
	}
	enumerated = 1;
}

// Call control function -- use external code
pair<bool,vector<pair<double,double> > > circuit::callControlFunction(string functionName,
													   vector<double> functionData,
													   vector<pair<double,double> > data_in,
													   vector<pair<double,double> > data_out)
{
	return call_control_function(functionName,
								 functionData,
								 data_in,
								 data_out);
}

// helps get if its component, subcircuit or control
ElementType circuit::getElementType(string elem_name) const
{
	return NameHandler.getElementType(elem_name);
}


//-----------------------------
// read ini - unpolished, unfinished code
//-----------------------------
bool circuit::readIni(string iniFilePath)
{
	// Read ini file
	string iniFilePathWithName = iniFilePath+"APREM.INI";

	FileParser fileParser;
	fileParser.LoadFile(iniFilePathWithName);

	solver = Solver(fileParser.Get("DEFAULT_SOLVER", "QR" /* Default */));

	return true;
}

void circuit::BuildOutput(vector<string>& varNames, vector<pair<double, double>>& values)
{
	for (const circuit_ns::CircuitOutput& output : m_Outputs)
	{
		const string name = output.m_sComponentName;
		const string field = circuit_ns::ToString(output.m_eType);

		vector<std::pair<double, double>> Output;
		if (field == "V")
		{
			Output = getV(name);
		}
		else
		{
			Output = get(name, field);
		}

		const string variableName = m_sCircuitTag + "." + name + "." +  field;

		varNames.push_back(variableName);
		values.push_back(Output[0]);
	}
}

