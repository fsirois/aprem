#!/bin/sh

# First compile all the .cpp files in .o object files
STARTTIME=$(date +%s)
echo compiling
$(g++ -O2 -fPIC -I/usr/include/eigen3/Eigen -Isrc/includes -Isrc/Circuit/includes -I/usr/include/python3.5 -c src/ini_file_reader/iniFileReader.cpp src/Circuit/*.cpp src/circuit_class/*.cpp src/circuit_elements/component_types/*.cpp src/circuit_elements/*.cpp -lleveldb)
ENDTIME=$(date +%s)
echo took $(($ENDTIME - $STARTTIME)) seconds to compile

# Second link all the object files into a single shared library
STARTTIME=$(date +%s)
echo linking
$(g++ -shared -o libCircuit.so *.o $(python3.5-config --ldflags))
ENDTIME=$(date +%s)
echo took $(($ENDTIME - $STARTTIME)) seconds to link

# finalize the build - cleanup the object files and move the library to the user libraries folder
echo deleting object files
rm *.o

echo moving libCircuit.so to usr/lib
sudo mv libCircuit.so /usr/lib
sudo cp src/Circuit/Circuit.py /usr/lib


